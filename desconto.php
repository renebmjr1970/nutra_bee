<?php
	include("../include/inc_conexao.php");	
	include("inc_sessao.php");

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie

	$usuario	=	$_SESSION["usuarioid"];
	
	if(!is_numeric($usuario)){
		$usuario = 0;	
	}

	$id = $_REQUEST["id"];	
	if(!is_numeric($id)){
		$id = 0;	
	}
	


	if($_POST && $_REQUEST["action"]=="gravar"){
		$id					= 	addslashes($_REQUEST["id"]);

		$titulo					=	addslashes($_REQUEST["titulo"]);
		$codtipo				=	addslashes($_REQUEST["codtipo"]);
		$tipo					=	addslashes($_REQUEST["tipo"]);
		$valor					=	addslashes($_REQUEST["valor"]);
		$desconto				=	addslashes($_REQUEST["desconto"]);
		$forma_pagamento		=	addslashes($_REQUEST["forma_pagamento"]);
		$condicao_pagamento		=	addslashes($_REQUEST["condicao_pagamento"]);
		$categoria				=	addslashes($_REQUEST["codcategoria"]);
		$marca					=	addslashes($_REQUEST["codmarca"]);
		$produto				=	addslashes($_REQUEST["produto"]);
		$quantidade				=	addslashes($_REQUEST["quantidade"]);
		$utm_source				=	addslashes($_REQUEST["utm_source"]);
		$cep_inicial			=	addslashes($_REQUEST["cep_inicial"]);
		$cep_final				=	addslashes($_REQUEST["cep_final"]);
		$frete					=	addslashes($_REQUEST["frete"]);
		$ativo					=	addslashes($_REQUEST["ativo"]);
		$fornecedor				= 	0;
		
		$valor					= formata_valor_db($valor);
		$desconto				= formata_valor_db($desconto);
		$quantidade				= formata_valor_db($quantidade);
		
		if(!is_numeric($codtipo)){
			$codtipo = 0;	
		}

		if(!is_numeric($tipo)){
			$tipo = 0;	
		}

		if(!is_numeric($valor)){
			$valor = 0;	
		}

		if(!is_numeric($desconto)){
			$desconto = 0;	
		}
		
		if(!is_numeric($forma_pagamento)){
			$forma_pagamento = 0;	
		}		

		if(!is_numeric($condicao_pagamento)){
			$condicao_pagamento = 0;	
		}		

		if(!is_numeric($categoria)){
			$categoria = 0;	
		}		
		
		if(!is_numeric($marca)){
			$marca = 0;	
		}		
			
		if(!is_numeric($produto)){
			$produto = 0;	
		}		
		
		if(!is_numeric($quantidade)){
			$quantidade = 0;	
		}		

		if(!is_numeric($fornecedor)){
			$fornecedor = 0;	
		}	

		if(!is_numeric($cep_inicial) || strlen($cep_inicial)<8){
			$cep_inicial = "00000000";	
		}	

		if(!is_numeric($cep_final) || strlen($cep_final)<8){
			$cep_final = "99999999";	
		}	

		if(!is_numeric($frete)){
			$frete = 0;	
		}	

		if(!is_numeric($ativo)){
			$ativo = 0;	
		}	

		
		if($id==0){
			//verifica se nao tem ja o desconto cadastrado			
			$ssql = "select descontoid from tbldesconto where dtitulo='{$titulo}' and dcodtipo='{$tipo}' and dtipo='{$tipo}' and dvalor='{$valor}' and ddesconto='{$desconto}'
						and dcodforma_pagamento='{$forma_pagamento}' and dcodcondicao_pagamento='{$condicao_pagamento}' and dcodcategoria='{$categoria}' 
						and dcodmarca='{$marca}' and dcodproduto='{$produto}' and dquantidade='{$quantidade}' and dutm_source='{$utm_source}' 
						and dcep_inicial='{$cep_inicial}' and dcep_final='{$cep_final}' and dcodfrete='{$frete}'";
			$result = mysql_query($ssql);
			if($result){
				$num_rows = mysql_num_rows($result);
				if($num_rows>0){
					$msg = "Já existe um registro com os mesmos dados digitados";
					$erro = 1;
				}
			}
			
			if($num_rows==0){
				$ssql = "insert into tbldesconto (dtitulo, dcodtipo, dtipo, dvalor, ddesconto, dcodforma_pagamento, dcodcondicao_pagamento, dcodcategoria, dcodmarca, dcodproduto, 
						dquantidade, dcodfornecedor, dutm_source, dcep_inicial, dcep_final, dcodfrete, dativo, dcodusuario, ddata_alteracao, ddata_cadastro) 
						values ('{$titulo}','{$codtipo}','{$tipo}','{$valor}','{$desconto}','{$forma_pagamento}','{$condicao_pagamento}','{$categoria}','{$marca}',
								'{$produto}','{$quantidade}','{$fornecedor}','{$utm_source}','{$cep_inicial}','{$cep_final}','{$frete}','{$ativo}',
								'{$usuario}','{$data_hoje}','{$data_hoje}')
						";						
						$result = mysql_query($ssql);
						if(!$result){
							$msg = "Erro ao gravar registro";
							$erro = 1;
						}
						else
						{
							$id = mysql_insert_id();
							$msg = "Registro salvo com sucesso";
						}						
						
			}
			
			
		}
		else
		{
		
			$ssql = "update tbldesconto set dtitulo='{$titulo}', dcodtipo='{$codtipo}', dtipo='{$tipo}', dvalor='{$valor}', ddesconto='{$desconto}', 
					dcodforma_pagamento='{$forma_pagamento}', dcodcondicao_pagamento='{$condicao_pagamento}', dcodcategoria='{$categoria}', dcodmarca='{$marca}', 
					dcodproduto='{$produto}', dquantidade='{$quantidade}', dcodfornecedor=0, dutm_source='{$utm_source}', dcep_inicial='{$cep_inicial}', 
					dcep_final='{$cep_final}', dcodfrete='{$frete}', dativo='{$ativo}', 
					dcodusuario='{$usuario}', ddata_alteracao='{$data_hoje}'
					where descontoid='{$id}'";
			$result = mysql_query($ssql);
			if($result){
				$msg = "Registro atualizado com sucesso.";	
			}else{
				$msg = "Erro ao atualizar registro.";	
			}		

		}

	
	}
	
	
	//echo $ssql;



	if($id>0){
	
		$ssql = "select descontoid, dtitulo, dcodtipo, dtipo, dvalor, ddesconto, dcodforma_pagamento, dcodcondicao_pagamento, dcodcategoria, dcodmarca, dcodproduto, 
				dquantidade, dcodfornecedor, dutm_source, dcep_inicial, dcep_final, dcodfrete, dativo, dcodusuario, ddata_alteracao, ddata_cadastro
				from tbldesconto where descontoid='{$id}'
				";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				
				$titulo					=	$row["dtitulo"];
				$codtipo				=	$row["dcodtipo"];
				$tipo					=	$row["dtipo"];
				$valor					=	number_format($row["dvalor"],2,",",".");
				$desconto				=	number_format($row["ddesconto"],2,",",".");
				$forma_pagamento		=	$row["dcodforma_pagamento"];
				$condicao_pagamento		=	$row["dcodcondicao_pagamento"];
				$categoria				=	$row["dcodcategoria"];
				$marca					=	$row["dcodmarca"];
				$produto				=	$row["dcodproduto"];
				$quantidade				=	number_format($row["dquantidade"],2,",",".");
				$utm_source				=	$row["dutm_source"];
				$cep_inicial			=	$row["dcep_inicial"];
				$cep_final				=	$row["dcep_final"];
				$frete					=	$row["dcodfrete"];
				$ativo					=	$row["dativo"];
				
				$usuario			=	get_usuario($row["dcodusuario"],"unome");		
				$data_alteracao		=	formata_data_tela($row["ddata_alteracao"]);
				$data_cadastro		=	formata_data_tela($row["ddata_cadastro"]);
				
			}
			mysql_free_result($result);
		}	
		
	}

												

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Painel de Administração - desconto Virtual</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="NOINDEX, NOFOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="Painel de Administração - desconto Virtual" />
<meta name="description" content="Painel de administração da desconto virtual" />
<meta name="keywords" content="desconto virtual" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="painel de administração" />



<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>" />

<link href="css/admin.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {							   		
		$("#codtipo").focus();	
		$("#cep_inicial").mask("99999-999");
		$("#cep_final").mask("99999-999");
		desconto_habilita_campos($("#codtipo").attr("value"));
	});
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="header">
    <span class="label-inicio">Painel de Administração</span> <a href="index.php">[ <em>Sair do Sistema</em> ]</a>
</div>

<div id="global-container">

    <div id="menu-left">
    	
	<?php
    	include("inc_menu.php");
    ?>
        
    </div>
    
    <div id="content">    
	<form method="post" name="frm_desconto" id="frm_desconto" action="desconto.php" onsubmit="return valida_desconto();">
    <input name="action" id="action" type="hidden" value="gravar" />  
    <input name="id" id="id" type="hidden" value="<?php echo $id?>" />
    	<div id="conteudo">
        	<div id="titulo-conteudo">
            	<span class="label-inicio">Desconto &raquo;  <span onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='desconto.php';"><?php echo ($id==0) ? "Novo Registro" : $titulo;  ?></span> </span> <a href="desconto_consulta.php">Consulta</a>
       	  </div>
            
            <div id="conteudo-interno">
				<table width="98%" border="0" cellspacing="0" cellpadding="2" style="margin:10px;">
              <tr>
                <td width="130">ID</td>
                <td>#<?php echo $id;?></td>
                <td width="200" rowspan="41" align="center" valign="top">
				<?php echo $msg;?>
                    <div id="dica" style="margin:10px;">
                    &nbsp;
                    </div>
                </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Tipo</td>
                <td><select name="codtipo" size="1" id="codtipo" class="formulario" onchange="javascript:desconto_habilita_campos(this.value);">
                  <option value="0">Selecione</option>
                  <option value="1" <?php if($codtipo==1)echo "selected";?>>Valor do Pedido</option>
                  <option value="2" <?php if($codtipo==2)echo "selected";?>>Frete Grátis</option>
                  <option value="3" <?php if($codtipo==3)echo "selected";?>>Desconto por Produto</option>
                  <option value="4" <?php if($codtipo==4)echo "selected";?>>Desconto na 1o. Compra</option>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr> 
              <tr>
                <td>Título</td>
                <td><input name="titulo" type="text" class="formulario" id="titulo" value="<?php echo $titulo;?>" size="75" maxlength="100" /></td>
                </tr>  
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>                                                        
              <tr>
                <td>Tipo de desconto</td>
                <td><select name="tipo" size="1" id="tipo" class="formulario">
                  <option value="0">Selecione</option>
                  <option value="1" <?php if($tipo==1)echo "selected";?>>Percentual</option>
                  <option value="2" <?php if($tipo==2)echo "selected";?>>Valor Real</option>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Valor</td>
                <td><input name="valor" type="text" class="formulario" id="valor" value="<?php echo $valor;?>" size="20" maxlength="20" /> 
                  0,00 (Valor do pedido)</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>                                          
              <tr>
                <td>desconto</td>
                <td><input name="desconto" type="text" class="formulario" id="desconto" value="<?php echo $desconto;?>" size="20" maxlength="20" />
                  0,00 </td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Forma Pagamento</td>
                <td><select name="forma_pagamento" size="1" class="formulario" id="forma_pagamento">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select formapagamentoid, fforma_pagamento from tblforma_pagamento order by fforma_pagamento";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["formapagamentoid"]."' ";
							if($forma_pagamento==$row["formapagamentoid"]){
								echo " selected";	
							}	
							echo ">";
							echo $row["fforma_pagamento"];
							echo "</option>";
						}
						mysql_free_result($result);
					}
				  ?>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Condição Pagamento</td>
                <td><select name="condicao_pagamento" size="1" class="formulario" id="condicao_pagamento">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select tblforma_pagamento.fforma_pagamento, tblcondicao_pagamento.condicaoid, tblcondicao_pagamento.ccondicao 
							from tblcondicao_pagamento 
							inner join tblforma_pagamento on tblcondicao_pagamento.ccodforma_pagamento = tblforma_pagamento.formapagamentoid
							order by tblforma_pagamento.fforma_pagamento, tblcondicao_pagamento.cnumero_parcelas";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["condicaoid"]."' ";
							if($condicao_pagamento==$row["condicaoid"]){
								echo " selected";	
							}	
							echo ">";
							echo $row["fforma_pagamento"] . " -> " . $row["ccondicao"];
							echo "</option>";
						}
						mysql_free_result($result);
					}
				  ?>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Categoria</td>
                <td><select name="categoria" size="1" class="formulario" id="categoria">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select tblcategoria.categoriaid, tblcategoria.ccategoria 
								from tblcategoria 
								where tblcategoria.ccodcategoria=0
								order by tblcategoria.cordem, tblcategoria.ccategoria";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["categoriaid"]."' ";
							if($categoria==$row["categoriaid"]){
								echo " selected";	
							}	
							echo ">";
							echo $row["ccategoria"];
							echo "</option>";

							/*-------------------------------------------*/
							
							$ssql1 = "select tblcategoria.categoriaid, tblcategoria.ccategoria 
										from tblcategoria 
										where tblcategoria.ccodcategoria=".$row["categoriaid"]."
										order by tblcategoria.cordem, tblcategoria.ccategoria";
							$result1 = mysql_query($ssql1);
							if($result1){
								while($row1=mysql_fetch_assoc($result1)){
									echo "<option value='".$row1["categoriaid"]."' ";
									if($categoria1==$row1["categoriaid"]){
										echo " selected";	
									}	
									echo ">";
									echo $row["ccategoria"] . " -> " . $row1["ccategoria"];
									echo "</option>";
								}
								mysql_free_result($result1);
							}
							/*-------------------------------------------*/
							
							
							
						}
						mysql_free_result($result);
					}
				  ?>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Marca</td>
                <td><select name="marca" size="1" class="formulario" id="marca">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select marcaid, mmarca from tblmarca order by mmarca";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["marcaid"]."' ";
							if($marca==$row["marcaid"]){
								echo " selected";	
							}	
							echo ">";
							echo $row["mmarca"];
							echo "</option>";
						}
						mysql_free_result($result);
					}
				  ?>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Produto</td>
                <td><input name="produto" type="hidden" id="produto" value="<?php echo $produto;?>" />
                  <input name="codigo" type="text" class="formulario" id="codigo" value="<?php echo $codigo;?>" size="20" maxlength="20" onblur="javascript:get_produtoid_by_sku(this.value)"; /> 
                  SKU do Produto</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Quantidade</td>
                <td><input name="quantidade" type="text" class="formulario" id="quantidade" value="<?php echo $quantidade;?>" size="20" maxlength="20" />
                  0,00</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Parceiro de Mídia</td>
                <td><input name="utm_source" type="text" class="formulario" id="utm_source" value="<?php echo $utm_source;?>" size="20" maxlength="20" /> 
                  parâmetro utm_source</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Cep Inicial</td>
                <td><input name="cep_inicial" type="text" class="formulario" id="cep_inicial" value="<?php echo $cep_inicial;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Cep FInal</td>
                <td><input name="cep_final" type="text" class="formulario" id="cep_final" value="<?php echo $cep_final;?>" size="20" maxlength="20" /></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>Tipo de Frete</td>
                <td><select name="frete" size="1" class="formulario" id="frete">
                  <option value="0">Selecione</option>
                  <?php
                  	$ssql = "select freteid, fcodigo, fdescricao from tblfrete_tipo order by freteid";
					$result = mysql_query($ssql);
					if($result){
						while($row=mysql_fetch_assoc($result)){
							echo "<option value='".$row["freteid"]."' ";
							if($frete==$row["freteid"]){
								echo " selected";	
							}	
							echo ">";
							echo $row["fdescricao"] . " - " . $row["fcodigo"] . "";
							echo "</option>";
						}
						mysql_free_result($result);
					}
				  ?>
                </select></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>Ativo</td>
                <td><label>
                  <input type="radio" name="ativo" id="ativo" value="-1" <?php echo ($ativo == -1) ? "checked" : "" ;?>  />
                  Sim</label>
                  <label>
                    <input type="radio" name="ativo" id="ativo" value="0" <?php echo ($ativo == 0) ? "checked" : "" ;?>  />
                Não</label></td>
              </tr>
              
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              
              <tr>
                <td>Usuário</td>
                <td><?php echo $usuario;?></td>
                </tr>
              <tr>
                <td>Data Alteração</td>
                <td><?php echo $data_alteracao;?></td>
                </tr>
              <tr>
                <td>Data Cadastro</td>
                <td><?php echo $data_cadastro;?></td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                </tr>
              <tr>
                <td>&nbsp;</td>
                <td><input type="submit" id="btn-cmd-gravar" name="btn-cmd-gravar" value="Gravar" class="btn-gravar" /></td>
                </tr>
              </table>
          </div>
        </div>
        
        
        </form>
    </div>
    
    <div id="footer"></div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>