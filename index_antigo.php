<?php
	include("include/inc_conexao.php");	
	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/	
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."");
			exit();
		}
	}
		
	
  if(isset($_REQUEST['action'])){
	if( $_REQUEST['action'] == 'logout' ){
	
		if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name = trim($parts[0]);
				setcookie($name, '', time()-1000);
				setcookie($name, '', time()-1000, '/');
			}
		}
		$_SESSION = array(); // destroi todas as sessoes abertas
	
		header("location: index.php");		
	}	
  }


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />
<meta name="title" content="<?php echo $site_nome;?>" />
<meta name="description" content="<?php echo $site_descricao;?>" />
<meta name="keywords" content="<?php echo $site_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> " />
<meta name="copyright" content="<?php echo $site_nome;?>" />
<link rel="shortcut icon" href="images/favicon.png" type="image/png" />
<link rel="canonical" href="<?php echo $site_site;?>" />
<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-cycle.js"></script>
<script type="text/javascript" src="js/jcarousellite.js"></script>
<?php
  include("include/inc_analytics.php");	
?>
<style type="text/css">
	.produto{ margin: 29px 0 -24px 0; }
	
	.bolinhabanner{
	
	border: 2px solid #fff;
    border-radius: 6px;
    cursor: pointer;
    display: inline-block;
    height: 10px;
    margin: 0 4px;
	position:relative;
	z-index:1;
    text-indent: -999em;
    transition: background 0.5s ease 0s, opacity 0.5s ease 0s;
    width: 10px;
	background: #FFF;
	}
	
	.bolinhabannerativo{
	border: 2px solid #fff;
	background-color:#0079cc;
    border-radius: 6px;
    cursor: pointer;
    display: inline-block;
    height: 10px;
    margin: 0 4px;
	position:relative;
	z-index:1;
    text-indent: -999em;
    transition: background 0.5s ease 0s, opacity 0.5s ease 0s;
    width: 10px;
	}
	
</style>
</head>

<body onload="loadBanner();">

<div id="header-content" style="height:200px;">
	<?php
	include ("inc_header.php");
	?>
</div>

<div id="global-container">
	<!-- popup whatsapp -->
	<div id="dark"></div>
		<div id="popup-img">
			<div id="close-popup">x</div>
		</div>
	
	<!-- end popup -->
	<div id="banner">
		<?php carregaBanners(1); ?>
		<!--<div id="botoesBannerFull"><div id="bbfleft" onclick="trocaBanner('p');"></div><div id="bbfright" onclick="trocaBanner('n');"></div></div>-->
		
		<script>
		$(document).ready(function(){
		
			$("li[name=bolinha]").click(function(){
				$("li[name=bolinha]").attr("class","bolinhabanner");
				$(this).attr("class","bolinhabannerativo");
				
				
				clearTimeout(temporizador);
					
				$("span[title$='Banner']").fadeOut();
				$("span[title$='Banner']").attr('class','off');
				
				index = eval($(this).html()); 
				rotacionaBanner(index);	
				
				$("span[title$='Banner']:contains('"+index+"')").fadeIn();
				$("span[title$='Banner']:contains('"+index+"')").attr('class','on');
			});

			//popup whatsapp
			// $('#dark').fadeIn(500);
			// $('#popup-img').fadeIn(500);	
		
			setTimeout(function(){
				$('#dark').fadeOut(500);
				$('#popup-img').fadeOut(500);			
			}, 10000);
		
			$('#close-popup, #dark').on('click', function(){
				$('#dark').fadeOut(500);
				$('#popup-img').fadeOut(500);
			});
		});

		//var date = new Date();
		//var minutes = 30;
		//date.setTime(date.getTime() + (minutes * 60 * 1000));
		
		//var teste = String(date);
		//var test = teste.split("(");
		
		//document.cookie="teste=home-popup; expires=Tue Dec 30 2014 11:55:14 UTC; path=/";
		//alert(document.cookie);
		//alert("username=home-popup; expires="+test[0]);
		//console.log("username=home-popup; expires="+test[0]);
		</script>

	</div>
	<div style="width: 1001px; margin: 0 auto;">
		<?php
			carregaSlideBanners();
		?>
	</div>
		<!--
		<ol class="bolinha_wrapper">
			<li name="bolinha" class="bolinhabanner">1</li>
			<li name="bolinha" class="bolinhabanner">2</li>
			<li name="bolinha" class="bolinhabanner">3</li>
			<li name="bolinha" class="bolinhabanner">4</li>
		</ol>
		-->
	<div class="carousel-container">
		<div id="social-bar">
			<span id="social-text">
		</span>
			<div id="icones-content">
				<span class="icone"><a href="https://www.facebook.com/nutracorpore" target="_blank"><img src="images/icoFace.jpg" alt="Facebook"></a></span>
				<span class="icone"><a href="https://www.youtube.com/user/nutracorpore" target="_blank"><img src="images/icoYouTube.jpg" alt="YouTube"></a></span>
			</div>
			<span id="text-news">Novidades, artigos, dicas e ofertas exclusivas, cadastre-se:</span>
			<div id="email-news">
				<form name="news" method="post" action="cadastro.php">
					<input type="text" id="campo" name="email" value="Digite seu e-mail" onfocus="if(this.value == 'Digite seu e-mail'){ this.value=''; }" onblur="if(this.value == ''){ this.value='Digite seu e-mail'; }">
					<input type="submit" id="botao" value="Enviar" >
				</form>
			</div>
		</div>
		<div id="aside-left-bar">
			<div id="navegue-title">Navegue<span id="blue-mark"></span></div>
			<?php carregaCategorias(1); ?>
		</div>
		<div id="products-container">
			<div id="products-title">Mais Vendidos</div>
			<div class="produtos">
				<?php
					$produtos = mysql_query("(select 1 as rank, pproduto, pdescricao,pvalor_unitario,plink_seo,marquivo, tblproduto.produtoid as produtoid, tblmarca.mmarca
					from tblproduto
					left join tblproduto_midia on mcodproduto = produtoid and mprincipal = -1
					left join tblmarca on marcaid = pcodmarca
					where produtoid in ( (select pcodproduto from (select pcodproduto,sum(pquantidade) as quantidade from tblpedido_item group by pcodproduto) as tempDB
					order by quantidade desc)) and pdisponivel = -1 and now() between pdata_inicio and pdata_termino)
					union all
					(select 2 as rank, pproduto,pdescricao,pvalor_unitario,plink_seo,marquivo, tblproduto.produtoid, tblmarca.mmarca
					from tblproduto
					left join tblproduto_midia on mcodproduto = produtoid and mprincipal = -1
					left join tblmarca on marcaid = pcodmarca
					where produtoid in ((select produtoid from tblproduto where pdisponivel = -1 and now() between pdata_inicio and pdata_termino)))
					order by rank");
					$cnt = 0;
					$products = array();
					$valor_total = 0;
					while($produto = mysql_fetch_array($produtos)){
						$estoque = getEstoque($produto['produtoid']);
						if(!in_array('0', $estoque)){
							$compara = strtolower($produto["pproduto"]);
							if(!strstr($compara,"creatina") && !strstr($compara,"carnetina") && !strstr($compara,"carnitine") && !strstr($compara,"polifenol de alcachofra")){
								$cnt++;
								$valor_total += $produto["pvalor_unitario"];
								$products[] = $produto["produtoid"];
					?>
					<a href="<?=$produto["plink_seo"] ?>">
					<div class="produto">
						<span class="product-thumb"><img src="<?php if(file_exists($produto["marquivo"])){ echo $produto["marquivo"]; }else{ echo "imagem/produto/med-indisponivel.png"; } ?>" alt="produto"></span>
						<span class="product-menu-top20-product-title"><?=$produto["pproduto"] ?></span>
						<span class="product-menu-top20-product-mark"><b>+</b> <?=$produto["mmarca"] ?></span>
						<span class="product-menu-top20-product-price">R$ <s><?=number_format($produto["pvalor_unitario"],2,",",".") ?></s></span>
						<span class="par-velue"><?=get_valor_parcelado_new($produto["produtoid"],$produto["pvalor_unitario"])?></span>
						<span class="a-vista-price">Ou R$ <?=number_format($produto["pvalor_unitario"]-($produto["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
						<span class="boleto-text">via boleto</span>
						
					</div>
					</a>
					<?php
							}
							if($cnt == 4){
								break;
							}
						}
					}
				?>
			</div>
			<div id="products-title" style="margin-top: 380px;">Ofertas</div>
			<div class="produtos">
				<?php monta_vitrine(); ?>
			</div>
		</div>
		<div id="aside-right-bar">
			<?php carregaBanners(2); ?>
		</div>
	</div>
        
        
  <div id="footer-container" style="margin-top: 2715px;">
        <?php
            include("inc_footer.php");
        ?>
  </div>
  
</div>


<script type="text/javascript">
var google_tag_params = {
	ecomm_prodid: <?php echo json_encode($products); ?>,
	ecomm_pagetype: 'home',
	ecomm_totalvalue: <?php echo number_format($valor_total, 2, ".", ".");?>,
};
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');


fbq('init', '1096346693782265');
fbq('track', "PageView");
fbq('track', 'ViewContent', {
	content_type: 'product_group',
	content_ids: <?php echo json_encode($products); ?>
});</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1096346693782265&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border­style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970176820/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>




<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>