<?php
	include("include/inc_conexao.php");
	
    $redir			= $_REQUEST["redir"];
	$expires 		= time()+ 60 * 60 * 24 * 60; // 60 dias de cookie	

	$erro			= 	0;
	$id 			= 	0;
	$tipo			=	1;
	$razaosocial 	=	"";
	$nome 			=	"";
	$apelido		=	"";
	$tipo			=	"";
	$cnpj			=	"";
	$ie				=	"";
	$email			=	"";
	$senha			=	"";
	$site			=	"";
	$nascimento		=	"";
	$sexo			=	1;
	$telefone		=	"";
	$celular		=	"";
	$news			=	1;
	$sms			=	1;


	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/

	function validaCPF($cpf = null) {
 
	    // Verifica se um número foi informado
	    if(empty($cpf)) {
	        return false;
	    }
	 
	    // Elimina possivel mascara
	    $cpf = ereg_replace('[^0-9]', '', $cpf);
	    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	     
	    // Verifica se o numero de digitos informados é igual a 11 
	    if (strlen($cpf) != 11) {
	        return false;
	    }
	    // Verifica se nenhuma das sequências invalidas abaixo 
	    // foi digitada. Caso afirmativo, retorna falso
	    else if ($cpf == '00000000000' || 
	        $cpf == '11111111111' || 
	        $cpf == '22222222222' || 
	        $cpf == '33333333333' || 
	        $cpf == '44444444444' || 
	        $cpf == '55555555555' || 
	        $cpf == '66666666666' || 
	        $cpf == '77777777777' || 
	        $cpf == '88888888888' || 
	        $cpf == '99999999999') {
	        return false;
	     // Calcula os digitos verificadores para verificar se o
	     // CPF é válido
	     } else {   
	         
	        for ($t = 9; $t < 11; $t++) {
	             
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }
	            $d = ((10 * $d) % 11) % 10;
	            if ($cpf{$c} != $d) {
	                return false;
	            }
	        }
	 
	        return true;
	    }
	}

	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		$id = $cadastro;
	}

	


	if($_POST){
	
		if($redir==""){
			$redir = "minha-conta.php";
		}
	
		if($_POST["action"]=="gravar"){
			$id				=	$_POST["id"];
			$tipo			=	$_POST["tipo"];
			$razaosocial 	=	addslashes($_POST["txt-razao-social"]);
			$nome 			=	addslashes($_POST["txt-nome"]);
			$apelido		=	addslashes($_POST["txt-apelido"]);
			$cnpj			=	addslashes($_POST["txt-cnpj"]);
			$ie				=	addslashes($_POST["txt-ie"]);
			$email			=	addslashes($_POST["txt-email"]);
			$senha			=	addslashes($_POST["txt-senha"]);
			$site			=	addslashes($_POST["txt-site"]);
			$nascimento		=	addslashes($_POST["txt-data-nascimento"]);
			$sexo			=	$_POST["sexo"];
			$telefone		=	addslashes($_POST["txt-telefone"]);
			$celular		=	addslashes($_POST["txt-celular"]);
			$news			=	$_POST["news"];
			$sms			=	$_POST["sms"];
			
						
			$nascimento 	=	formata_data_db($nascimento);
			
			$telefone		=	get_only_numbers($telefone);
			$celular		=	get_only_numbers($celular);
			$cnpj			=	get_only_numbers($cnpj);
			
			$senha			=	base64_encode($senha);
			
			
			if(!is_numeric($id)){
				$id = 0;	
			}
			if(!is_numeric($tipo)){
				$tipo = 1;	
			}
			if(!is_numeric($sexo)){
				$sexo = 1;	
			}
			if(!is_numeric($news)){
				$news = 0;	
			}
			if(!is_numeric($sms)){
				$sms = 0;	
			}

			//if(validaCPF($cnpj)){

			
				if($id==0){

					//verifica se já existe o email ou cpf
					$ssql = "select cemail from tblcadastro where cemail='{$email}' or ccpf_cnpj='{$cnpj}'";
					$result = mysql_query($ssql);
					if($result){
						$num_rows = mysql_num_rows($result);	
					}
					
					//echo $ssql;
					
					if($num_rows==0){

						$ssql = "insert into tblcadastro (crazao_social, cnome, capelido, ccodtipo, ccpf_cnpj, crg_ie, cemail, csenha, csite, cdata_nascimento, ccodsexo, 
								ctelefone, ccelular, cnews, csms, ccodusuario, cdata_alteracao, cdata_cadastro) VALUES
								('{$razaosocial}', '{$nome}', '{$apelido}', '{$tipo}', '{$cnpj}', '{$ie}', '{$email}', '{$senha}', '{$site}', '{$nascimento}', '{$sexo}', 
								 '{$telefone}', '{$celular}', '{$news}', '{$sms}', 0, '{$data_hoje}', '{$data_hoje}')";
						$ret = mysql_query($ssql);
						//echo $ssql;
						
						if($ret==1){
							$id = mysql_insert_id();						
							$_SESSION["cadastro"]=$id; 
							setcookie("apelido",$apelido,$expires);
							setcookie("cadastro",$id,$expires);
						}else{
							$erro = 2;
							$msg = "Erro ao gravar preencha todos os campos ou tente mais tarde";	
							
							$nascimento 	= formata_data_tela($nascimento);
							$senha			=	base64_decode($senha);
						}
						//echo $ssql;
					}
					else
					{
						$erro = 1;	
						$msg = "O e-mail / cpf-cnpj já está cadastrado em nosso sistema. Utilize o esqueci a senha para reaver sua senha.";	
						
						$nascimento 	= formata_data_tela($nascimento);
						$senha			=	base64_decode($senha);
					}
				
				}
				else
				{
					
					$ssql = "update tblcadastro set crazao_social='{$razaosocial}', cnome='{$nome}', capelido='{$apelido}', crg_ie='{$ie}', csenha='{$senha}', csite='{$site}', 
							cdata_nascimento='{$nascimento}', ccodsexo='{$sexo}', ctelefone='{$telefone}', ccelular='{$celular}', cnews='{$news}', csms='{$sms}', 
							cdata_alteracao='{$data_hoje}' where cadastroid='{$id}'";
					$ret = mysql_query($ssql);

					if($ret==1){
						$_SESSION["cadastro"]=$id; 
						setcookie("apelido",$apelido,$expires);
						setcookie("cadastro",$id,$expires);
					}else{
						$erro = 3;
						$msg = "Erro ao gravar preencha todos os campos ou tente mais tarde";	
						
						$nascimento 	= formata_data_tela($nascimento);
						$senha			=	base64_decode($senha);
					}
					

				}
				/*
			}else{
				$erro = 1;	
				$msg = "O CPF informado é inválido, favor verifique o CPF inserido.";	
			}
			*/
				
				if($erro==0){
					header("location: $redir");
					exit();
				}

		}
		
	
	}

	

	/*-------------------------------------------------------------
	carrega os dados
	-------------------------------------------------------------*/
	$cadastro = $_SESSION["cadastro"];
	$ssql = "select cadastroid, crazao_social, cnome, capelido, ccodtipo, ccpf_cnpj, crg_ie, cemail, csenha, csite, cdata_nascimento, ccodsexo, ctelefone, ccelular, cnews, csms
			from tblcadastro where cadastroid = '{$cadastro}'";
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){
			$tipo			=	$row["ccodtipo"];
			$razaosocial 	=	$row["crazao_social"];
			$nome 			=	$row["cnome"];
			$apelido		=	$row["capelido"];
			$tipo			=	$row["ccodtipo"];
			$cnpj			=	$row["ccpf_cnpj"];
			$ie				=	$row["crg_ie"];
			$email			=	$row["cemail"];
			$senha			=	base64_decode($row["csenha"]);
			$site			=	$row["csite"];
			$nascimento		=	formata_data_tela($row["cdata_nascimento"]);
			$sexo			=	$row["ccodsexo"];
			$telefone		=	$row["ctelefone"];
			$celular		=	$row["ccelular"];
			$news			=	$row["cnews"];
			$sms			=	$row["csms"];
			
		}	
		mysql_free_result($result);
	}
	

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Cadastro</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Cadastro" />
<meta name="description" content="<?php echo $site_nome;?> Cadastro. Efetue seu cadastro e tenha acesso a comprar produto em nossa loja virtual." />
<meta name="keywords" content="<?php echo $site_nome;?> Cadastro" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> Cadastro" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/cadastro.php" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
$(document).ready(function() {	
	$("#txt-telefone").mask("99 9999-9999");
	$("#txt-cnpj").mask("999.999.999-99");
	$("#txt-data-nascimento").mask("99/99/9999");
	$("#txt-razao-social").focus();
	
	
	<?php
		if(strlen($celular)>=11){
			echo '$("#txt-celular").mask("99 9 9999-9999");';
		}
		else
		{
			echo '$("#txt-celular").mask("99 9999-9999");';
		}
	
		if($id>0){
			echo '$("#txt-email").attr("disabled","disabled");';
			echo '$("#txt-email2").attr("disabled","disabled");';
			echo '$("#txt-cnpj").attr("disabled","disabled");';			
			echo '$("#tipo").attr("disabled","disabled");';			
		}
		else
		{
			echo '$("#tipo").attr("checked","checked");';			
			echo '$("#news").attr("checked","checked");';			
			echo '$("#sms").attr("checked","checked");';			
		}
	?>
	

	$("#txt-celular").keypress(function(event) { 
		//var ddd = String(get_only_numbers($('#txt-celular').val()));
		if($(this).val().length >= 12){
			$("#txt-celular").unmask();
			$("#txt-celular").mask("99 99999-9999");
		}else{
			$("#txt-celular").unmask();
			$("#txt-celular").mask("99 9999-9999");
		}
		/*
		if(ddd.substring(0,2)=="11" && ddd.length==3){
			$("#txt-celular").unmask();
			$("#txt-celular").mask("11 9999-9999");
			//$("#txt-celular").val( '11 9____-___' );
	
			var elem = document.getElementById('txt-celular');
			if(elem != null) {
				if(elem.createTextRange) {
					var range = elem.createTextRange();
					range.move('character', 3);
					range.select();
				}
				else {
					if(elem.selectionStart) {
						elem.focus();
						elem.setSelectionRange(3, 10);
					}
					else
						elem.focus();
				}
			}
		}
		*/
	});	
	
});	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
            <form name="frm_cadastro" id="frm_cadastro" method="post" action="cadastro.php" onsubmit="return valida_cadastro();">
            <input type="hidden" name="action" id="action" value="gravar" />
            <input type="hidden" name="id" id="id" value="<?php echo $id;?>" />
            <input type="hidden" name="redir" id="redir" value="<?php echo $redir;?>" />
        
          <div id="mapeamento-pagina"> <a href="index.php?ref=cadastro"><?php echo $site_nome;?></a>&nbsp;/&nbsp;<a href="cadastro.php">Cadastro</a> </div>
          <h2 class="h2-pg-interna">CADASTRO</h2><span class="msg"><?php echo $msg;?></span>
          
          <div id="container-cadastro">
            <div class="campo-cadastro">
                <span class="tit-cat-cadastro">Tipo de Pessoa </span> 
                <label><input type="radio" name="tipo" id="tipo" value="1" class="radio-tipo-cadastro" <?php if($tipo==1){echo ' checked="checked"';}?> onclick="javascript:cadastro_tipo(1);" /><span class="label-radio">Pessoa Física</span></label>
                <label><input type="radio" name="tipo" id="tipo" value="2" class="radio-tipo-cadastro" <?php if($tipo==2){echo ' checked="checked"';}?> onclick="javascript:cadastro_tipo(2);" /><span class="label-radio">Pessoa Juridica</span></label>
            </div>
            
            <div class="campo-cadastro">
                <span class="tit-cat-cadastro">Dados Pessoais*</span>
            </div>
            <div class="campo-cadastro">
                <span class="label-campos" id="lbl-razao">Nome*</span> <input name="txt-razao-social" type="text" class="campos-cadastro" id="txt-razao-social" value="<?php echo $razaosocial;?>" maxlength="100" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos" id="lbl-nome">Sobrenome*</span> <input name="txt-nome" type="text" class="campos-cadastro" id="txt-nome" value="<?php echo $nome;?>" maxlength="100" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Como gostaria de ser chamado*</span> 
                <input name="txt-apelido" type="text" class="campos-cadastro" id="txt-apelido" value="<?php echo $apelido;?>" maxlength="50" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos" id="lbl-cnpj">CPF*</span> 
                <div id="div-txt-cnpj">
                <input name="txt-cnpj" type="text" class="campos-cadastro" id="txt-cnpj" value="<?php echo $cnpj;?>" />
                </div>
            </div>
            <div class="campo-cadastro">
                <span class="label-campos" id="lbl-ie">RG*</span> 
                <div id="div-txt-cnpj">
                <input name="txt-ie" type="text" class="campos-cadastro" id="txt-ie" value="<?php echo $ie;?>" />
                </div>
            </div>            
            <div class="campo-cadastro">
                <span class="label-campos">Data de nascimento*</span><input name="txt-data-nascimento" type="text" class="campos-cadastro" id="txt-data-nascimento" value="<?php echo $nascimento;?>" maxlength="10" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Sexo</span>
                <div style="margin: 5px 0 0 0; float:left;">
                <label><input name="sexo" id="sexo" type="radio" class="radio-tipo-cadastro" value="1" <?php if($sexo==1){echo ' checked="checked"';}?> /><span class="label-radio">Masculino</span></label>
                <label><input name="sexo" id="sexo" type="radio" class="radio-tipo-cadastro" value="2" <?php if($sexo==2){echo ' checked="checked"';}?> /><span class="label-radio">Feminino</span></label>
             	</div>
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Telefone*</span>
                <input name="txt-telefone" type="text" class="campos-cadastro" id="txt-telefone" value="<?php echo $telefone;?>" maxlength="14" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Celular*</span>
                <input name="txt-celular" type="text" class="campos-cadastro" id="txt-celular" value="<?php echo $celular;?>" maxlength="14" />
                <span class="label-campos-exemplo">formato 11 1234-1234</span>
            </div>
            <div class="campo-cadastro">
                <span class="tit-cat-cadastro">Dados de Acesso*</span>
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Email*</span>
                <input name="txt-email" type="text" class="campos-cadastro" id="txt-email" value="<?php if(isset($_POST["txt-email"])){ echo $_POST["txt-email"]; }else{ echo $email; }?>" maxlength="50" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Confirme seu email</span><input name="txt-email2" type="text" class="campos-cadastro" id="txt-email2" value="<?php echo $email;?>" maxlength="50" />
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Senha*</span>
                <input name="txt-senha" type="password" class="campos-cadastro" id="txt-senha" value="<?php echo $senha;?>" maxlength="12" />
                <span class="label-campos-exemplo">mínimo 4 caracteres.</span>
            </div>
            <div class="campo-cadastro">
                <span class="label-campos">Confirme sua Senha</span><input name="txt-senha2" type="password" class="campos-cadastro" id="txt-senha2" value="<?php echo $senha;?>" maxlength="12" />
            </div>
            <div class="campo-cadastro">
                <span class="tit-cat-cadastro">Termos e Avisos</span>
            </div>
            <div class="campo-cadastro">
                <span class="label-cad-news" >Inscreva-se para receber nossas ofertas exclusivas no seu e-mail e celular. Por favor, desmarque as opções abaixo caso não deseje receber e-mails ou comunicados por SMS no seu celular. Conheça nossa <a href="politica-privacidade.php">Politica de Privacidade</a>.</span>
            </div>
            <div class="campo-cadastro">
                <input name="news" type="checkbox" class="check-cadastro" id="news" value="-1" <?php if($news==-1){echo ' checked="checked"';}?> /><span class="label-campos-news">Desejo receber avisos de ofertas por e-mail</span>
            </div>
            <div class="campo-cadastro">
                <input name="sms" type="checkbox" class="check-cadastro" id="sms" value="-1" <?php if($sms==-1){echo ' checked="checked"';}?> /><span class="label-campos-news">Desejo receber avisos de ofertas por SMS no celular</span>
            </div>
            <div class="campo-cadastro">
                &nbsp;
            </div>
        
            
            
            <div id="btn-voltar">
                <a href='minha-conta.php'>Voltar</a>
            </div>
            
            <div id="btn-continuar"><input name="cmd_continuar" type="image" src="images/btn-continuar.jpg" alt="Continuar" /></div>
          </div>
            </form>
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>