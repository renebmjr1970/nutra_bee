<?php
	include("include/inc_conexao.php");	
	include_once("include/inc_funcao.php");	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}	
	$id = addslashes(strtolower($_REQUEST["id"]));
	if(!is_numeric($id)){
		$link_seo = addslashes(strtolower($_SERVER['REQUEST_URI']));
		$link_seo = explode("---",$link_seo);
		$id = $link_seo[1];
	}
	
	if(!is_numeric($id) || $id < 1){	
		$id = get_only_numbers($id);
	}
	$data_hoje = date("Y-m-d");
	$artigo = mysql_fetch_array(mysql_query("select atitulo,adata_cadastro,acategoria,apalavra_chave,aimagem,adescricao,ccategoria,aensaio,aensaio_pasta,data_inicio
	from tblartigos
	left join tblcategoria_artigo on acategoria = categoriaid
	where artigoid = '$id';"), MYSQL_ASSOC);
	
	if($artigo["adata_cadastro"] == ''){
		header('Location: artigos');
	}
	
/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("artigo.php","",$_SERVER['SCRIPT_NAME']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $artigo["atitulo"]; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta content="<?=$artigo["atitulo"] ?>" property="og:title"></meta>
<meta content="Nutra Corpore" property="og:site_name"></meta>
<meta content="<?=$site_site.'/'.str_replace("tumb","big",$artigo["aimagem"]) ?>" property="og:image"></meta>
<meta content="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" property="og:url"></meta>


<link rel="shortcut icon" href="images/favicon.png" type="image/png" />
<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script type="text/javascript" src="fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="fancybox/jquery.fancybox-buttons.js?v=1.0.5"></script>
<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="fancybox/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script type="text/javascript" src="fancybox/jquery.fancybox-media.js?v=1.0.6"></script>


<script type="text/javascript">
<?php if($artigo["aensaio"]){ ?>
	$(document).ready(function() {
		$(".fancybox").click(function() {
				$.fancybox.open([
			    
				<?php
					$ensaio = '';
					$pasta = "imagem/midia/images/".$artigo["aensaio_pasta"];
					$files = glob($pasta.'/*.{jpg,png,gif}', GLOB_BRACE);
					foreach($files as $file) {
						if($ensaio != ''){ $ensaio .= ","; }
						$ensaio .= "{ href : '$file', }";
					}
					echo $ensaio;
				?>
			], {
				helpers : {
					thumbs : {
						width: 75,
						height: 50
					}
				}
			});
		});
	});	
<?php } ?>
</script>
<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="lb"></div>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<div id="header-content">
	<?php
			include("inc_header.php");
	?>
    </div>
	<div id="main-box-container">
		<div id="box-sup-artigo">
			<form name="artigobusca"  method="post" action="busca/" onsubmit="return valida_busca_artigo();">
				<input type="text" value="<?=$string ?>" name="string" id="search-artigo">
				<input type="submit" value="" name="enviar" id="search-artigo-btn">
			</form>
		</div>
    	<div id="container-menu-left" style="width:164px; overflow:visible;">
        	<?php
            	include("inc_left_artigo.php");
			?>
  
	
	</div>
        
        <div class="box-products-container">
            <div id="products-category-box">
				<?php
					if(!is_null($artigo["data_inicio"])){
						$aux = explode("-",$artigo["data_inicio"]);
						$data = $aux[2]."/".$aux[1]."/".$aux[0];
					}else{
						$data = explode(" ",$artigo["adata_cadastro"]);
						$data = explode("-",$data[0]); 
						$data = $data[2]."/".$data[1]."/".$data[0];
					}
				?>
				<span id="artigo-date"><?php echo $data; ?></span>
				<span id="artigo-name"><?=$artigo["atitulo"] ?></span>
				<span id="artigo-published">Publicado em: <span style="color:#f00;"><?=$artigo["ccategoria"] ?></span></span>
				<?php if(file_exists($artigo["aimagem"])){ if($artigo["aensaio"]){ echo '<a href="javascript:void(0)" class="fancybox" >'; } ?><img src="<?=str_replace("tumb","big",$artigo["aimagem"]) ?>" border="0" alt="Banner" width="655" style="display:block;margin-top:20px;"> <?php if($artigo["aensaio"]){ echo '</a>'; } } ?>
				<span id="artigo-text">
					<?php
						echo $artigo["adescricao"];
						if($artigo["aensaio"]){
					?>
					<br><br><span id="artigosChaves"><a href="javascript:void(0)" class="fancybox">Clique aqui para ver as fotos do ensaio</a></span>
					<?php
						}
					?>
				</span>
				<div id="artigo-share">
					<div id="fb-share"><div class="fb-share-button" data-href="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" data-type="button_count"></div></div>
					<div id="fb-like"><div class="fb-like" data-href="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></div>
					<div id="artigoEmail" onclick="javascript:artigo_indique(<?=$id ?>)"></div>
				</div>
				<span id="artigo-more">Mais sobre: <span id="artigosChaves">
				<?php
					$palavras = str_replace(" ,",",",$artigo["apalavra_chave"]);
					$palavras = str_replace(", ",",",$palavras);
					$palavras = explode(",",$palavras);
					for($p=0;$p<sizeof($palavras);$p++){
						if($p){ echo ", "; }
				?>
						<a href="busca_artigo.php?string=<?=$palavras[$p] ?>"><?=$palavras[$p] ?></a>
				<?php
					}
					$hoje = date("Y-m-d");
					$relacionados = mysql_query("select * from((select 1 as rank,artigoid,atitulo,adata_cadastro,aimagem,alink_seo from tblartigos where acategoria = '".$artigo["acategoria"]."' and artigoid <> '$id' and (data_inicio <= '{$hoje}' OR data_inicio IS NULL) order by adata_cadastro desc limit 3)
					union (select 2 as rank,artigoid,atitulo,adata_cadastro,aimagem,alink_seo from tblartigos where artigoid <> '$id' and (data_inicio <= '{$hoje}' OR data_inicio IS NULL) order by adata_cadastro desc limit 3)) as TempDB
					group by artigoid order by rank	limit 3");
					$cont = 1;
					while($relacionado = mysql_fetch_array($relacionados)){
						$dataA[$cont]	= explode(" ",$relacionado["adata_cadastro"]);
						$dataA[$cont]	= explode("-",$dataA[$cont][0]);
						$dataA[$cont]	= $dataA[$cont][2]."/".$dataA[$cont][1]."/".$dataA[$cont][0];
						$imagem[$cont]	= $relacionado["aimagem"];
						if(!file_exists($imagem[$cont])){ $imagem[$cont] = "imagem/produto/tumb-indisponivel.png"; }
						$titulo[$cont]	= $relacionado["atitulo"];
						$link[$cont]	= "artigo/".$relacionado["alink_seo"]."---".$relacionado["artigoid"];
						$cont++;
					}
				?>
				</span></span>
				<div id="products-title" style="clear: both;width: 654px;padding-top: 20px;margin-bottom: 20px;font-family: Oswald-REGULAR;">PODERÁ TAMBÉM GOSTAR DE:</div>
				<table border="0" width="655" style="margin-bottom: 25px;">
					<tr>
						<td width="218" align="left" style="cursor:pointer;" onclick="window.location='<?=$link[1] ?>';">
							<img src="<?=$imagem[1] ?>" width="195"><br>
							<span class="artigo-rel-date"><?=$dataA[1] ?></span>
							<span class="artigo-rel-tit"><?=$titulo[1] ?></span>
						</td>
						<td width="218" align="center" style="cursor:pointer;" onclick="window.location='<?=$link[2] ?>';">
							<img src="<?=$imagem[2] ?>" width="195"><br>
							<span class="artigo-rel-date" style="margin-left: 10px;"><?=$dataA[2] ?></span>
							<span class="artigo-rel-tit" style="margin-left: 10px;"><?=$titulo[2] ?></span>
						</td>
						<td width="218" align="right" style="cursor:pointer;" onclick="window.location='<?=$link[3] ?>';">
							<img src="<?=$imagem[3] ?>" width="195"><br>
							<span class="artigo-rel-date" style="margin-left: 20px;"><?=$dataA[3] ?></span>
							<span class="artigo-rel-tit" style="margin-left: 20px;"><?=$titulo[3] ?></span>
						</td>
					</tr>
				</table>
				
				<div class="fb-comments" data-href="<?='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ?>" data-numposts="5" data-width="655px" style="margin-top: 10px;"></div>
			</div>
        </div>
		<div id="aside-right-bar2" style="margin-left:31px;margin-top:0px;">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>