<?php

	include("include/inc_conexao.php");
	include("include/inc_cadastro.php");
	include("include/inc_frete.php");
	include("include/inc_orcamento.php");
	
	/*--------------------------------------------------------------------------
	 verifica se foi postado pro proprio site pra exitar tentativa de hacker
	---------------------------------------------------------------------------*/
	if($_SERVER['HTTP_REFERER']){
		$refer = $_SERVER['HTTP_REFERER'];
		
		if( strpos($refer,$_SERVER['SERVER_NAME']) && !strpos($refer,"carrinho.php")  ){
			$_SESSION['history_back'] = $refer;
			
		}
		
	}
	
	/*-------------------------------------------------------------------	
	//navegação com ssl
	---------------------------------------------------------------------*/
	if($_SERVER['SERVER_PORT'] != 80){
		header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
		die();
	}
	
	$history_back = $_SESSION['history_back'];
	
	
	$orcamento = 0;
	$cadastro = 0;
	$ip = $_SERVER['REMOTE_ADDR'];
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie


	$cep_destino	= "";
	$frete_gratis 	= -1;
	$peso			= 0;
	$subtotal		= 0;
	$desconto		= 0;
	$frete			= 0;		
	$presente		= 0;
	$presente_cartao= 0;
	$desconto_cupom	= 0;
	$desconto_troca	= 0;
	$total			= 0;
	$valor_desconto_cupom = 0;
	$msg 			= "";

	/*------------------------------------------------------------------------
	verifica se tem algum orcamento em aberto com base em cookies
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["orcamento"])){
		$orcamento = $_COOKIE["orcamento"];
		if(!is_numeric($orcamento)){
			$orcamento = 0;	
		}
	}
	
	
	/*------------------------------------------------------------------------
	verifica se está logado
	--------------------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_COOKIE["cadastro"];
		if(!is_numeric($cadastro) || $cadastro <= 0){
			$cadastro = 0;	
		}
	}
	
	/*if(isset($_COOKIE["orcamento"])){
		mysql_unbuffered_query("update tblorcamento_item set tblorcamento_item.ovalor_unitario = (select tblproduto.pvalor_unitario from tblproduto where tblproduto.produtoid = tblorcamento_item.ocodproduto) where ocodorcamento = ".$_COOKIE["orcamento"].";");
		mysql_unbuffered_query("update tblorcamento_item set oquantidade = 0 where ocodproduto not in (select produtoid from tblproduto) and ocodorcamento = '".$_COOKIE["orcamento"]."';");
		mysql_unbuffered_query("update tblorcamento_item set oquantidade = 0 where ocodproduto in (select produtoid from ((select produtoid from tblproduto where produtoid in (select ocodproduto from tblorcamento_item where ocodorcamento = '".$_COOKIE["orcamento"]."') and ((pdisponivel = 0) or (now() not between pdata_inicio and pdata_termino))) as TempDB)) and ocodorcamento = '".$_COOKIE["orcamento"]."';");
		mysql_unbuffered_query("update tblorcamento_item set oquantidade = 0 where ocodproduto in (select produtoid from ((select produtoid,pcontrola_estoque from tblproduto left join tblestoque on produtoid = ecodproduto where produtoid in (select ocodproduto from tblorcamento_item where ocodorcamento = '".$_COOKIE["orcamento"]."') HAVING (((SUM(tblestoque.eestoque) = 0 || SUM(tblestoque.eestoque) is null) && tblproduto.pcontrola_estoque = -1))) as TempDB)) and ocodorcamento = '".$_COOKIE["orcamento"]."';");
	}*/

	/*------------------------------------------------------------------------
	verifica se ja tem cookie com o cep de destino
	--------------------------------------------------------------------------*/	
	if(isset($_COOKIE["cep_destino"])){
		$cep_destino = 	$_COOKIE["cep_destino"];			  
	}


	/*------------------------------------------------------------------------
	verifica se ja tem cookie com o cupom de desconto
	--------------------------------------------------------------------------*/	
	if(isset($_COOKIE["cupom_desconto"])){
		$cupom = $_COOKIE["cupom_desconto"];	
	}


	/*--------------------------------------------------------------------------
	exclui produtos que estavam no carrinho e agora não tem mais estoque
	ex. pessoa comecou a comprar parou no dia seguinte volta pra finalizar
	---------------------------------------------------------------------------*/
	$permite_estoque_negativo = intval(get_configuracao("permite_estoque_negativo"));
	if( $permite_estoque_negativo != 0 ){
		$ssql = "select tblorcamento_item.ocodproduto, tblorcamento_item.ocodpropriedade, tblorcamento_item.ocodtamanho, tblestoque.eestoque, 
				tblproduto.pcontrola_estoque, tblproduto.pdisponivel
				from tblorcamento_item
				inner join tblproduto on tblorcamento_item.ocodproduto = tblproduto.produtoid
				inner join tblestoque on tblorcamento_item.ocodproduto = tblestoque.ecodproduto and tblorcamento_item.ocodtamanho = tblestoque.ecodtamanho 
				and tblorcamento_item.ocodpropriedade = tblestoque.ecodpropriedade
				where tblorcamento_item.ocodorcamento='{$orcamento}' 
				and tblorcamento_item.oquantidade > 0 
				and tblproduto.pcontrola_estoque = -1";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){			
				if( intval($row["eestoque"]) == 0 || intval($row["pdisponivel"]) == 0 ){
					$ssql = "update tblorcamento_item set oquantidade = 0 where ocodorcamento = '{$orcamento}' 
								and  ocodproduto = " . $row["ocodproduto"] . " 	
								and  ocodpropriedade = " . $row["ocodpropriedade"] . "
								and  ocodtamanho = " . $row["ocodtamanho"] . "
								";
					mysql_query($ssql);
				}			
			}
			mysql_free_result($result);
		}	
	}

		
	
	/*------------------------------------------------------------------------
	insere o orcamento
	--------------------------------------------------------------------------*/
	if($_POST){
		if($_POST["action"]=="incluir"){
			
			$lista_presente = intval($_REQUEST['lista_presente']);
			$http_referer 	= $_SESSION['HTTP_REFERER'];
			
			if( $orcamento == 0 ){
				
				$ssql = "insert into tblorcamento (ocodcadastro, oip, ocodlista_presente, oreferencia, odata_alteracao, odata_cadastro) 
						values('{$cadastro}','{$ip}','{$lista_presente}','{$http_referer}','{$data_hoje}','{$data_hoje}')";
				$ret = mysql_query($ssql);
				$orcamento = mysql_insert_id();
				
				mysql_query("update tblorcamento set ocodigo='{$orcamento}' where orcamentoid='{$orcamento}'");
				
				//grava o cookie com o orcamento 
				setcookie("orcamento",$orcamento,$expires);
				
			}
			else
			{
				$ssql = "update tblorcamento set ocodlista_presente='{$lista_presente}', odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";	
				mysql_query($ssql);		
			}
		}
	}




	/*------------------------------------------------------------------------
	insere o item no orcamento
	--------------------------------------------------------------------------*/	
	if(isset($_POST["action"])){
		
		$id				= $_REQUEST["id"];
		$produtoid 		= $_REQUEST["produtoid"];
		$tamanho		= $_POST["tamanho"];
		$propriedade	= $_POST["propriedade"];	
		$qtde			= intval($_POST["qtde"]);
		$qtde_update	= intval($_POST["qtde"]);
		
		if($propriedade < 0){
			$propriedade = 0;	
		}
		
		if($qtde==0){
			$qtde = 1;	
		}

		$controla_estoque	= intval(get_produto($produtoid, "pcontrola_estoque"));
		$estoque			= intval(get_estoque($produtoid,$tamanho,$propriedade));
		
		if( $controla_estoque == -1 && $estoque < $qtde){
			$qtde = $estoque;
		}
		if( $qtde < 0 ){
			$qtde = 0;	
		}

		//$frete_gratis 	= -1;
		
		
		
		
		if($_POST["action"]=="finalizar"){
			
			$texto_presente	= addslashes(left($_REQUEST['lista_presente_texto'],300));
			$texto_note = addslashes($_REQUEST["note_texto"]);
			
			$ssql = "update tblorcamento set otexto_presente='{$texto_presente}', onote='{$texto_note}', odata_alteracao='{$data_hoje}' where orcamentoid='{$orcamento}'";
			mysql_query($ssql);
				
			header("location: endereco.php");
			die();
		}
		

		
		if($_POST["action"]=="incluir"){
							
			//desabilita itens ja existentes
			$ssql = "update tblorcamento_item set oquantidade=0 where ocodorcamento='{$orcamento}' and ocodproduto='{$produtoid}' and ocodtamanho='{$tamanho}' and ocodpropriedade='{$propriedade}'";
			mysql_query($ssql);
			
			//pega o valor do produto e o peso
			$pesoproduto = get_produto($produtoid,"ppeso");
			$valorunitario = get_produto($produtoid,"pvalor_unitario");
			$valordesconto =0;
			if( isset($_SESSION["utm_source"]) ){
				$desconto_utm =  get_desconto_utm_source($produtoid);
				$valorunitario = number_format($valorunitario - $desconto_utm,2,".","");
				
				if(floatval($desconto_utm)>0){
					$valordesconto = number_format($desconto_utm,2,'.','');
					
					//echo $valordesconto;
					
					$log = date("Y-m-d H:i:s") . " - Orçamento # ".$orcamento." - Desconto com base em regra de desconto utm_source # ".$_SESSION["utm_source"].".\r\n";
					gera_log($log);
				}
				
			}
			
			//inclui item
			$ssql = "insert tblorcamento_item (ocodorcamento, ocodigo, ocodproduto, ocodpropriedade, ocodtamanho, oquantidade, ovalor_unitario, opeso, odesconto, opresente, odata_alteracao, odata_cadastro )
					values('{$orcamento}','{$orcamento}','{$produtoid}','{$propriedade}','{$tamanho}','{$qtde}','{$valorunitario}','{$pesoproduto}','{$valordesconto}','0','{$data_hoje}','{$data_hoje}')";
					
					
			mysql_query($ssql);	
			
			//atualiza o desconto no prcamento
			if( $valordesconto ){
			//	$ssql = "update tblorcamento set ovalor_desconto=ovalor_desconto+$valordesconto where orcamentoid='{$orcamento}'";
			//	mysql_query($ssql);	
			//	echo $ssql;
			}
		
		}
		


		if($_POST["action"]=="atualizar-item"){
			//atualiza itens ja existentes
			$ssql = "update tblorcamento_item set oquantidade='{$qtde_update}' where ocodorcamento='{$orcamento}' and itemid='{$id}'";
			mysql_query($ssql);		
		}	

		
		
		if($_POST["action"]=="excluir"){
			//desabilita itens ja existentes
			$ssql = "update tblorcamento_item set oquantidade=0 where ocodorcamento='{$orcamento}' and itemid='{$id}'";
			mysql_query($ssql);		
		}	



		if($_POST["action"]=="calculafrete"){
			$cep_destino = $_REQUEST["cep"];	
			setcookie("cep_destino",$cep_destino,$expires);					
		}
		


		if($_POST["action"]=="calculacupomdesconto" || $cupom!=""){
			$cupom = addslashes($_REQUEST["cupom"]);
			setcookie("cupom_desconto",$cupom,$expires);	
		}



		if($_POST["action"]=="calculabonus"){		
		
		}

	}
	

	/*--------------------------------------------------------------------------------
	verifica se so tem item de frete gratis na cesta de compra
	----------------------------------------------------------------------------------*/
	$subtotal = 0;
	$total = 0;
	$peso = 0;
	$items = 0;
	$ssql = "select tblorcamento_item.itemid, tblorcamento_item.ocodproduto, tblorcamento_item.ocodpropriedade, tblorcamento_item.ocodtamanho, tblorcamento_item.oquantidade, 
			tblorcamento_item.ovalor_unitario, tblorcamento_item.opeso, tblproduto.pfrete_gratis
			from tblorcamento_item
			inner join tblproduto on tblorcamento_item.ocodproduto = tblproduto.produtoid
			where tblorcamento_item.ocodorcamento='{$orcamento}' and tblorcamento_item.oquantidade > 0";
			
			//echo $ssql;
			
	$result = mysql_query($ssql);
	if($result){
		while($row=mysql_fetch_assoc($result)){			
			if($row["pfrete_gratis"]==0){
				$frete_gratis	= $row["pfrete_gratis"];
				$frete			= 0;
			}
			$peso = $peso + ($row["oquantidade"]*$row["opeso"]);
			$subtotal = $subtotal + ($row["oquantidade"]*$row["ovalor_unitario"]);
			$total = $total + ($row["oquantidade"]*$row["ovalor_unitario"]);
			$items++;
		}
		mysql_free_result($result);
	}	
	
		
	/*-----------------------------------------------------------------------------------
	verifica se tem desconto configurado com frete gratis
	-------------------------------------------------------------------------------------*/
	$cfrete = new Frete();
	$frete_gratis = $cfrete->is_frete_gratis("0");	

	//$frete_gratis = -1;
	
	if($frete_gratis==0){
		$valor_frete 	= calcula_frete();
	}

	 //echo calcula_frete();
	
	
	/*-----------------------------------------------------------------------------------
	verifica se tem desconto configurado com frete gratis
	-------------------------------------------------------------------------------------*/
	if($cupom != "" && $orcamento!=""){
		$valor_desconto_cupom = calcula_cupom($cupom, $orcamento);
		//echo $valor_desconto_cupom;
		//die();		
	}


	
	/*--------------------------------------------------------------------------------
	//atualiza os totais do carrinho
	----------------------------------------------------------------------------------*/
	if($orcamento>0){	

		$ssql = "select osubtotal, ovalor_desconto, ovalor_frete, ovalor_presente, ovalor_presente_cartao, ovalor_desconto_cupom, ovalor_desconto_troca, ovalor_total,
				otexto_presente, onote
				from tblorcamento where orcamentoid='{$orcamento}' ";
		$result = mysql_query($ssql);		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$desconto			=	0;
				$presente			=	$row["ovalor_presente"];
				$presente_cartao	=	$row["ovalor_presente_cartao"];
				$desconto_troca		=	$row["ovalor_desconto_troca"];
				$texto_presente		=	$row["otexto_presente"];
				$texto_note			=	$row["onote"];
			}
			mysql_free_result($result);
		}
		
		$subtotal = get_carrinho_subtotal($orcamento);

		if($subtotal==0){
			$valor_frete=0;
			$total=0;
		}
		
		if($valor_frete==""){
			$valor_frete = 0;	
		}
		
		$total = $subtotal + $valor_frete + $presente + $presente_cartao  - ($desconto + $valor_desconto_cupom + $desconto_troca);
		$total = number_format($total,2,".","");
		
		$ssql = "update tblorcamento set osubtotal='{$subtotal}', ovalor_desconto='{$desconto}', ovalor_frete='{$valor_frete}', ovalor_presente='{$presente}', 
				ovalor_presente_cartao='{$presente_cartao}', ovalor_desconto_cupom='{$valor_desconto_cupom}', ovalor_desconto_troca='{$desconto_troca}',
				ovalor_total='{$total}' where orcamentoid='{$orcamento}'";
		mysql_query($ssql);	
		
		//echo $ssql;
	}


	
function calcula_frete(){
	
		$cep_destino = $GLOBALS["cep_destino"];
		$peso = $GLOBALS["peso"];
		$subtotal = $GLOBALS["subtotal"];
		
		//seleciona os frete_tipo da tbl	
		$t = array();
		$ssql = "select freteid, fcodigo, fdescricao, fcalcular from tblfrete_tipo where fativo=-1 and fcalcular=-1";
		$result = mysql_query($ssql);
		$num_rows = mysql_num_rows($result);
		$i = 0;
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$tipo = $row["fdescricao"].'||'.$row["fcodigo"]."||".$row["fcalcular"];
				$t[$i] = $tipo;
				$i++;
			}	
			mysql_free_result($result);
		}
		
		$cfrete = new Frete();
		$cfrete->codservico  = $t;
		$cfrete->valor_declarado = $subtotal;
		$cfrete->peso = $peso;
		$cfrete->cep_destino = $cep_destino;
		
		$cfrete->caCalculaFrete();
		if($cfrete->erro!=0){
			$sfrete = 0;
			$GLOBALS["cep_destino"] = "";
			//echo $cfrete->erro_msg;
		}else{
			$sfrete = $cfrete->valor_frete;				
		}
		
		//echo $sfrete . " zzz   ";
		
		$vfrete = explode("||",$sfrete);
		$count = count($vfrete);
		
		// for($n=0;$n<$count;$n++){
			
		// 	if($n==0){
		// 		$sfrete = $vfrete[$n];	
		// 	}
			
		// 	if($vfrete[$n]<=$sfrete && $vfrete[$n] != "" ){
		// 		$sfrete = $vfrete[$n];	
		// 	}
		// }
		
		$sfrete = $vfrete[0];

		if($sfrete==-1){
			$sfrete=0;
			$GLOBALS["frete_gratis"] = -1;
		}
		
		if(!is_numeric($sfrete)){
			//$sfrete = 0;	
		}
				

		$sfrete = str_replace(",",".",$sfrete);
		$sfrete = $sfrete;

		return $sfrete;
}	



function calcula_cupom($cupom, $orcamento){
	
	
	$valida_usuario = false;
	$valida_produto = false;
	
	$cadastro_email = $_SESSION["cadastro_email"];
	$valor_desconto_cupom = 0;
	$cupomid = 0;
	$site_email = $GLOBALS["site_email"];
	
	$subtotal = get_carrinho_subtotal($orcamento);
	$subtotal = number_format($subtotal,2,".","");
	
	//verifica se tem o cupom no banco de dados
	$ssql = "select cupomid, cemail, ccodtipo, ccodfiltro, cdesconto, cdata_validade, cstatus, ccodproduto, ccodcategoria, tipo_cupom, quantidade_geral, quantidade_geral_cliente, desconto_por from tblcupom_desconto 
			where ccupom='{$cupom}' and cstatus='-1' and cdata_validade >='".date("Y-m-d H:i:s")."' OR cdata_validade = '0000-00-00 00:00:00'
			";
	//echo $ssql;
	
	$result = mysql_query($ssql);
	if($result){
		
		if(mysql_num_rows($result)==0){
			$GLOBALS["msg"] = 'Cupom inválido, verifique as condições de uso do cupom.';
			setcookie("cupom_desconto","",time()-3600);	
		}
		
		while($row=mysql_fetch_assoc($result)){
			$tipo_cupom = $row["tipo_cupom"];
			$cupomid    = $row["cupomid"];
			$quantidade_geral = $row["quantidade_geral"];
			$quantidade_geral_cliente = $row["quantidade_geral_cliente"];
			$desconto_por = $row["desconto_por"];
			$desconto 	= $row["cdesconto"];
			$tipo_desconto		= $row["ccodtipo"];
		}
		
		/* Desconto para todos clientes */
		if($tipo_cupom == 1){
			$results_total = mysql_query("SELECT * FROM tblpedido WHERE pcodcupom_desconto = ".$cupomid);
			$total_cupom = mysql_num_rows($results_total);
			
			$results_total = mysql_query("SELECT * FROM tblpedido WHERE pcodcupom_desconto = ".$cupomid." AND pcodcadastro = ".$_SESSION["cadastro"]);
			$total_cliente = mysql_num_rows($results_total);
			
			if($total_cupom < $quantidade_geral && $total_cliente < $quantidade_geral_cliente){
				$valida_usuario = true;
			}else{
				$valida_usuario = false;
			}
			
		/* Desconto Individual */
		}else{
			$results_total = mysql_query("SELECT * FROM tblpedido WHERE pcodcupom_desconto = ".$cupomid." AND pcodcadastro = ".$_SESSION["cadastro"]);
			$total_cupom = mysql_num_rows($results_total);
			
			$total_por_cliente = mysql_query("SELECT quantidade FROM tblcupomdesconto_usuario WHERE idcupom = ".$cupomid." AND email = '".$_SESSION["cadastro_email"]."'");
			while($ln=mysql_fetch_assoc($total_por_cliente)){
				$total_cliente = $ln["quantidade"];
			}
			
			if($total_cupom < $total_cliente){
				$valida_usuario = true;
			}else{
				$valida_usuario = false;
			}
		}
		$valor_desconto_cupom = 0;
		/* Produto grátis */
		if($tipo_desconto == 3){

			$produto = mysql_query("SELECT * FROM produtogratuito WHERE idCupom = ". $cupomid);
			$prod = mysql_fetch_assoc($produto);

			mysql_query("INSERT INTO tblorcamento_item (ocodorcamento, ocodigo, ocodproduto, ovalor_unitario, oquantidade) VALUES ('".$_COOKIE["orcamento"]."', '".$_COOKIE["orcamento"]."', '".$prod["idProduto"]."', 0, 1) ");
			
			$valida_produto = true;
		
		/* Desconto por Categoria */
		}elseif($desconto_por == 2){
			$array_produto = array();
			$categoria_cupom = mysql_query("SELECT * FROM tblcupomdesconto_categoria WHERE idcupom = ".$cupomid);
			while($ln=mysql_fetch_assoc($categoria_cupom)){
				$orcamento_item = mysql_query("select ocodproduto, oquantidade, ovalor_unitario from tblorcamento_item where ocodorcamento='{$_COOKIE["orcamento"]}' and oquantidade>0");
				while($ln2=mysql_fetch_assoc($orcamento_item)){
					$produto_categoria = mysql_query("SELECT * FROM tblproduto_categoria WHERE pcodproduto = ".$ln2["ocodproduto"]." AND pcodcategoria = ".$ln["idcategoria"]);
					if(mysql_num_rows($produto_categoria) > 0){
						if(!in_array($ln2["pcodproduto"], $array_produto)){
							$total_produto  = $ln2["ovalor_unitario"] * $ln2["oquantidade"];				
							$total_desconto = $desconto * $ln2["oquantidade"];
							
							if($tipo_desconto == 1){
								$valor_desconto_cupom += ($total_produto * $total_desconto/100);
							}else{
								$valor_desconto_cupom += $total_desconto;
							}
							$array_produto[] = $ln2["pcodproduto"];
						}
						$valida_produto = true;
					}
				}
			}
			
		/* Desconto por Produto */
		}elseif($desconto_por == 3){
			$qr = mysql_query("SELECT * FROM tblcupomdesconto_produto WHERE idcupom = ".$cupomid);
			while($ln = mysql_fetch_assoc($qr)){
				$orcamento_item = mysql_query("select ocodproduto, oquantidade, ovalor_unitario from tblorcamento_item where ocodorcamento='{$_COOKIE["orcamento"]}' and ocodproduto='{$ln["idproduto"]}' and oquantidade>0");
				if(mysql_num_rows($orcamento_item) > 0){
					while($ln=mysql_fetch_assoc($orcamento_item)){
						$total_produto  = $ln["ovalor_unitario"] * $ln["oquantidade"];
						$total_desconto = $desconto * $ln["oquantidade"];
						if($tipo_desconto == 1){
							$valor_desconto_cupom += ($total_produto * $total_desconto/100);
						}else{
							$valor_desconto_cupom += $total_desconto;
						}
					}
					$valida_produto = true;
				}
			}
			
		/* Desconto em todos os Produtos */
		}elseif($desconto_por == 1){
			$valida_produto = true;
			$orcamento = mysql_query("SELECT * FROM tblorcamento WHERE orcamentoid = '{$_COOKIE["orcamento"]}'");
			while($ln = mysql_fetch_assoc($orcamento)){
				if($tipo_desconto == 1){
					$valor_desconto_cupom += ($ln["osubtotal"] * $desconto/100);
				}else{
					$valor_desconto_cupom += $desconto;
				}
			}
		}
	}

	if($valida_produto && $valida_usuario){
		//atualiza o codigo do cupom na tblorcamento
		$ssql = "update tblorcamento set ocodcupom_desconto='{$cupomid}' where orcamentoid='{$_COOKIE["orcamento"]}'";
		mysql_query($ssql);
		$orcamento = mysql_query("SELECT * FROM tblorcamento WHERE orcamentoid = '{$_COOKIE["orcamento"]}'");
		while($ln=mysql_fetch_assoc($orcamento)){
			$desconto_final = $valor_desconto_cupom;
		}
		return number_format($desconto_final,2,".","");
	}else{
		$_GLOBALS["msg"] = 'Cupom inválido, verifique as condições de uso do cupom.';
		setcookie("cupom_desconto","",time()-3600);
		return 0;
	}
}
	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Carrinho de Compras</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> carrinho de Compras" />
<meta name="description" content="<?php echo $site_descricao;?>" />
<meta name="keywords" content="<?php echo $site_nome;?> Carrinho de Compras" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Carrinho de Compras" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/carrinho.php" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery3.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$('#cep-carrinho').mask('99999-999');
		
		$('#cep-carrinho').keypress(function(e){ 
			if(e.which == 13){ 
				carrinho_calcula_frete();
			} 
		}); 
		
		$("#cupom-desconto-field").keypress(function(e){ 
			if(e.which == 13){ 
				carrinho_calcula_cupom_desconto();
			} 
		}); 
		
		
		$("#lista_mensagem").keypress(function(event){
			var key = event.which;
			//todas as teclas incluindo enter
			//if(key >= 33 || key == 13) {
				var maxLength = 300;
				var length = this.value.length;
				
				$("#total_carcteres").html(length);
				
				if(length >= maxLength) {
					event.preventDefault();
				}
			//}		
		});
		
		
		$(".atualiza-qtde").click(function(){
			var id = $(this).attr("rel");
			var produto = $(this).attr("produto");
			
			atualizar_qtde_carrinho(id, produto);
			
		});
		
		
		$("#box-observacao").click(function(){
			$("#box-carrinho-observacao").show(1000);
			$("#carrinho_note").focus();
		});
		
		<?php
			if( trim($texto_note) != "" ){
				echo '$("#box-carrinho-observacao").show();';
				echo "\r\n";
			}
			
			if( trim($texto_presente) != "" ){
				echo '$("#box-carrinho-mensagem-presente").show();';
				echo "\r\n";				
			}
			
		?>
		
		
		$(".radio-tipo-cadastro").click(function(){
			var id = $(this).attr("value");
			var tipo = ($(this).attr("checked")=="checked") ? "-1" : "0";
		
			atualizar_carrinho_item_presente(id, tipo);
			
			if( tipo == -1){
				$("#box-carrinho-mensagem-presente").show(1000);	
			}
		});
		
		
		$(".btn-finaliza-compra").click(function(){
			$("#action").attr("value","finalizar");
			$("#lista_presente_texto").attr("value", $("#lista_mensagem").attr("value") );
			$("#note_texto").attr("value", $("#carrinho_note").attr("value") );
			$("#frm_carrinho").submit();
		});
		
		
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div style="display:none;"><?php print_r($_POST); ?></div>
<div id="global-container">
	<div id="header-content">
    	<div id="box-efeito-left"></div>
    	<?php
			include("inc_header.php");
			if($cart_items>0 && isset($_COOKIE["orcamento"])){
				//mysql_unbuffered_query("update tblorcamento_item set tblorcamento_item.ovalor_unitario = (select tblproduto.pvalor_unitario from tblproduto where tblproduto.produtoid = tblorcamento_item.ocodproduto) where ocodorcamento = ".$_COOKIE["orcamento"].";");
			}
		?>
	</div>
    
	<div id="main-box-container">
		<div id="products-title">Carrinho
			<div style="float:right;">
				<a href="<?php echo $history_back;?>"><img src="images/continuarComprando.jpg"></a>
				<a href="endereco.php" style="margin-right: 10px;"><img src="images/finalizarCompra.jpg"></a>
			</div>
		</div>
        <div style="float:left; width:100%;">
            <form name="frm_carrinho" id="frm_carrinho" method="post" action="carrinho.php">
                <input type="hidden" name="action" id="action" value="" />
                <input type="hidden" name="id" id="id" value="0" />
				
				<input type="hidden" name="tamanho" id="tamanho" value="0" />
				<input type="hidden" name="propriedade" id="propriedade" value="0" />
				
                <input type="hidden" name="produtoid" id="produtoid" value="0" />
                <input type="hidden" name="qtde" id="qtde" value="0" />
                <input type="hidden" name="peso" id="peso" value="<?php echo $peso_total;?>" />
                <input type="hidden" name="cep" id="cep" value="" />
                <input type="hidden" name="cupom" id="cupom" value="<?php echo $cupom;?>" />
                <input type="hidden" name="valor_frete" id="valor_frete" value="<?php echo $valor_frete;?>" size="5" />
                <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotal;?>" />
                <input type="hidden" id="valor_total" value="<?php echo $valor_total;?>" size="5" />
                <input type="hidden" name="lista_presente_texto" id="lista_presente_texto" value="<?php echo $texto_presente;?>" />
                <input type="hidden" name="note_texto" id="note_texto" value="<?php echo $texto_note;?>" />
            </form>
		</div>    
    
        <div id="container-carrinho">
            <div id="box-header-carrinho">
                <span id="tit-produto-carrinho" class="tit-bar-text-cart-box">Produto</span>
                <span id="preco-produto-carrinho" class="tit-bar-text-cart-box">Quantidade</span>
				<span id="qtde-produto-carrinho" class="tit-bar-text-cart-box">Preço unitário</span>
                <span id="total-produto-carrinho" class="tit-bar-text-cart-box">Subtotal</span>
            </div>
            
        <div id="box-relacao-produtos">

           <?php
		  	$subtotal = 0;

			$ssql = "select tblorcamento_item.itemid, tblorcamento_item.ocodproduto, tblorcamento_item.ocodpropriedade, tblorcamento_item.ocodtamanho, 
					tblorcamento_item.oquantidade, tblorcamento_item.ovalor_unitario, tblorcamento_item.opeso, tblorcamento_item.opresente,
					tblproduto.pproduto, tblproduto.plink_seo, tblmarca.mmarca, 
					tblproduto_propriedade.ppropriedade as ptamanho, 
					tb_propriedade.ppropriedade,
					
					tblproduto_midia.marquivo 
					from tblorcamento_item 
					inner join tblproduto on tblorcamento_item.ocodproduto=tblproduto.produtoid left 
					join tblmarca on tblproduto.pcodmarca=tblmarca.marcaid 
					left join tblproduto_propriedade on tblorcamento_item.ocodtamanho=tblproduto_propriedade.propriedadeid 
					
					left join tblproduto_propriedade as tb_propriedade on tblorcamento_item.ocodpropriedade=tb_propriedade.propriedadeid
					
					left join tblproduto_midia on tblproduto.produtoid=tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal=-1
					where ocodorcamento='{$orcamento}' and tblorcamento_item.oquantidade >0 
					group by tblorcamento_item.ocodproduto, tblorcamento_item.ocodpropriedade, tblorcamento_item.ocodtamanho, tblorcamento_item.oquantidade, 
					tb_propriedade.ppropriedade";

					//echo $ssql;
					
			$result = mysql_query($ssql);
			if($result){				
				$products = array();
				$valor_total = 0;
				while($row=mysql_fetch_assoc($result)){
					
					$id 		= $row["itemid"];
					$produtoid	= $row["ocodproduto"];
					$products[] = $row["ocodproduto"];
					$produto	= $row["pproduto"];
					$link_seo 	= $row["plink_seo"];
					$marca		= $row["mmarca"];
					$tamanho	= $row["ptamanho"];	// $row["ppropriedade"];
					$propriedade= $row["ppropriedade"];
					$imagem 	= $row["marquivo"];
					$valor_total += $row["ovalor_unitario"];
					$valor 		= number_format($row["ovalor_unitario"],2,',','.');
					$qtde 		= number_format($row["oquantidade"],0,',','.'); 
					$peso_total	= $peso_total + ($row["oquantidade"]*$row["opeso"]);

					$presente	= $row["opresente"];
					
					$imagem = str_replace("tumb","big",$imagem);
					
					$subtotal	=  number_format($row["oquantidade"] * $row["ovalor_unitario"],2,',','.'); 

					$subtotalgeral += $row["oquantidade"] * $row["ovalor_unitario"];

					//echo $subtotalgeral."<br>";


					//$valor_total=  $valor_total + ($row["oquantidade"] * $row["ovalor_unitario"]);
					
					if($tamanho!="" && $tamanho != "Unico"){
						$tamanho = " - " . $tamanho;
					}else{
						$tamanho = "";	
					}
					
					if($propriedade!=""){
						$propriedade = " - " . $propriedade;
					}
					
					
					if(!file_exists($imagem)){
						$imagem = "imagem/produto/med-indisponivel.png";		
					}		
					
           ?>
			<div class="itens-carrinho" id="item<?php echo $id;?>">
				<a href="<?php echo $link_seo;?>">
                <img src="<?php echo $imagem;?>" width="135" height="135" border="0" alt="<?php echo $produto;?>" title="<?php echo $produto;?>" class="img-produto-carrinho"/>
                </a>
                <div class="box-desc-produto">
                    <span class="desc-produto-carrinho" style="font-family:Gudea;color:#000;font-size:16px;"><?php echo $produto . "&nbsp;&nbsp;" . $tamanho . "&nbsp;&nbsp;" . $propriedade;?></span>
                    <span class="fabr-produto-carrinho" style="font-family:Gudea;color:#000;font-weight:bold;font-size:16px;"><?php echo $marca;?></span>
                </div>
				<div id="produto-id-<?php echo $id;?>" class="box-qtde-produto-carrinho" style="width:110px; height:50px; float:left; padding-left: 125px;">
                    <input name="qtde-produto-carrinho-<?php echo $id;?>" id="qtde-produto-carrinho-<?php echo $id;?>" type="text" class="qtde-item-carrinho" value="<?php echo $qtde;?>" maxlength="3" />
                    <div class="qtde-produto-carrinho-aumentar">
						<img src="images/atualizarCarrinho.jpg" class="atualiza-qtde" rel="<?php echo $id;?>" produto="<?php echo $produtoid;?>" width="59" height="29" alt="Aumentar" title="Aumentar" border="0" />
					</div>
               		<div id="produto-refresh-<?php echo $produtoid;?>" class="qtde-produto-carrinho-refresh">&nbsp;</div>
                </div>
				<span class="valor-produto-carrinho"> <span style="font-size:12px">R$</span> <?php echo $valor;?> </span>
                <span class="total-produto-carrinho"> <span style="font-size:12px">R$</span> <span id="vlrsub" style="color: #666;font-family: Oswald-REGULAR;font-size: 23px;"><?php echo $subtotal;?> </span></span>
                <a href="javascript:void(0);" onclick="javascript:excluir_item_carrinho(<?php echo $id;?>,<?php echo $produtoid;?>);" id="del<?php echo $id;?>">
                  <img src="images/ico-excluir.jpg" border="0" alt="Excluir" title="Excluir" class="btn-excluir-prod"/>
                </a>
            </div>
            <?php
				}

				$valor_desconto_cupom = number_format($valor_desconto_cupom,2,',','.');
				$peso_total = number_format($peso_total,2,',','.');
				$valor_frete = number_format($valor_frete,2,',','.');
								
				mysql_free_result($result);
			}
			?> 
            
        </div>
        
        <div id="box-dados-compra">
			<table border="0" style="margin-left:20px;">
				<tr>
					<td style="font-family: Oswald-REGULAR;float: right;">
						Vale-presente / Cupom de desconto (opcional)
					</td>
					<td>
						<input type="text" name="cupom-desconto" id="cupom-desconto-field" value="<?php echo $cupom;?>" maxlength="35"/>
					</td>
					<td>
						<a href="javascript:void(0);" onclick="javascript:carrinho_calcula_cupom_desconto();">
							<img src="images/btn-calc-desconto.jpg" alt="calcular desconto" border="0" title="calcular desconto" id="btn-calcula-desconto" />
						</a>
					</td>
					<td style="font-family: Oswald-REGULAR;padding-left: 20px;">DESCONTO: <span style="color:#a2a2a2;margin-left:10px;font-family:Oswald-REGULAR;">R$<?php echo $valor_desconto_cupom;?></span></td>
					<td rowspan="2" style="border-right: 1px solid #bcbcbc;width: 40px;">&nbsp;</td>
					<td style="font-family: PTSans;padding-left: 40px;font-size: 20px;">Total:</td>
				</tr>
				<tr>
					<td style="font-family: Oswald-REGULAR;float: right;">
						Digite o CEP para calcular o frete:
					</td>
					<td>
						<input name="cep-carrinho" type="text" id="cep-carrinho" value="<?php echo $cep_destino;?>" maxlength="9"/>
					</td>
					<td>
						<a href="javascript:void(0);" onclick="javascript:carrinho_calcula_frete();">
							<img src="images/btn-calcula-frete.jpg" alt="calcular frete" border="0" title="calcular frete" id="btn-calcula-frete" margin="0"/>
						</a>
					</td>
					<td style="font-family: Oswald-REGULAR;padding-left: 20px;">FRETE: <span style="color:#a2a2a2;margin-left:45px;font-family:Oswald-REGULAR;">
						<?php 
							//echo $cep_destino."a".$frete_gratis."x".floor($valor_frete)."y";
							if($cep_destino=="" && floor($valor_frete)==0 ||$cep_destino=="00000000" && floor($valor_frete)==0){
								echo "<i>não calculado</i>";	
							}else{
								if(intval($valor_frete)==0) {
									?>
									<span id="valordofrete">
										* grátis
									</span>
								<?php
								} else {
									?>
									<span id="valordofrete">
										<?php
										echo  "R$ $valor_frete";
										?>
									</span>
									<?php
								}
							}	
							
							$valor_total = formata_valor_tela($total);
						?>
					</span></td>
					<td style="font-family: Oswald-BOLD;padding-left: 40px;font-size: 26px;color: #f00;font-family:Oswald-REGULAR;"><small style="color:#f00;font-family:Oswald-REGULAR;">R$</small> <span id="vlrtotal" style="font-family: Oswald-BOLD;font-size: 26px;color: #f00;font-family: Oswald-REGULAR;"><?php echo $valor_total;?></span></td>
				</tr>
			</table>
		</div>
        </div>
    
        
<div id="selec_frete" style="padding: 0px; display:none;">
	<form name="frm_frete" id="frm_frete" action="frete.php" method="post">
<?php
			
				$cart_peso = get_carrinho_peso($orcamento);
			
				$ssql = "select freteid, fcodigo, fdescricao, ftexto, fprazo, fcalcular, fvalor_fixo, icone
						from tblfrete_tipo where fativo=-1 order by fordem";
						
				$result = mysql_query($ssql);
				if($result){
					while($row=mysql_fetch_assoc($result)){
						if(!(($cep_destino >= "01000001" && $cep_destino <= "05999999" ) || ($cep_destino >= "08000000" && $cep_destino <= "08499999" )) && $row["fcodigo"] == "99997"){
							continue;
						}
						$frete = new Frete();
						$frete->codservico = array($row["fdescricao"].'||'.$row["fcodigo"]."||".$row["fcalcular"]);
						$frete->valor_declarado = $cart_subtotal;
						$frete->peso = $cart_peso;
						$frete->cep_destino = $cep_destino;
						$frete->freteid = $row["freteid"];
						
						$frete->caCalculaFrete();						
						
						$valor_frete = $frete->valor_frete;
						//$valor_frete = str_replace("||","",$valor_frete);
						$aux = explode('||', $valor_frete);
						$valor_frete = number_format($aux[0],2,",",".");
						if ($aux[1] == 1000) {
			?>
							<script>
								$(document).ready(function() {
									$('.lightbox').fadeIn();
									$('.warning .message').html("<?php echo $aux[0]; ?>");
									$('.response').on('click', function() {
										$('.lightbox').fadeOut();
										window.location = 'endereco.php'
									});
								});
							</script>
			<?php
						} elseif ($aux[1] == 1001) {
							$body  = '<h2>Erro no calculo de frete</h2>';
							$body .= '<p><b>ID do cliente:</b> '.$_SESSION['cadastro'].'</p>';
							$body .= '<p><b>Código do erro:</b>'.$aux[0].'</p>';
							$body .= '<p><b>Mensagem de erro:</b>'.$aux[2].'</p>';
							//echo envia_email('Nutracorpore', '', 'suporte', 'suporte@feeshop.com.br', 'Erro no calculo de frete', $body);
							//die();
			?>
							<script>
								$(document).ready(function() {
									$('.lightbox').fadeIn();
									$('.warning .message').html("Ocorreu um erro ao calcular o frete. Entre em contato conosco através de um de nossos canais abaixo e informe o código do erro: "+<?php echo $aux[0]?>+". Obrigado.");
									$('.response').on('click', function() {
										$('.lightbox').fadeOut();
										window.location = 'endereco.php'
									});
								});
							</script>	
			<?php
						}
						//$frete = new Frete();
						$frete_gratis = $frete->is_frete_gratis($row["freteid"], $row["fcodigo"]);
						
						if( intval($frete_gratis) == -1 ) {
							$valor_frete = "*FRETE GRÁTIS";//"* frete grátis";	
						}
						//$frete = new Frete();
						$frete_prazo = $frete->CalculaPrazoEntrega($row["fcodigo"]);
						
						$frete_prazo = ($frete_prazo == 0 ) ? "" : " até $frete_prazo dias úteis";						
						// echo $frete_prazo;
						
						// $frete_gratis;						
						
						if($row["fcalcular"]==0){
							$valor_frete = number_format($row["fvalor_fixo"],2,",",".");
						}
						if($valor_frete != "*FRETE GRÁTIS") {
							$valor_frete_recal = str_replace(",", ".", $valor_frete);
						} else {
							$valor_frete_recal = 0;
						}
						//echo "valor_frete_recal: $valor_frete_recal<br>";

						//print_r($frete->erro);

						//if(count($frete->erro) == 0){
							$i++;
							//echo "freteid: ".$row["freteid"]."<br>";
							//if($valor_frete_recal != "" && $row["freteid"] !=4){
								echo '	<div class="box-frete" style="font-family:arial">
										<label style="font-family:arial;">
										<div class="entrega-logo-content"><img src="'.$row["icone"].'" /></div>
										<span class="prazo-frete"><b>'.$frete_prazo.'</b></span>
										<input type="radio" id="frete" name="frete" onclick="recalc_frete(this.value);" value="'.$valor_frete_recal.'"';


								if($i==1 && $freteid==0){
									//echo ' checked ';
								}
								else
								{
									if($freteid==$row["freteid"]){
										//echo ' checked '; 	
									}	
								}
								if($freteid==1){
									//echo ' checked ';
								}
								echo '	/>&nbsp;';

								// echo $row["fdescricao"];
								
								// if($frete_prazo != ""){
								// 	echo '< '.$frete_prazo.' > ';
								// }
								// else
								// {
								// 	echo ' > ';	
								// }
								
										// if($valor_frete>0){
										// 	echo ' R$ ';	
										// }
										// echo $valor_frete . ". ";	
										echo ' <div class="text-box">';
								
										echo "<strong>(R$ ".$valor_frete.")</strong>&nbsp;";
										echo $row["ftexto"];
										echo '</label>';
										echo '</div>';
										echo '</div>';
							//}
						//}
					}
					mysql_free_result($result);
				}
			?>
	</form>
</div>


        <!--// mensagem para presente //-->
        <div id="box-carrinho-mensagem-presente">
            <p><div class="ico-presente"><img src="images/ico-carrinho-presente.png" /></div><strong style="color:#666; font-size:14px; margin-left:5px;">Mensagem para presente:</strong></p><br />
            <p style="line-height:25px; font-size:12px; color:#666; margin-top:-2px">
            <strong>Você escolheu enviar esse(s) produto(s) como presente, inclua uma mensagem que será impressa e enviada junto com o pacote.<br />
            Insira a mensagem abaixo :</strong><br />
            </p>
              <textarea name="lista_mensagem" id="lista_mensagem" cols="95" rows="5" style="background:#fff; height: 60px; width: 1000px; border: #ccc solid 1px;"><?php echo $texto_presente;?></textarea>
              <p style="margin-top:10px;font-size:12px;"><strong>Limite de 300 caracteres.</strong> Total de caracteres digitados <span id="total_carcteres">0</span></p>
        </div>
    
        

		<div style="width:980px; height:30px; float:left; margin: 0 0 0 0; padding: 0 0 0 10px; font-size:12px;">
		   &nbsp;
		</div>
		
		<div style="width:980px; height:30px; float:left; margin: 0 0 0 0; padding: 0 0 0 10px; font-size:12px;">
		   &nbsp;
		</div>
		<div id="box-botoes-carrinho">
			<div id="btn-continuar-comprando"><a href="<?php echo $history_back;?>">Continuar comprandoa</a></div>
			<div class="btn-finalizar-compra" style="float:right; margin:0;"><a href="#" class="btn-finaliza-compra">Finalizar Compra</a></div>
		</div>
		<div style="width:980px; height:30px; float:left; margin: 0 0 0 0; padding: 0 0 0 10px; font-size:12px;">
		   &nbsp;
		</div>
    	</div>
        
        
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>

<?php
/*echo "<pre>";
print_r($_SESSION);
echo "</pre>";
echo "cep_destino: $cep_destino";*/
?>


<script type="text/javascript">
var google_tag_params = {
	ecomm_prodid: <?php echo json_encode($products); ?>,
	ecomm_pagetype: 'cart',
	ecomm_totalvalue: <?php echo number_format($valor_total, 2, ".", ".");?>,
};
</script>

<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '570933616448411');
fbq('track', "PageView");
fbq('track', 'ViewContent', {
	content_type: 'product_group',
	content_ids: <?php echo json_encode($products); ?>
});
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=570933616448411&ev=PageView&noscript=1"/></noscript>


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border­style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970176820/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<script type="text/javascript">
$( document ).ready(function() {
    var cep = $("#cep-carrinho").val();
    //alert("cep: "+cep);
    if(cep == ""){
    	$("#selec_frete").hide('fast');
    } else {
		$("#selec_frete").show('fast');
    }
});

var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();

function recalc_frete(freteselecionado){
	var freteselecionado 	= eval(freteselecionado);
	var subtotal 			= eval(<?php echo $subtotalgeral;?>);
	var totalgeral			= eval(subtotal+freteselecionado);

	var pacRRRO				= $('.box-frete:first-child .text-box strong').html();
	pacRRRO					= pacRRRO.replace('(R$','');
	pacRRRO					= pacRRRO.replace(')','');
	pacRRRO					= eval(pacRRRO);
	pacRRRO					= pacRRRO.toFixed(2);

	if(freteselecionado >0){
		$("#valordofrete").html("R$ "+freteselecionado);

		var strNewString = $('#valordofrete').html().replace(/\./g,',');
		$("#valordofrete").html(strNewString);

		$("#vlrtotal").html(totalgeral.toFixed(2));
		strNewString = $('#vlrtotal').html().replace(/\./g,',');
		$("#vlrtotal").html(strNewString);			
	}

	if(freteselecionado == 0 && isNaN(pacRRRO) == true){
		$("#valordofrete").html(" * grátis");
	}
	
	if(freteselecionado ==0 && isNaN(pacRRRO) == false){
		$("#valordofrete").html("R$ "+pacRRRO);

		var st = eval(subtotal)*1;
		var pa = eval(pacRRRO)*1;
		var tt = st+pa;

		var strNewString = $('#valordofrete').html().replace(/\./g,',');
		$("#valordofrete").html(strNewString);

		$("#vlrtotal").html(tt.toFixed(2));
		strNewString = $('#vlrtotal').html().replace(/\./g,',');
		$("#vlrtotal").html(strNewString);	
	}



	//alert("freteselecionado: "+freteselecionado);
	//alert("subtotal: "+subtotal);
	//alert("totalgeral: "+totalgeral);


}
</script>
<?php
$cepAUX = str_replace('-', '', $cep_destino);

if(($cepAUX >= "69301000" && $cepAUX <= "69318736") || ($cepAUX >= "76801000" && $cepAUX <= "78979335")) {
	?>
	<script>
		$('.box-frete:first-child .text-box strong').html('(R$ <?php echo $_SESSION["valoraux"];?>)')
	</script>
	<?php
}
?>
<style>
.box-frete:first-child {
	/*display: none !important;*/
}
</style>
</body>
</html>