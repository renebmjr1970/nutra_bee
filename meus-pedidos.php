<?php
	include("include/inc_conexao.php");
	
	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=meus-pedidos.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=meus-pedidos.php");
		exit();
	}



	if($_REQUEST){
		$data1 	= addslashes($_REQUEST["data1"]);
		$data2 	= addslashes($_REQUEST["data2"]);
		$status = addslashes($_REQUEST["status"]); 
		$codigo = addslashes($_REQUEST["codigo"]); 
	
		if($data1!= null && $data1 != ""){
			list($dia,$mes,$ano) = explode("/",$data1);
			$data_db1 = $ano."-".$mes."-".$dia;
			$data_db1 .= " 00:00:00";	
		}
		
		if($data2!= null && $data2 != ""){
			list($dia,$mes,$ano) = explode("/",$data2);
			$data_db2 = $ano."-".$mes."-".$dia;
			$data_db2 .= " 23:59:59";	
		}	
	
	}
	
	if(!$_REQUEST || strlen($data1)<10 || strlen($data2)<10){
	
		$prazo = 30;
		
		$data1 = strtotime(date("Y-m-d", strtotime($data_hoje)) . "-$prazo days") . "";
		$data1 = date("d/m/Y",$data1);
		$data2 	= date("d/m/Y") . " 23:59:59";
		
		$data_db1 = formata_data_db($data1);
		$data_db2 = formata_data_db($data2);	
		
		$codigo = 0;
		$status	= 0;

	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Meus Pedidos</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="description" content="<?php echo $site_nome;?> Meus Pedidos. Acompanhe o status e histórico de suas compras." />
<meta name="keywords" content="<?php echo $site_nome;?> Meus Pedidos" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Meus Pedidos" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/meus-pedidos.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#data1").mask("99/99/9999");
		$("#data2").mask("99/99/9999");
    });	
	
	function seta_status(st){
		document.getElementById("status").value = String(st);
		document.getElementById("frm_pedidos").submit();
	}
	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">

         <div id="menu-topo-conta">
    		<a href="minha-conta.php" id="meu-perfil">Minha Conta</a>
            <span style="float:left; padding:0; font-size:15px;">&nbsp;|&nbsp;</span>
            <a href="meus-pedidos.php" id="meus-pedidos">Meus Pedidos</a>
    	</div>  

    <div id="menu-conta-left" class="top-margin-align">
         <?php include("inc_menu_mc.php");?>
    </div>
    
    <div id="box-meio-minha-conta">
    	<div id="box-meus-dados" class="box-margin-align">
        	<!-- <h2 class="h2-pg-meus-pedidos">Meus Pedidos</h2> -->
            <h4 class="h4-minha-conta">Todos os pedidos</h4>
            <div id="relacao-pedidos">
                <div id="titulos-relacao">
                    <span class="numero-do-pedido">No. Pedido</span>
                    <span class="data-do-pedido">Data do Pedido</span>
                    <span class="valor-total-pedido">Valor Total</span>
                    <span class="forma-de-pagamento-pedido">F.  Pagamento</span>
                    <span class="status-pedido">Status</span>
                    <span class="links-pedido-titulo">Links</span>
   	      </div>
                <div id="itens-relacao">
					
                    <?php
					
					
                    	$ssql = "select tblpedido.pedidoid, tblpedido.pcodigo, tblpedido.pvalor_total, tblpedido.pdata_cadastro, tblforma_pagamento.fforma_pagamento, 
								tblpedido_status.sstatus, tblpedido_status.sdescricao
								from tblpedido
								left join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid
								left join tblpedido_status on tblpedido.pcodstatus=tblpedido_status.statusid ";
								
						$ssql .= "  where tblpedido.pcodcadastro='{$cadastro}' and tblpedido.pdata_cadastro between '{$data_db1}' and '{$data_db2}'";		
						if($status>0){
							$ssql .= " and tblpedido_status.statusid=$status ";
						}
						if($codigo>0){
							$ssql .= " and tblpedido.pedidoid=$codigo ";
						}						
						
						
						//echo $ssql;
						
						$result = mysql_query($ssql);
						if($result){
							
							if(mysql_num_rows($result)==0){
								echo "Nenhum pedido encontrado desde " . $data1 . " escolha outro período ou refaça a busca.";	
							}
							
							while($row=mysql_fetch_assoc($result)){
								
								$codigo_pedido	= 15500+$row["pcodigo"];
								$codigo_pedido_link = "0000".$row["pcodigo"];
								$codigo_pedido_link	= substr($codigo_pedido_link, strlen($codigo_pedido_link)-6,6);
								$data 			= formata_data_tela($row["pdata_cadastro"]);
								$valor			= number_format($row["pvalor_total"],2,",",".");
								$forma_pagamento= $row["fforma_pagamento"];
								$status_pedido	= $row["sdescricao"];
								
								echo '
								<div class="pedido-item">
									<span class="numero-do-pedido">&nbsp;&nbsp;'.$codigo_pedido.'</span>
									<span class="data-do-pedido">'.$data.'</span>
									<span class="valor-total-pedido">R$ '.$valor.'</span>
									<span class="forma-de-pagamento-pedido">'.$forma_pagamento.'</span>
									<span class="status-pedido">'.$status_pedido.'</span>
									<span class="links-pedido"><a href="detalhe-pedido.php?codigo='.$codigo_pedido_link.'">Ver Detalhes</a></span>
								</div>							
								';
								
							}
							mysql_free_result($result);
						}
						
						
						if($codigo==0){
							$codigo = "";	
						}
					
					?>
                    


              </div>
            </div>
            <span class="dados-conta"></span>
            <form name="frm_pedidos" id="frm_pedidos" method="get" action="meus-pedidos.php" >
<div class="box-meus-pedidos"><span class="dados-meus-pedidos">Pesquisar por status:</span> 
            	<select name="status" id="select-meus-pedidos">
           			<option value="0">Todos</option>
                    <?php
                    	echo monta_combo_pedido_status($status);
					?>
                </select>
            </div>
            <div class="box-meus-pedidos"><span class="dados-meus-pedidos">Pesquisar por número:</span> <input type="text" id="codigo" name="codigo" class="filtro-meus-pedidos" value="<?php echo $codigo;?>" maxlength="10"/></div>
            <div class="box-meus-pedidos"><span class="dados-meus-pedidos">Pesquisar por data:</span> <input name="data1" type="text" class="filtro-meus-pedidos" id="data1" value="<?php echo $data1;?>"/><span style="float:left; margin: 0 5px 0 5px; padding: 3px 0 0 0;"> até </span><input name="data2" type="text" class="filtro-meus-pedidos" id="data2" value="<?php echo $data2?>"/></div>
            
            <div class="box-btns-conta">
                <div id="box-btn-buscar">
                  <input type="image" name="cmd_buscar" id="cmd_buscar" src="images/btn-busca-meus-pedidos.png" />
                </div>
            </div>
            </form>
        </div>
        
        
    </div>   
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>