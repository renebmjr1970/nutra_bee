<?php
	include("include/inc_conexao.php");	

	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=pagamento.php");
		exit();
	}

	/*-------------------------------------------------------------------	
	//navegação com ssl
	---------------------------------------------------------------------*/
	$config_certificado_instalado = get_configuracao("config_certificado_instalado");
	if($config_certificado_instalado==-1){
		if(strpos($_SERVER['SERVER_NAME'],".com")>0){
			if($_SERVER['SERVER_PORT']==80){
				header("location: https://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
				exit();
			}
		}	
	}

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie
	$formapagamento = 0;


	/*------------------------------------------------------------------------
	verifica se tem algum orcamento em aberto com base em cookies
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["orcamento"])){
		$orcamento = $_COOKIE["orcamento"];
		if(!is_numeric($orcamento)){
			$orcamento = 0;	
		}		
	}
	if($orcamento==0){
		header("location: index.php");
		exit();
	}	



	/*------------------------------------------------------------------------
	verifica o valor mínimo de parcela
	--------------------------------------------------------------------------*/
	$valor_minimo_parcela = get_configuracao("valor_minimo_parcela");


	/*-----------------------------------------------------------------
	
	-----------------------------------------------------------------*/
	if($_POST){
		
		if($_POST["action"]=="gravar" || $_POST["action"]=="ws-gravar"){
			$formapagamento = $_REQUEST["pagamento"];
			$condicaopagamento = $_REQUEST["parcelas"];
			
			if($formapagamento=="1"){
				$condicaopagamento = 1;
			}
			
			if(strlen($condicaopagamento) < 1 || !is_numeric($condicaopagamento)){
				$ssql = "select condicaoid from tblcondicao_pagamento where ccodforma_pagamento='{$formapagamento}' and cativa=-1 order by cnumero_parcelas limit 0,1";
				$result = mysql_query($ssql);
				if($result){
					while($row=mysql_fetch_assoc($result)){
						$condicaopagamento = $row["condicaoid"];
					}
					mysql_fetch_assoc($result);
				}
			}
			
			if(!is_numeric($condicaopagamento)){
				$condicaopagamento = 1;	
			}
			
			$cartao_titular 	=	addslashes($_REQUEST["cartao-titular"]);
			$cartao_numero	 	=	addslashes($_REQUEST["cartao-numero"]);
			$cartao_validade 	=	addslashes($_REQUEST["cartao-validade"]);
			$cartao_codseguranca=	addslashes($_REQUEST["cartao-codseguranca"]);
			
			$cartao_numero		=	get_only_numbers($cartao_numero);
			
			$endereco_fatura	=	$_POST["endereco-fatura"];
			if(!is_numeric($endereco_fatura)){
				$endereco_fatura = 0;	
			}
			
			$totals = get_carrinho_totals();
			list($items, $total, $subtotal, $frete, $desconto, $desconto_cupom) = explode("||",$totals);
			
			
			
			//desconto por valor do pedido ou por forma de pagamento
					 
			//por valor do pedido				
			$ssql = "select descontoid, dtitulo, dcodtipo, dtipo, dvalor, dquantidade, ddesconto from tbldesconto where dativo=-1 and dcodtipo=1 and dvalor<='{$subtotal}' 
					and dcodforma_pagamento='0' and dcodcondicao_pagamento='0' and dquantidade='0'
					";
					
			//1o. compra		
			$ssql .= "union all
					select descontoid, dtitulo, dcodtipo, dtipo, dvalor, dquantidade, ddesconto from tbldesconto where dativo=-1 and  dcodtipo=4 and dvalor<='{$subtotal}' 
					and '{$cadastro}' not in (select pcodcadastro from tblpedido where pcodcadastro='{$cadastro}')
					";
					
			//forma e condicao de pagamento		
			$ssql .= "union all
					select descontoid, dtitulo, dcodtipo, dtipo, dvalor, dquantidade, ddesconto from tbldesconto where dativo=-1 and  dcodtipo=1 and  dvalor<='{$subtotal}' 
					and dcodforma_pagamento='{$formapagamento}' and dcodcondicao_pagamento='{$condicaopagamento}'
					";
			
			//echo $ssql;
			//echo "<br />";
			//exit();
			
			$valor_desconto = 0;
			
			$result = mysql_query($ssql);	
			
			if($result){
				$num_rows = mysql_num_rows($result);
				while($row=mysql_fetch_assoc($result)){
					if($row["dtipo"]==1){
						$fator 		= 1 - ($row["ddesconto"]/100);
						$total		= ($subtotal * $fator);
						$desconto 	= $subtotal * ($row["ddesconto"]/100);
					}
					
					if($row["dtitpo"]==2){
						$total 		= $total - $row["ddesconto"];
						$desconto 	= $row["ddesconto"];
					}
					
					$subtotal 	= $subtotal;
					$total 		= $subtotal-$desconto+$frete-$desconto_cupom;
					$desconto 	= $desconto;
										
					$valor_desconto = $valor_desconto + $desconto;  
					
					//gera o log se tiver desconto
					$log = $data_hoje . " - Desconto no pedido # ".$orcamento."  Regra de desconto: ".$row["descontoid"]." ".$row["dtitulo"]." \r\n";
					gera_log($log);		
					
				}
				mysql_free_result($result);
				
			}
			
			
			
			if($valor_desconto>0){
				//$total = $total - $valor_desconto;
				$total = formata_valor_db($total);
				$valor_desconto = formata_valor_db($valor_desconto);
				$ssql = "update tblorcamento set osubtotal='{$subtotal}', ovalor_frete='{$frete}', ovalor_total='{$total}', ovalor_desconto='{$valor_desconto}', ovalor_desconto_cupom='{$desconto_cupom}' where orcamentoid=$orcamento";
				mysql_query($ssql);					
				//echo $ssql;
				//exit();
								
				//$log = date("Y-m-d H:i:s") . " - DESCONTO - " . $ssql . "\r\n";
				//gera_log($log);
				
				
			}


			if(!is_numeric($condicaopagamento)){
				$condicaopagamento = 1;	
			}
			
			$ssql = "update tblorcamento set ocodforma_pagamento='{$formapagamento}', ocodcondicao_pagamento='{$condicaopagamento}' , ocartao_titular='{$cartao_titular}', 
					ocartao_validade='{$cartao_validade}', ocartao_numero='{$cartao_numero}', ocartao_codseguranca='{$cartao_codseguranca}', ocodendereco_fatura='{$endereco_fatura}',
					odata_alteracao='{$data_hoje}' 
					where orcamentoid=$orcamento";
			mysql_query($ssql);				
			//echo $ssql;
			//exit();
			
			//redireciona no caso se ser postado pela pagina e nao pelo ajax webservice
			if($_POST["action"]=="gravar"){
				header("location: confirmacao-compra.php");
			}
			if($_POST["action"]=="ws-gravar"){
				echo "ok";
			}
			exit();
		}
		
	}
	
	
	
	if(!$_POST){
			/*------------------------------------------------------------------------
			carrega o total
			--------------------------------------------------------------------------*/	
			
		$ssql = "select osubtotal, ovalor_desconto, ovalor_frete, ovalor_presente, ovalor_presente_cartao, ovalor_desconto_cupom, ovalor_desconto_troca, ovalor_total
				from tblorcamento where orcamentoid='{$orcamento}' ";
				
//		echo $ssql;	
				
		$result = mysql_query($ssql);		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$subtotal 				= 	$row["osubtotal"];
				$desconto				=	$row["ovalor_desconto"];
				$valor_frete			=	$row["ovalor_frete"];
				$valor_desconto_cupom	=	$row["ovalor_desconto_cupom"];
				$presente				=	$row["ovalor_presente"];
				$presente_cartao		=	$row["ovalor_presente_cartao"];
				$desconto_troca			=	$row["ovalor_desconto_troca"];
			}
			mysql_free_result($result);
		}
				
		$total = $subtotal + $valor_frete + $presente + $presente_cartao  - ($desconto + $valor_desconto_cupom + $desconto_troca);
		$total = formata_valor_db($total);
		
		$ssql = "update tblorcamento set osubtotal='{$subtotal}', ovalor_desconto='{$desconto}', ovalor_frete='{$valor_frete}', ovalor_presente='{$presente}', 
				ovalor_presente_cartao='{$presente_cartao}', ovalor_desconto_cupom='{$valor_desconto_cupom}', ovalor_desconto_troca='{$desconto_troca}',
				ovalor_total='{$total}' where orcamentoid='{$orcamento}'";
		mysql_query($ssql);	

	}
	
	
	/*---------------------------------------------------------------------
	carrega os totais do carrinho
	----------------------------------------------------------------------*/

	$totals = get_carrinho_totals();
	
	list($cart_items, $cart_total, $cart_subtotal, $cart_frete, $cart_desconto, $cart_desconto_cupom, $cart_peso) = explode("||",$totals);
	
	if(!is_numeric($cart_items)){
		$cart_items = 0;	
	}
	
	
	
	$cart_items 			= number_format($cart_items,0);
	$cart_total 			= number_format($cart_total,2,",",".");
	$cart_subtotal 			= number_format($cart_subtotal,2,",",".");
	$cart_frete				= number_format($cart_frete,2,",",".");
	$cart_desconto			= number_format($cart_desconto,2,",",".");
	$cart_desconto_cupom	= number_format($cart_desconto_cupom,2,",",".");
	$cart_peso				= number_format($cart_peso,2,",",".");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Forma de Pagamento</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Forma de Pagamento" />
<meta name="description" content="<?php echo $site_nome;?> Forma de Pagamento. Escolha a forma de pagamento para finalizar seu pedido" />
<meta name="keywords" content="<?php echo $site_nome;?> Forma de Pagamento" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Forma de Pagamento" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/pagamento.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery2.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script language="javascript" type="text/javascript">

var pgto_tipo = 0;

$(document).ready(function() {	
	$("#cartao-validade").mask("99/99");
	$('#cartao-numero').mask("9999.9999.9999.9999");
	$('#cartao-codseguranca').mask("999");
	
});


function clear_campos(){
	$("#cartao-titular").attr("value","");
	$("#cartao-numero").attr("value","");
	$("#cartao-validade").attr("value","");
	$("#cartao-codseguranca").attr("value","");
}



function show_botao(id, tipo){
	
	clear_campos();
	
	var bt = '<input name="cmd_continuar" id="cmd_continuar" type="image" src="images/btn-continuar.jpg" border="0" />';	
	pgto_tipo = tipo;
	var m = "9999.9999.9999.9999";
	var m1 = "999";
	
	$("#dados-cart").hide();	
	$("#msg-cartao").html("");	
	
	if(tipo==1 || tipo>=5){
		$("#btn-continuar-cartoes").html("");
		$("#btn-continuar-boleto").html(bt);
		return;
	}
	
	if(tipo>=2 && tipo<=4){
		//if(tipo==4){			
			$('#div-cartao-numero').html('<input type="text" name="cartao-numero" id="cartao-numero" class="campos-cadastro" />');
			$('#div-cartao-codseguranca').html('<input type="text" name="cartao-codseguranca" id="cartao-codseguranca" class="campos-cadastro" style="width:50px;" />');	

			if(id>=2 || id<=3){
				m = "9999.9999.9999.9999";
				m1 = "999";
			}

			if(id==4){
				m = "9999.999999.9999";	
				m1 = "999";
			}

			if(id==5){
				m = "9999.999999.99999";	
				m1 = "9999";
			}

			$('#cartao-numero').mask(m);
			$('#cartao-codseguranca').mask(m1);
			$("#dados-cart").show();	
		//}
		//else
		//{
		//	$("#dados-cart").hide();	
		//}

		setTimeout("document.getElementById('cartao-titular').focus();",100);

		get_condicao_pagamento(id);		
		
		$("#btn-continuar-cartoes").html(bt);
		$("#btn-continuar-boleto").html("");
		
	}
	else
	{
		$("#dados-cart").hide();		
	}


}	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">

	<div id="header-content">

        <?php
			include("inc_headerSTEP.php");
		?>

    </div>
    
	<div id="main-box-container">
		<div id="andamento">
			<span class="passox " style="margin-left:0;">1 - Identificação <img src="images/setaSTEP.jpg"/></span>
			<span class="passox " style="margin-left: 162px;">2 - Entrega <img src="images/setaSTEP.jpg"/></span>
			<span class="passox ativo" style="margin-left: 162px;">3 - Pagamento</span>
			<span class="passox ">4 - Confirmação</span>
		</div>
		<span id="tituloStep">Pagamento =></span>
		<span id="subtituloStep">Escolha uma forma de pagamento</span>
		<br>
	<div id="main-box-container">
	
    <form name="frm_pagamento" id="frm_pagamento" method="post" action="pagamento.php" onsubmit="return valida_pagamento();">
    <input type="hidden" name="action" id="action" value="gravar" />
  <div id="informacoes-pagamento">
    <div class="campo-cadastro">
		<table border="0" width="100%" id="tablePagamento">
			<tr>
				<td><span class="campo-pagamento">Valor total dos produtos:</span></td>
				<td align="right" class="valorPagamento">R$ <?php echo $cart_subtotal;?></td>
			</tr>
			<tr>
				<td><span class="campo-pagamento">Descontos:</span></td>
				<td align="right" class="valorPagamento">R$ <?php echo $cart_desconto_cupom;?></td>
			</tr>
			<tr>
				<td><span class="campo-pagamento" style="font-family: PTSans-BOLD;">Despesas com frete:</span></td>
				<td align="right" class="valorPagamento"><?php if($cart_frete>0){ echo "R$ " . $cart_frete;}else{ echo "Frete grátis!";}?></td>
			</tr>
		</table>
		<hr style="margin:10px 0;">
		<span class="campo-pagamento">Valor total a pagar:</span>
		<span style="float:right;"  class="valorPagamento">R$ <?php echo $cart_total;?></span>
    </div>
    
  </div>
  
      <div id="formas-pagamento">
            <div class="campo-cadastro">
                <span id="opcoesPagamento">Opções de pagamento:</span>
            </div>
        
            <div class="campo-cadastro">
                <?php
					
					/*----------------------------------------------------------------------
					BOLETO 
					------------------------------------------------------------------------*/
                    $subtotal = formata_valor_db($cart_subtotal); 
                    $total = formata_valor_db($cart_total); 
                    
                    $ssql = "select p.formapagamentoid, p.fforma_pagamento, p.fdescricao, p.finstrucao_pagamento, p.fexibe_imagem, p.fimagem, p.fcodtipo, p.fativa, p.fordem, 
                    c.condicaoid, c.ccodforma_pagamento, c.ccondicao, c.cdescricao, c.cnumero_parcelas, c.cativa , d.dtipo, d.dvalor, d.ddesconto , d.dativo
                    from tblforma_pagamento as p
                    inner join tblcondicao_pagamento as c on p.formapagamentoid=c.ccodforma_pagamento
                    left join tbldesconto as d on p.formapagamentoid=d.dcodforma_pagamento and d.dativo=-1 and d.dvalor<='{$subtotal}' 
                    where p.fcodtipo = 1  and p.fativa=-1 and c.cativa=-1 and c.cnumero_parcelas=1 
					order by p.fforma_pagamento, c.cnumero_parcelas
					
					";
                    
                    //echo $ssql;	
                    
                    $result = mysql_query($ssql);		
                    if($result){
                        
                        while($row=mysql_fetch_assoc($result)){
                            
                            $valor 		= $row["dvalor"];
                            $desconto 	= $row["ddesconto"]; 
                            $tipo		= $row["dtipo"];
                            
                            
                            echo '<span class="tit-forma-pagamento" title="'.$row["fdescricao"].'">'.$row["fforma_pagamento"];

                            if($subtotal>$valor && $row["ddesconto"]>0){
                                
                                if($tipo==1){
                                    $fator 		= 1 - ($row["ddesconto"]/100);
                                    $total		= ($subtotal * $fator);
                                    $desconto 	= $row["ddesconto"] . "%";
                                    $economia	= " você economiza R$ " . number_format($total * ($row["ddesconto"]/100),2,",",".") . ".";
                                }
            
                                if($tipo==2){
                                    $total 		= $subtotal -  $row["ddesconto"];
                                    $desconto 	= "R$ " . number_format($row["ddesconto"],2,",",".") . "";
                                }
                                
                                $total		= number_format($total,2,",",".");
            
                                echo '<span style="font-size:11px;">Com '.$desconto.' de desconto. Total R$ '.$total.'</span>';
                            }

							
							echo '</span>
                            <div class="forma-pagamento"><label>
							<input type="radio"  name="pagamento" id="pagamento" value="'.$row["formapagamentoid"].'" class="radio-pagamento" onclick="javascript:show_botao(1,1);" />
                            <div id="logo-boleto">';
                            if($row["fexibe_imagem"]==-1){
                                echo '<img src="'.$row["fimagem"].'" border="0" />';	
                            }
                            echo '</div></label></div>';
                            

                        }
                        mysql_free_result($result);
                    
                    }
                
					/*----------------------------------------------------------------------
					DEPÓSITO 
					------------------------------------------------------------------------*/
                    $subtotal = formata_valor_db($cart_subtotal); 
                    $total = formata_valor_db($cart_total); 
                   
                    $ssql = "select p.formapagamentoid, p.fforma_pagamento, p.fdescricao, p.finstrucao_pagamento, p.fexibe_imagem, p.fimagem, p.fcodtipo, p.fativa, p.fordem, 
                    c.condicaoid, c.ccodforma_pagamento, c.ccondicao, c.cdescricao, c.cnumero_parcelas, c.cativa , d.dtipo, d.dvalor, d.ddesconto , d.dativo
                    from tblforma_pagamento as p
                    inner join tblcondicao_pagamento as c on p.formapagamentoid=c.ccodforma_pagamento
                    left join tbldesconto as d on p.formapagamentoid=d.dcodforma_pagamento and d.dativo=-1 and d.dvalor<='{$subtotal}' 
                    where p.fcodtipo = 7  and p.fativa=-1 and c.cativa=-1 and c.cnumero_parcelas=1 
					order by p.fforma_pagamento, c.cnumero_parcelas";
                    
                    $result = mysql_query($ssql);		
                    if($result){
                        
                       while($row=mysql_fetch_assoc($result)){
							$i++;
							
							if($i==1){
								echo '<span class="tit-forma-pagamento">Depósito/Transferência Bancária</span>';	
							}	
							echo '<div class="forma-pagamento"><label>';
							echo '<input type="radio" name="pagamento" id="pagamento" value="'.$row["formapagamentoid"].'" class="radio-pagamento" onclick="javascript:show_botao('.$row["formapagamentoid"] .','.$row["fcodtipo"].');" />';
							echo '<div id="logo-visa">';
							if($row["fexibe_imagem"]==-1){
                                echo '<img src="'.$row["fimagem"].'" border="0" />';	
                            }
							echo '</div>';
							echo '</label></div>';
							
						}
						mysql_free_result($result);
                    
                    }
				
				
					/*----------------------------------------------------------------------
					CARTÕES DE CREDITO
					------------------------------------------------------------------------*/
					$i = 0;
                    $ssql = "select p.formapagamentoid, p.fforma_pagamento, p.fdescricao, p.finstrucao_pagamento, p.fexibe_imagem, p.fimagem, p.fcodtipo, p.fativa, p.fordem, 
							c.condicaoid, c.ccodforma_pagamento, c.ccondicao, c.cdescricao, c.cnumero_parcelas, c.cativa , d.dtipo, d.dvalor, d.ddesconto , d.dativo
							from tblforma_pagamento as p
							inner join tblcondicao_pagamento as c on p.formapagamentoid=c.ccodforma_pagamento
							left join tbldesconto as d on p.formapagamentoid=d.dcodforma_pagamento and d.dativo=-1 and d.dvalor<='{$subtotal}' 
							where p.fcodtipo >= 2 and p.fcodtipo <= 4  and p.fativa=-1 and c.cativa=-1 and c.cnumero_parcelas=1 
							order by p.fordem, p.fforma_pagamento, c.cnumero_parcelas";
							
                    $result = mysql_query($ssql);		
                    if($result){
                        while($row=mysql_fetch_assoc($result)){
							$i++;
							
							if($i==1){
								echo '<span class="tit-forma-pagamento">Cartão de Crédito</span>';	
							}
							
							
							echo '<div class="forma-pagamento"><label>';
							echo '<input type="radio" name="pagamento" id="pagamento" value="'.$row["formapagamentoid"].'" class="radio-pagamento" onclick="javascript:show_botao('.$row["formapagamentoid"] .','.$row["fcodtipo"].');" />';
							echo '<div id="logo-visa">';
                            if($row["fexibe_imagem"]==-1){
                                echo '<img src="'.$row["fimagem"].'" border="0" />';	
                            }							
							echo '</div>';
							echo '</label></div>';
							
						}
						mysql_free_result($result);
					}
					
					?>
                    
                
                <div id="dados-cart" style="display:none;">
                
                    <div class="campo-cadastro">
                        <span class="label-campos">Nome do titular</span> <input type="text" name="cartao-titular" id="cartao-titular" class="campos-cadastro" />
                        <span class="label-campos-exemplo">Nome como impresso no cartão</span>
                    </div>
                    <div class="campo-cadastro">
                        <span class="label-campos">Número do cartão</span> 
                        <div id="div-cartao-numero">
                        <input type="text" name="cartao-numero" id="cartao-numero" class="campos-cadastro" maxlength="20" />
                        </div>
                        <span class="label-campos-exemplo">Somente números</span>
                    </div>
                    <div class="campo-cadastro">
                        <span class="label-campos">Validade</span> <input type="text" name="cartao-validade" id="cartao-validade" class="campos-cadastro" style="width:50px;" maxlength="7" />
                        <span class="label-campos-exemplo">ex: <?php echo date("m/y");?></span>
                    </div>
                    <div class="campo-cadastro">
                        <span class="label-campos">Código de segurança</span> 
                        <div id="div-cartao-codseguranca">
                        <input type="text" name="cartao-codseguranca" id="cartao-codseguranca" class="campos-cadastro" style="width:50px;" maxlength="6"  />
                        </div>
                        <span class="label-campos-exemplo">Código impresso no verso do cartão</span>
                    </div>

                    <div class="campo-cadastro">
                    <span class="label-campos">Número de parcelas</span>
                    <select name="parcelas" id="parcelas" class="campos-cadastro">
                      <option value="">Selecione</option>
                    </select>
                    </div>
                    
                    <div class="campo-cadastro">
                    <span class="label-campos">Endereço de cobrança / fatura</span>
                    <select name="endereco-fatura" id="endereco-fatura" class="campos-cadastro"> 
                      <option value="">Selecione</option>
                      <?php
                      	$ssql = "select enderecoid, etitulo, enome, ecep, eendereco, enumero, ecomplemento, ebairro, ecidade, eestado 
						from tblcadastro_endereco
						where ecodcadastro='{$cadastro}'
						";
						$result = mysql_query($ssql);
						if($result){
							while($row=mysql_fetch_assoc($result)){
								echo '<option value="'.$row["enderecoid"].'">';
								echo $row["etitulo"]." - ".$row["eendereco"].",&nbsp;".$row["enumero"]."&nbsp;".$row["ecomplemento"]." - ".$row["ebairro"]." - ".$row["ecidade"]." - ".$row["eestado"]." - ".$row["ecep"];
								echo '</option>';
							}
							mysql_free_result($result);
						}
					  ?>
                    </select>
                    <span class="label-campos-exemplo" onmouseover="this.style.cursor='pointer';" onclick="javascript:window.location='cadastro-endereco.php?action=novo&redir=pagamento';">Clique aqui para alterar ou incluir outro.</span>
                    </div>    
                    
                    
                    <div id="btns-navegacao" style="margin-bottom:10px;">
                    	<div id="msg-cartao">
                        <!--// retorno do gatway de cartões  //-->
                        </div>
                    
                        <div id="btn-continuar-cartoes" >
                        	<!--//  //-->
                        </div>                    
                    </div>                   
                                    
                                       
                </div>                    
                    
                    
                    
                    <?php
					
					
					/*----------------------------------------------------------------------
					PAGSEGURO - PAYPAL
					------------------------------------------------------------------------*/
					$i = 0;
                    $ssql = "select p.formapagamentoid, p.fforma_pagamento, p.fdescricao, p.finstrucao_pagamento, p.fexibe_imagem, p.fimagem, p.fcodtipo, p.fativa, p.fordem, 
							c.condicaoid, c.ccodforma_pagamento, c.ccondicao, c.cdescricao, c.cnumero_parcelas, c.cativa , d.dtipo, d.dvalor, d.ddesconto , d.dativo
							from tblforma_pagamento as p
							inner join tblcondicao_pagamento as c on p.formapagamentoid=c.ccodforma_pagamento
							left join tbldesconto as d on p.formapagamentoid=d.dcodforma_pagamento and d.dativo=-1 and d.dvalor<='{$subtotal}' 
							where p.fcodtipo = 5  and p.fativa=-1 and c.cativa=-1 and c.cnumero_parcelas=1 
							or p.fcodtipo = 6  and p.fativa=-1 and c.cativa=-1 and c.cnumero_parcelas=1  
							order by p.fordem, p.fforma_pagamento, c.cnumero_parcelas";
							
                    $result = mysql_query($ssql);		
                    if($result){
                        while($row=mysql_fetch_assoc($result)){
							$i++;
							
							//if($i==1){
								echo '<span class="tit-cat-cadastro">'.$row["fforma_pagamento"].'</span>';	
							//}
							
							
							echo '<div class="forma-pagamento"><label>';
							echo '<input type="radio" name="pagamento" id="pagamento" value="'.$row["formapagamentoid"].'" class="radio-pagamento" onclick="javascript:show_botao('.$row["formapagamentoid"] .','.$row["fcodtipo"].');" />';
							echo '<div id="logo-visa">';
                            if($row["fexibe_imagem"]==-1){
                                echo '<img src="'.$row["fimagem"].'" border="0" />';	
                            }							
							echo '</div>';
							echo '</label></div>';
							
						}
						mysql_free_result($result);
					}					
							
				?>

                        
            </div>
      </div>
  	
    <div id="btns-navegacao" style="margin-bottom:50px;">
        <div id="btn-voltar" style="background:url(images/voltar-pagamento.jpg) no-repeat;">
            <a href='frete.php'>Voltar</a>
        </div>
        <div id="btn-continuar-boleto">
        <!--//  //-->
        </div>
        
    </div>
	
    </form>    
	</div>
    
</div>
    <div id="footer-container">
		<?php
            include("inc_footerSTEP.php");
        ?>
    </div>
	<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>