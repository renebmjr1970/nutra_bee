<?php
require("include/inc_conexao.php");
$erro = null;
if (isset($_FILES['arquivo']))
{
	$pedidoid = $_SESSION["pedidoid"];
    $extensoes = array(".gif", ".pdf", ".png", ".jpg");
    $caminho = "admin/deposito/";
    $nome = $_FILES['arquivo']['name'];
    $temp = $_FILES['arquivo']['tmp_name'];
    if (!in_array(strtolower(strrchr($nome, ".")), $extensoes)) {
		$erro = 'Extensão de arquivo inválida, por favor envie o arquivo no formato .jpg, .png, .pdf ou .gif';
	}
    if (!$erro) {
        //$nomeAleatorio = md5(uniqid(time())).strrchr($nome, ".");
		$nomeAleatorio = "pedido_".$pedidoid.strrchr($nome, ".");
		if(file_exists($caminho.$nomeAleatorio)){
			unlink($caminho.$nomeAleatorio);
		}
        if (!move_uploaded_file($temp, $caminho.$nomeAleatorio)){
            $erro = 'Não foi possível anexar o arquivo, por favor tente novamente mais tarde';
		}else{
			$caminho_imagem = $caminho.$nomeAleatorio;
			$sql = "UPDATE tblpedido SET deposito = '".$caminho_imagem."' WHERE pedidoid = ".$pedidoid;
			if(!mysql_query($sql)){
				$erro = 'Não foi possível anexar o arquivo, por favor tente novamente mais tarde';
			}else{
				$sucesso = 'Arquivo enviado com sucesso!';
				//$mensagem = "O pedido <strong>".$pedidoid."</strong> foi pago através de depósito bancário <a href='http://".$_SERVER["HTTP_HOST"]."/nutracorpore/".$deposito."'>clique aqui</a> para ver o comprovante.";
				//mail("fabioo.salgado@gmail.com","Recebimento de Depósito",$mensagem);
			}
		}
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Upload dinâmico com jQuery/PHP</title>
    
    <script type="text/javascript" src="js/jquery.js"></script>
    
    <script type="text/javascript">
    $(function($) {
        var pai = window.parent.document;
        
        <?php if (isset($erro)):?>
			alert('<?php echo $erro;?>');
			//$("#mensagem_upload").html('<?php echo $erro;?>');
        <?php elseif (isset($sucesso)):?>
			alert('<?php echo $sucesso;?>');
            //$("#mensagem_upload").html('<?php echo $sucesso;?>');
        <?php endif ?>
        
    	$("#arquivo").change(function() {	    
            if (this.value != ""){    
                $("#status").show();
                $("#upload").submit();
            }
        });
    });
    </script>
	<link href="css/style.css" media="all" rel="stylesheet" />
</head>

<body>

<form id="upload" action="upload.php" method="post" enctype="multipart/form-data">
    <span class="upload-wrapper">
		<input type="hidden" name="pedidoid" value="<?php echo $pedido;?>"/>
		<input class="upload-file" type="file" name="arquivo" id="arquivo" />
		<img class="upload-button" alt="" title="" src="images/btn-enviar-comprovante.png" />
	</span>
</form>

</body>
</html>