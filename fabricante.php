<?php
	include("include/inc_conexao.php");
	
	/*-------------------------------------------------
	//navegação sem ssl
	---------------------------------------------------*/
	//echo $_SERVER['REQUEST_URI'];
	if(strpos($_SERVER['SERVER_NAME'],".com")>0){
		if($_SERVER['SERVER_PORT']!=80){
			header("location: http://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
			exit();
		}
	}	
	$fabricante='-1';
	//monta a marca & categoria
	$uri = str_replace("/nutraNew/","",$_SERVER['HTTP_X_REWRITE_URL']);

	$pagina = 1;
	$start = 0;
	$limit = 12;
	$ordem = 0;
	
	$menor = 0;
	$maior = 999999;
	
	
	$canonical = "";
	

	/*---------------------------------------------------------------------------
	QUERY STRING
	-----------------------------------------------------------------------------*/
	$qs=array();
	$variaveis = $uri;
	$variaveis = explode("&", substr($uri, strpos($uri,"?")+1 , strlen($uri) ) );
	if($variaveis!=""){
		for($i=0;$i<count($variaveis);$i++){
			$nvar=explode("=",$variaveis[$i]);
				$qs[$nvar[0]] = $nvar[1];
		}
	}
	
	
	/*--------------------------------------------------------------------------
	variaveis query string
	---------------------------------------------------------------------------*/
	if(is_numeric($qs["limit"])){	
		$limit = $qs["limit"];
	}

	if(is_numeric($qs["pagina"])){	
		$pagina = $qs["pagina"];
		if($pagina <= 0){
			$pagina = 1;	
		}
		$start = ($pagina * $limit) - $limit; 
	}

	
	
	if(is_numeric($qs["ordem"])){	
		$ordem = $qs["ordem"];
		if($ordem>8 || $ordem <0){
			$ordem = 0;	
		}
	}	


	if(is_numeric($qs["menor"])){	
		$menor = intval($qs["menor"]);
	}	
	
	if(is_numeric($qs["maior"])){	
		$maior = intval($qs["maior"]);
		if($maior==0){
			$maior = 999999;	
		}
	}	
	
	/*--------------------------------------------------------------------------
	remove os parametros de querystring para montar as info de categorias
	---------------------------------------------------------------------------*/
	if(strpos($uri,"?")<>""){
		$uri = substr($uri,0,strpos($uri,"?")-1);
	}
	$uri = str_replace(".htm","",$uri);
	$uri = trim($uri,"//");
	$nodes = explode("/",$uri);

	//print_r($nodes);
	
	$marcaid		= intval(get_marcaid_by_node($nodes[1],0));
	$marca			= get_marca($marcaid,"mmarca");
	$departamentoid = intval(get_categoriaid_by_node($nodes[2],0));
	$categoriaid 	= intval(get_categoriaid_by_node($nodes[3],$departamentoid));
	$subcategoriaid = intval(get_categoriaid_by_node($nodes[4],$categoriaid));


	switch ($ordem){
		
		case "0":
		$order = " pdata_inicio desc";
		break;				
		
		case "1":
		$order = " pvalor_unitario";
		break;

		case "2":
		$order = " pvalor_unitario desc";
		break;

		case "3":
		$order = " pvalor_unitario desc";	//mais vendidos
		break;

		case "4":
		$order = " pvalor_unitario desc";	//mais bem avaliados
		break;

		case "5":
		$order = " pproduto";
		break;

		case "6":
		$order = " pproduto desc";
		break;

		case "7":
		$order = " pdata_cadastro desc";	//lancamento
		break;

		case "8":
		$order = " pdesconto desc";
		break;

	}
	
		//define se ira agrupar os produtos com mesma referencia
		$config_agrupa_por_referencia = get_configuracao("config_agrupa_por_referencia");
		if($config_agrupa_por_referencia==0){
			$agrupa_por_referencia = ", tblproduto.produtoid ";	
		}
		$vitrine_ordem = get_configuracao("vitrine_ordem");
		if($vitrine_ordem==""){
		  $vitrine_ordem = " RAND() ";
		}
	

			$ssql_marca = "select tblproduto.pdescricao, tblproduto.produtoid, tblproduto.pdisponivel, sum(tblestoque.eestoque) as estoque,tblproduto.pcontrola_estoque, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia, tblproduto.pproduto,
			tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, (tblproduto.pvalor_comparativo - tblproduto.pvalor_unitario) as pdesconto,
			tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
			from tblproduto
			left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
			left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
			left join tblproduto_categoria on tblproduto.produtoid = tblproduto_categoria.pcodproduto 
			left join tblmarca on marcaid = tblproduto.pcodmarca
				where tblproduto.pdata_inicio<='{$data_hoje}' and tblproduto.pdata_termino>='{$data_hoje}' ";
			
			if($marcaid>0){
				$ssql_marca .= " and tblproduto.pcodmarca = '{$marcaid}' ";	
			}
			
			$ssql_marca .= "
				and tblproduto.pvalor_unitario between '{$menor}' and '{$maior}'";
			
			if($departamentoid > 0 && $categoriaid == 0 && $subcategoriaid == 0){
				$ssql_marca .= " and tblproduto_categoria.pcodcategoria = '{$departamentoid}' ";		
			}

			if($departamentoid > 0 && $categoriaid > 0 && $subcategoriaid == 0){
				$ssql_marca .= " and tblproduto_categoria.pcodcategoria = '{$categoriaid}' ";		
			}

			if($departamentoid > 0 && $categoriaid > 0 && $subcategoriaid > 0){
				$ssql_marca .= " and tblproduto_categoria.pcodcategoria = '{$subcategoriaid}' ";		
			}

			$ssql_marca .= "	
				group by tblproduto.preferencia $agrupa_por_referencia
				having(1=1)
				order by $vitrine_ordem, $order
				";
			
			//echo $ssql_marca;
			//die();
			
			$result = mysql_query($ssql_marca);
			if($result){
				$total_registros = mysql_num_rows($result);	
			}

/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("marca.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $categoria?>" />
<meta name="description" content="<?php echo $categoria;?>" />
<meta name="keywords" content="<?php echo $categoria;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $categoria;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/categoria.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
<div id="global-container">
	<div id="header-content">
	<?php
			include("inc_header.php");
		?>
    </div>
	<div id="main-box-container">
   	  <div id="categoria-container-box"></div>
    	<div id="container-menu-left">
        	<?php
            	include("inc_left_marca.php");
			?>            
        </div>
        <div class="box-products-container">
			<div id="org-sup-box-content">
				<div class="box-sort-by">
					<span class="number-found-itens"><?php echo $total_registros;?> ítens encontrados</span>
					<span class="sort-by">Ordenar por: </span>
					<select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
						<?php
							echo combo_ordem_pagina($ordem);
						?>
					</select>
					<span class="itens-por-pagina">Itens por página: </span>
					<select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
						<?php
							echo combo_itens_pagina($limit);
						?>
					</select>
				</div>
				<div class="pagination-box">
					<div class="paginacao"></span> 
						<?php
							echo paginacao($pagina, $limit, $total_registros);
						?>                            
					</div>
				</div>
			</div>
            <div id="products-category-box">
				<?php 

					$imagem_x = 260;
					$imagem_y = 260;
					$ssql_marca = "(".str_replace("having(1=1)","HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))",$ssql_marca).") UNION ALL (".str_replace("having(1=1)","HAVING (((SUM(tblestoque.eestoque) = 0 || SUM(tblestoque.eestoque) is null) && tblproduto.pcontrola_estoque = -1) || tblproduto.pdisponivel = 0)",$ssql_marca).")";
					$ssql_marca .= " limit $start, $limit";
						
					$result = mysql_query($ssql_marca) or die(mysql_error());
			
					//echo $ssql_categoria;
				
					if($result){
						
						$count = 0;
						$num_rows = mysql_num_rows($result);
						if($num_rows==0){
							echo '<div style="width:100%; height:auto; margin:20px 0 20px 0; text-align:center;"><strong>Nenhum produto foi localizado.</strong></div>';	
						}
						$products = array();
						$valor_total = 0;
						while($row=mysql_fetch_assoc($result)){
							$products[] = $row["produtoid"];
							$id = $row["produtoid"];
							$referencia = $row["preferencia"];
							$valor_total += $row["pvalor_unitario"];
							$valor_unitario = $row["pvalor_unitario"];
							$valor_comparativo = number_format($row["pvalor_comparativo"],2,',','.');
							
							$valor = $row["pvalor_unitario"];
							
							if( isset($_SESSION["utm_source"]) ){
								$desconto_utm =  get_desconto_utm_source($id);
								$valor_unitario = number_format($valor_unitario - $desconto_utm,2,",",".");
								
								if(floatval($desconto_utm)>0){
									$valor_comparativo = number_format($row["pvalor_unitario"],2,',','.');
								}
								
							}
							else
							{
								$valor_unitario = number_format($valor_unitario,2,",",".");	
							}

							$imagem = $row["pimagem"];

							$count++;
							?>
							<a href="<?=$row["plink_seo"] ?>">
							<div class="produto">
								<span class="product-thumb"><img src="<?=$imagem?>" alt="produto"></span>
								<span class="product-menu-top20-product-title"><?=$row["pproduto"] ?></span>
								<span class="product-menu-top20-product-mark"><b>+</b> <?=$row["mmarca"] ?></span>
								<span class="product-menu-top20-product-price">R$ <s><?=number_format($row["pvalor_unitario"],2,",",".") ?></s></span>
								<span class="par-velue"><?=get_valor_parcelado_new($row["produtoid"],$row["pvalor_unitario"])?></span>
								<span class="a-vista-price">Ou R$ <?=number_format($row["pvalor_unitario"]-($row["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
								<span class="boleto-text">via boleto</span>
								
							</div>
							</a>
							<?php
						}
						mysql_free_result($result);
					}				  
				  
				  ?>
			</div>
			<div id="org-sup-box-content">
				<div class="box-sort-by">
					<span class="number-found-itens"><?php echo $total_registros;?> ítens encontrados</span>
					<span class="sort-by">Ordenar por: </span>
					<select name="sort-by-itens" class="sort-by-itens" onchange="javascript:paginacao('ordem',this.value);">
						<?php
							echo combo_ordem_pagina($ordem);
						?>
					</select>
					<span class="itens-por-pagina">Itens por página: </span>
					<select name="qtde-itens-pag" class="qtde-itens" onchange="javascrip:paginacao('limit',this.value);">
						<?php
							echo combo_itens_pagina($limit);
						?>
					</select>
				</div>
				<div class="pagination-box">
					<div class="paginacao"></span> 
						<?php
							echo paginacao($pagina, $limit, $total_registros);
						?>                            
					</div>
				</div>
			</div>
        </div>
		<div id="aside-right-bar2">
			<?php carregaBanners(2); ?>
		</div>
	</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>


<script type="text/javascript">
var google_tag_params = {
	ecomm_prodid: <?php echo json_encode($products); ?>,
	ecomm_pagetype: 'other',
	ecomm_totalvalue: <?php echo number_format($valor_total, 2, ".", ".");?>,
};
</script>

<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '570933616448411');
fbq('track', "PageView");
fbq('track', 'ViewContent', {
	content_type: 'product_group',
	content_ids: <?php echo json_encode($products); ?>
});
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=570933616448411&ev=PageView&noscript=1"/></noscript>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border­style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970176820/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>