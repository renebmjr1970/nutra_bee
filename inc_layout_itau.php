<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <title>Boleto</title>
  <style>
    table {
      border-collapse: collapse;
      margin: auto;
      font-family: Arial, Helvetica, sans-serif;
    }
    .billet-upper, .billet-upper tr {
      border: 1px solid #000;
    }
    .billet-upper td, .billet-upper th, .billet-under td, .billet-under th {
      border: .1px solid #000;
      vertical-align: top;
      position: relative;
    }
    .cell-description {
      position: relative;
      top: 5px;
      left: 5px;
      display: block;
      margin-bottom: 10px;
      font-size: 16px;
    }
    .cell-information {
      padding-left: 5px;
      font-size: 16px;
      display: block;
    }
    .center {
      text-align: center;
    }
    .bordered:not(:last-of-type) {
      border-bottom: 1px solid #000;
      display: block;
      height: 27px;
    }
  </style>
</head>
<body>
  <table class="billet-upper">
  <tr class="logo-and-number">
     <th style="width:20px; padding-top: 15px; border-top: 0;" colspan="1">
       <img src="images/logoitau.jpg" alt="Banco Itaú S.A">
     </th>
     <th style="width:10px; vertical-align: middle; border-top: 0;">
       <span class="cell-description center" style="padding-top: 25px;">341-7</span>
     </th>
     <th style="width:455px; vertical-align: middle; border-top: 0;" colspan="3">
       <span class="cell-description" style="padding-top: 25px; padding-right: 20px; text-align: right;">
         <?php echo $dadosboleto["linha_digitavel"]; ?>
       </span>
     </th>
   </tr>
    <tr style="height:86px;">
     <td style="width:580px;" colspan="3">
      <span class="cell-description">
        Beneficiário
      </span>
      <span class="cell-information">
        NUTRACORPORE COMERCIAL LTDA ME - CNPJ: 18.572.130/0001-64 - Rua Dona Primitiva Vianco, 901
      </span>
    </td>
    <td style="width: 240px;">
     <span class="cell-description">
      Agência/Código Beneficiário
    </span>
    <span class="cell-information center">
     7196/14179-5
   </span>
 </td>
 <td style="width: 180px;">
  <span class="cell-description">
    Vencimento
  </span>
  <span class="cell-information center">
    <?php echo $dadosboleto["data_vencimento"]?>
  </span>
</td>
</tr>
<tr class="three" style="height:60px;">
 <td style="width:580px;" colspan="3">
   <span class="cell-description">
    Pagador
  </span>
  <span class="cell-information">
    <?php echo $dadosboleto["sacado"]?>
  </span>
</td>
<td style="width: 240px;">
  <span class="cell-description">
    Número do Documento
  </span>
  <span class="cell-information center">
    <?php //echo $dadosboleto["numero_documento"];?>
  </span>
</td>
<td style="width: 180px;">
 <span class="cell-description">
   Nosso Número
 </span>
 <span class="cell-information center">
   <?php echo $dadosboleto["nosso_numero"]?>
 </span>
</td>
</tr>
<tr class="five" style="height: 60px;">
 <td style="width: 140px;">
   <span class="cell-description">
     Espécie
   </span>
   <span class="cell-information center">
     R$
   </span>
 </td>
 <td style="width: 190px;">
  <span class="cell-description">
   Quantidade
 </span>
</td>
<td style="width: 255px;">
  <span class="cell-description">
   (x) Valor
 </span>
</td>
<td style="width: 240px;">
  <span class="cell-description">
   (=) Valor do Documento
 </span>
 <span class="cell-information center">
   <?php echo $dadosboleto["valor_boleto"]; ?>
 </span>
</td>
<td style="width: 180px;">
 <span class="cell-description">
   (-) Desconto
 </span>
</td>
</tr>
<tr class="three" style="height: 60px;">>
 <td style="width:580px;" colspan="3">
   <span class="cell-description">
     Demonstrativo
   </span>
 </td>
 <td style="width:240px;">
   <span class="cell-description">
     (+) Outros Acréscimos
   </span>
 </td>
 <td style="width: 180px;">
   <span class="cell-description">
     (=) Valor Cobrado
   </span>
 </td>
</tr>
<tr style="height:300px;">
 <td style="width:1000px;" colspan="6">
 </td>
</tr>
<tr style="height:75px;">
  <td style="width: 1000px" colspan="6">
    <span class="cell-description" style="right:0; margin:auto; margin-right:-200px; width: 600px;">
      Autenticação Mecânica
    </span>
  </td>
</tr>
</table>


<table class="billet-under">
  <tr class="logo-and-number">
   <th style="width:30px; padding-top: 15px; border-top: 0;" colspan="1">
     <img src="images/logoitau.jpg" alt="Banco Itaú S.A">
   </th>
   <th style="width:10px; vertical-align: middle; border-top: 0;">
     <span class="cell-description center" style="padding-top: 25px;">341-7</span>
   </th>
   <th style="width:656px; vertical-align: middle; border-top: 0;" colspan="4">
     <span class="cell-description" style="padding-top: 25px; padding-right: 20px; text-align: right;">
       <?php echo $dadosboleto["linha_digitavel"]?>
     </span>
   </th>
 </tr>
 <tr class="two" style="height:50px;">
   <td style="width:733px;" colspan="5">
     <span class="cell-description">
       Local de Pagamento
     </span>
     <span class="cell-description" style="left:191px;">
       Até o vencimento, preferencialmente no Itaú
     </span>
   </td>
   <td style="width: 251px;">
     <span class="cell-description">Vencimento</span>
     <span class="cell-information center"><?php echo $dadosboleto["data_vencimento"]?></span>
   </td>
 </tr>
 <tr class="two" style="width: 65px;">
   <td style="width:733px;" colspan="5">
     <span class="cell-description">
      Beneficiário
    </span>
    <span class="cell-information">
     NUTRACORPORE COMERCIAL LTDA ME - CNPJ: 18.572.130/0001-64 - Rua Dona Primitiva Vianco, 901
   </span>
 </td>
 <td style="width: 251px;">
   <span class="cell-description">Agência/Código Beneficiário</span>
   <span class="cell-information center"><?php echo $dadosboleto["agencia_codigo"]; ?></span>
 </td>
</tr>
<tr class="six" style="height: 60px;">
 <td style="width: 150px;">
   <span class="cell-description">Data do Documento</span>
   <span class="cell-information center"><?php echo $dadosboleto["data_documento"]?></span>
 </td>
 <td style="width: 215px;">
   <span class="cell-description">Número do Documento</span>
   <span class="cell-information center"><?php echo $dadosboleto["numero_documento"]?></span>
 </td>
 <td style="width: 120px;">
   <span class="cell-description">Espécie Doc.</span>
   <span class="cell-information center">DM</span>
 </td>
 <td style="width: 65px;">
   <span class="cell-description">Aceite</span>
   <span class="cell-information center">N</span>
 </td>
 <td style="width:180px;">
   <span class="cell-description">Data de Processamento</span>
   <span class="cell-information center"><?php echo $dadosboleto["data_documento"]?></span>
 </td>
 <td style="width: 251px;">
   <span class="cell-description">Nosso Número</span>
   <span class="cell-information center"><?php echo $dadosboleto["nosso_numero"]?></span>
 </td>
</tr>
<tr class="six">
  <td style="width: 150px;">
    <span class="cell-description">Uso do Banco</span>
  </td>
  <td style="width: 215px;">
   <span class="cell-description">Carteira</span>
   <span class="cell-information center">109</span>
 </td>
 <td style="width: 120px;">
   <span class="cell-description">Espécie</span>
   <span class="cell-information center">R$</span>
 </td>
 <td style="width: 65px;">
   <span class="cell-description" >Quant.</span>
 </td>
 <td style="width:180px;">
   <span class="cell-description">(x) Valor</span>
 </td>
 <td style="width: 251px;">
   <span class="cell-description">(=) Valor do Documento</span>
   <span class="cell-information center"><?php echo $dadosboleto['valor_boleto']; ?></span>
 </td>
</tr>
<tr style="height: 115px;">
  <td colspan="5" style="width: 740px; padding-top: 10px;">
    <span class="cell-information">Instruções (Texto de responsabilidade do cedente)</span>
    <span class="cell-information">Após o Vencimento Cobrar Juros de R$:0,00 ao dia e Multa de R$:0,10</span>
    <span class="cell-information">Protestar após 5 dia(s) do vencimento.</span>
  </td>
  <td style="width:251px;">
    <span class="bordered">(-) Desconto</span>
    <span class="bordered">(+) Mora/Multa</span>
    <span class="bordered">(+) Outros Acréscimos</span>
    <span class="bordered">(=) Valor Cobrado</span>
  </td>
</tr>
<tr style="height: 75px;">
  <td colspan="6">
    <span class="cell-description">Pagador: &nbsp; <?php echo $dadosboleto['sacado']; ?></span>
    <span class="cell-information"><?php echo $dadosboleto["endereco1"]?></span>
    <span class="cell-information">Pagador/Avalista</span>
    <span style="position: absolute; right: 50px; top: 0;"><?php echo $dadosboleto["cpf"]?></span>
    <span style="position: absolute; right: 50px; bottom: 0;">Ficha de Compensação</span>
  </td>
</tr>
<tr style="height:90px;">
  <td colspan="6" style="vertical-align: middle; padding-left: 5px;"> 
    <!-- <img src="images/barcode.jpg"> -->
    <?php fbarcode($dadosboleto["codigo_barras"]); ?>
    <span style="position: absolute; right: 100px; top: 10px;">Autenticação Mecânica</span>
  </td>
</tr>
</table>
</body>
</html>


<?php
// ini_set('display_errors', true);
  $content  = '0'; // TIPO DE REGISTRO
  $content .= '1'; // OPERAÇÃO
  $content .= 'REMESSA'; // LITERAL DE REMESSA
  $content .= '01'; // CÓDIGO DE SERVIÇO
  $content .= str_pad('COBRANCA', 15, ' ', STR_PAD_RIGHT); // LITERAL DE SERVIÇO
  $content .= '7196'; // AGÊNCIA
  $content .= '00'; // ZEROS COMPLEMENTARES
  $content .= '14179'; // CONTA
  $content .= '5'; // DIGITO VERIFICADOR
  $content .= str_pad('', 8, ' ', STR_PAD_RIGHT); // BRANCO
  $content .= 'NUTRACORPORE COMERCIAL LTDA ME'; // NOME DA EMPRESA
  $content .= '341'; // CÓDIGO DO BANCO
  $content .= str_pad('BANCO ITAU SA', 15, ' ', STR_PAD_RIGHT); // NOME DO BANCO
  $document_date = explode('/', $dadosboleto['data_documento']);
  $content .= $document_date[0].$document_date[1].substr($document_date[2], 2, 2); // DATA DA GERAÇÃO
  $content .= str_pad('', 294, ' ', STR_PAD_RIGHT); // BRANCO
  $content .= '000001'.PHP_EOL; // NÚMERO SEQUENCIAL

  $content .= '1'; // TIPO DE REGISTRO
  $content .= '02'; // CÓDIGO DE INSCRIÇÃO
  $content .= '18572130000164'; // CNPJ
  $content .= '7196'; // AGENCIA
  $content .= '00'; // ZEROS
  $content .= '14179'; // CONTA
  $content .= '5'; // DIGITO VERIFICADOR
  $content .= '    '; // BRANCOS
  $content .= '00003'; // INSTRUÇÃO/ALEGAÇÃO
  $content .= str_pad('', 25, ' ', STR_PAD_RIGHT); // USO DA EMPRESA
  //$content .= '0';
  $nosso_numero = $dadosboleto["nosso_numero"];
  // $nosso_numero = str_replace('/', '', $nosso_numero);
  // $nosso_numero = str_replace('-', '', $nosso_numero);
  $aux_nosso_numero  = explode('/', $nosso_numero);
  $aux_nosso_numero2 = explode('-', $aux_nosso_numero[1]);
  $content .= str_pad($aux_nosso_numero2[0], 8, 0, STR_PAD_LEFT); // NOSSO NÚMERO
  $content .= str_pad(0, 12, 0, STR_PAD_RIGHT); // QUANTIDADE DE MOEDA obs: verificar depois
  $content .= '109'; // NÚMERO DA CARTEIRA NO BANCO
  $content .= str_pad('', 21, ' ', STR_PAD_RIGHT); // IDENTIFICAÇÃO DA CARTEIRA NO BANCO
  $content .= 'I'; // CÓDIGO DA CARTEIRA
  $content .= '01'; // IDENTIFICAÇÃO DA OCORRÊNCIA
  $content .= str_pad('3', 10, ' ', STR_PAD_RIGHT); // NÚMERO DE DOCUMENTO DA COBRANÇA
  $expiration = explode('/', $dadosboleto['data_vencimento']);
  $content .= $expiration[0].$expiration[1].substr($expiration[2], 2, 2); // DATA DE VENCIMENTO DO TÍTULO
  $content .= str_pad(str_replace(',', '', $dadosboleto['valor_boleto']), 13, '0', STR_PAD_LEFT); // VALOR DO TÍTULO
  $content .= '341'; // CÓDIGO DO BANCO
  $content .= '000000'; // AGÊNCIA COBRADORA
  $content .= '1'; // ESPÉCIE
  $content .= 'N'; // ACEITE
  $document_date = explode('/', $dadosboleto['data_documento']);
  $content .= $document_date[0].$document_date[1].substr($document_date[2], 2, 2); // DATA DA GERAÇÃO
  $content .= '81'; // PRIMEIRA INSTRUÇÃO DE COBRANÇA
  $content .= '00'; // SEGUNDA INSTRUÇÃO DE COBRANÇA
  $content .= str_pad(0, 13, 0, STR_PAD_RIGHT); // JUROS DE 1 DIA
  $content .= str_pad(0, 6, 0, STR_PAD_RIGHT); // DATA LIMITE PARA CONCESSÃO DE DESCONTO
  $content .= str_pad(0, 13, 0, STR_PAD_RIGHT); // VALOR DO DESCONTO
  $content .= str_pad(0, 13, 0, STR_PAD_RIGHT); // VALOR DO IOF RECOLHIDO
  $content .= str_pad(0, 13, 0, STR_PAD_RIGHT); // VALOR DO ABATIMENTO A SER RECOLHIDO
  $content .= '01'; // TIPO DE INSCRIÇÃO PAGADOR
  $document = str_replace('.', '', $dadosboleto["cpf"]);
  $document = str_replace('/', '', $document);
  $document = str_replace('-', '', $document);
  $content .= str_pad($document, 14, 0, STR_PAD_LEFT); // NÚMERO DE INSCRIÇÃO DO PAGADOR
  $content .= str_pad($dadosboleto['sacado'], 30, ' ', STR_PAD_RIGHT); // NOME DO PAGADOR
  $content .= str_pad('', 10, ' ', STR_PAD_RIGHT); // COMPLEMENTO DE REGISTRO
  $endereco_final = substr($dadosboleto['endereco_cliente'], 0, 40);
  $content .= str_pad($endereco_final, 40, ' ', STR_PAD_RIGHT); // ENDEREÇO CLIENTE
  $content .= str_pad($dadosboleto['bairro_cliente'], 12, ' ', STR_PAD_RIGHT); // BAIRRO CLIENTE
  $content .= str_pad($dadosboleto['cep_cliente'], 8, ' ', STR_PAD_RIGHT); // CEP CLIENTE
  $content .= str_pad($dadosboleto['cidade_cliente'], 15, ' ', STR_PAD_RIGHT); // CIDADE CLIENTE
  $content .= str_pad($dadosboleto['estado_cliente'], 2, ' ', STR_PAD_RIGHT); // UF CLIENTE
  $content .= str_pad('', 30, ' ', STR_PAD_RIGHT); // NOME DO SACADOR/AVALISTA
  $content .= str_pad('', 04, ' ', STR_PAD_RIGHT); // COMPLEMENTO DO REGISTRO
  $content .= str_pad(0, 06, 0, STR_PAD_RIGHT); // DATA DE MORA
  $content .= str_pad($dadosboleto['dias'], 02, 0, STR_PAD_LEFT); // QUANTIDADE DE DIAS
  $content .= ' '; // COMPLEMENTO DO REGISTRO
  $content .= '000002'.PHP_EOL; // NÚMERO SEQUENCIAL

  $content .= '5'; // TIPO DE REGISTRO
  $content .= str_pad($dadosboleto["endereco1"], 120, ' ', STR_PAD_RIGHT); // E-MAIL DO PAGADOR
  $content .= '00'; // TIPO DE IDENTIFICAÇÃO SACADOR\AVALISTA
  $content .= str_pad(0, 14, 0, STR_PAD_LEFT); // CNPJ SACADOR\AVALISTA
  $content .= str_pad('', 40, ' ', STR_PAD_LEFT); // LOGRADOURO SACADOR\AVALISTA
  $content .= str_pad('', 12, ' ', STR_PAD_LEFT); // BAIRRO SACADOR\AVALISTA
  $content .= str_pad(0, 8, 0, STR_PAD_LEFT); // CEP SACADOR\AVALISTA
  $content .= str_pad('', 15, ' ', STR_PAD_LEFT); // CIDADE SACADOR\AVALISTA
  $content .= str_pad('', 2, ' ', STR_PAD_LEFT); // ESTADO SACADOR\AVALISTA
  $content .= str_pad('', 180, ' ', STR_PAD_LEFT); // BRANCOS
  $content .= '000003'.PHP_EOL; // NÚMERO SEQUENCIAL

  $content .= '9'; // TIPO DE REGISTRO
  $content .= str_pad('', 393, ' ', STR_PAD_LEFT); // BRANCOS
  $content .= '000004'.PHP_EOL; // NÚMERO SEQUENCIAL

  $filename = $dadosboleto['cod_pedido'].'.txt';
  $fp = fopen('./boletos/'.$filename, 'w');
  fwrite($fp, $content);
  fclose($fp);
?>
