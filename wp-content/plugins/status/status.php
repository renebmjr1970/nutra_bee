﻿<?php
set_time_limit(0);

global $wpdb;

$sql 		= "SELECT * FROM tblproduto_aviseme ORDER BY avisemeid ASC";
$retorno 	= $wpdb->get_results($sql);


$sql_pedido = "SELECT
                    wp_posts.ID as id_um,
                    wp_users.ID as id_dois,
                    wp_usermeta.meta_value as first,
                    wp_postmeta.meta_value as email,
                    wp_posts.post_status as status,
                    envio_email.msg as msg
               FROM
                    wp_posts
               LEFT JOIN     
                    wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
               LEFT JOIN     
                    wp_users ON wp_users.user_email = wp_postmeta.meta_value
               LEFT JOIN
                    wp_usermeta ON wp_users.ID = wp_usermeta.user_id
               LEFT JOIN
                    envio_email ON wp_posts.ID = envio_email.pedido                    
               WHERE 
                    wp_posts.post_type = 'shop_order' and wp_postmeta.meta_key = '_billing_email' and wp_usermeta.meta_key = 'billing_first_name' and envio_email.status = wp_posts.post_status
               GROUP BY
                    wp_posts.ID
               ORDER BY      
                    wp_posts.ID DESC
                ";

$resultado = $wpdb->get_results($sql_pedido);


?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.print.min.js"></script>


<script type="text/javascript">

	jQuery(document).ready(function() {
    	jQuery('#grid').DataTable({
    		"language": {
    			"url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Portuguese-Brasil.json"
    		},
    		dom: 'Bfrtip',
        	"pageLength": 50
    	});
	} );
</script>
<table id="grid" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Nº Pedido</th>
            <th>Cliente</th>
            <th>E-mail</th>
            <th>Status</th>
            <th>Situação</th>
            <th>Ação</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>Nº Pedido</th>
            <th>Cliente</th>
            <th>E-mail</th>
            <th>Status</th>
            <th>Situação</th>
            <th>Ação</th>
        </tr>
    </tfoot>
    <tbody>
    	<?php
    	foreach ($resultado as $resultado) {
    		?>
	        <tr style="text-align: center !important;">
	            <td><?php echo $pedido = $resultado->id_um; ?></td>
                <td><?php echo $nome = $resultado->first; ?></td>
                <td><?php echo $email = $resultado->email; ?></td>
	            <td><?php

                if($resultado->status == "wc-pending" || $resultado->status == "wc-on-hold") {
                    $status = "Aguardando";
                }

                if($resultado->status == "wc-processing") {
                    $status = "Em Processamento";
                }                       
                        
                if($resultado->status == "wc-completed") {
                    $status = "Enviado para cliente";
                }

                if($resultado->status == "wc-cancelled") {
                    $status = "Cancelada";
                }

                 echo $status; 


                 ?></td>
                <td><?php if( $msg = $resultado->msg == ""){ echo "Em aberto"; }else{ echo "<div style='color:green'>{$resultado->msg}</div>"; } ?></td>

                 <form method="post" action="https://nutracorpore.com.br/wp-content/plugins/status/envio.php">

                    <input type="hidden" name="pedido" value="<?php echo $pedido = $resultado->id_um; ?>">
                    <input type="hidden" name="nome" value="<?php echo $nome = $resultado->first; ?>">
                    <input type="hidden" name="email" value="<?php echo $email = $resultado->email; ?>">
                    <input type="hidden" name="status" value="<?php echo $resultado->status; ?>">

                    <td><button type="submit">Enviar Email</button></td>

                 </form>
	        </tr>

	        <?php

	    }
        ?>
    </tbody>
</table>



