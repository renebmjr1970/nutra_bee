<?php
/*
Plugin Name: Feeshop - Enviar Status
Description: Gerenciador de envio de email para os clientes, conforme mudança de status do woocommerce
Version: 1.0
Author: Raphael Ribas D´Avila
Author URI: http://www.feeshop.com.br
*/

/* Define variáveis estáticas */

// Função ativar	
	@register_activation_hook( $pathPlugin, array('status','ativar'));
 
	// Função desativar
	@register_deactivation_hook( $pathPlugin, array('status','desativar'));
 
	//Ação de criar menu
	add_action('admin_menu', array('status','criarMenu'));
	
	
	
class status {
	
	 public function ativar(){
	 	add_option('status', '');
	 }
	 
	 public function desativar(){
	 	delete_option('status');
	 }
	 
	 public function criarMenu(){
		add_menu_page('status','Enviar Status','edit_pages', plugin_dir_path( __FILE__ ).'/status.php','', 'dashicons-index-card', 58);		  
	 }	
}
?>