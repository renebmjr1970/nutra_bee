<?php
/*
Plugin Name: Relatorio Usuarios Cadsatrados Avise-me
Description: Plugin responsável por gerenciar os cadsatros de avise-me
Version: 5.0b (1.0)
Author: Rene Ballesteros Machado Junior
Author URI: http://www.feeshop.com.br
*/

/* Define variáveis estáticas */

// Função ativar	
	@register_activation_hook( $pathPlugin, array('aviseme','ativar'));
 
	// Função desativar
	@register_deactivation_hook( $pathPlugin, array('aviseme','desativar'));
 
	//Ação de criar menu
	add_action('admin_menu', array('aviseme','criarMenu'));
	
	
	
class aviseme {
	
	 public function ativar(){
	 	add_option('aviseme', '');
	 }
	 
	 public function desativar(){
	 	delete_option('aviseme');
	 }
	 
	 public function criarMenu(){
		add_menu_page('aviseme','Avise-me Registros','edit_pages', 'aviseme/lista.php','', 'dashicons-id');		  
	 }	
}
?>