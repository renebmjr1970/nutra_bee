<footer role="footer">
	<div class="help-and-support">	
		<div class="center-content">
			<h1 class="title-rulers">CENTRAL DE ATENDIMENTO</h1>
		</div>
		<div class="information">
			<div class="generic-support column">
				<div class="blob">
					<span>AJUDA E SUPORTE</span>
				</div>
				<ul class="help">
					<li><a href="<?php echo get_bloginfo('url');?>/trocas-e-devolucoes">Trocas e Devoluções</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/politica-de-reembolso">Política de Reembolso</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/como-comprar">Como Comprar</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/entrega-e-frete">Entrega e Frete</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/formas-de-pagamento">Formas de Pagamento</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/seguranca-e-privacidade">Segurança e Privacidade</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/perguntas-frequentes">Perguntas Frequentes</a></li>
				</ul>
			</div>
			<div class="contact column">
				<div class="blob">
					<span>S.A.C</span>
				</div>
				<ul class="contact-segment">
					<li>
						<p class="segment">SAC</p>
						<span class="numbers"> 11 3683.0306</span>
					</li>
					<li>
						<p class="segment">WHATSAPP</p>
						<span class="numbers">11 97225.3257</span>
					</li>
					<li>
						<p class="segment">E-MAIL</p>
						<span>contato@nutracorpore.com.br</span>
					</li>
					<li>
						<p class="segment">HORÁRIO DE ATENDIMENTO</p>
						<span>Segunda a Quinta das 9h às 19h</span>
						<span>Sexta até as 18h</span>
						<span>Sábado das 9h às 13h</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="darken">
		<div class="center-content">
			<div class="institutional">
				<h1 class="category-title red">INSTITUCIONAL</h1>
				<ul>
					<li><a href="">HOME</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/quem-somos">QUEM SOMOS</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/cadastro">CADASTRE-SE</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/fale-conosco">FALE CONOSCO</a></li>
				</ul>
			</div>
			<div class="ajuda-e-suporte">
				<h1 class="category-title red">AJUDA E SUPORTE</h1>
				<ul>
					<li><a href="<?php echo get_bloginfo('url');?>/trocas-e-devolucoes/">TROCAS E DEVOLUÇÕES</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/politica-de-reembolso/">POLÍTICA DE REEMBOLSO</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/como-comprar">COMO COMPRAR</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/entrega-e-frete">ENVIO</a></li>
				</ul>
			</div>
			<div class="profile">
				<h1 class="category-title red">PERFIL</h1>
				<ul>
					<li><a href="<?php echo get_bloginfo('url');?>/minha-conta">MINHA CONTA</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/meus-pedidos">MEUS PEDIDOS</a></li>
				</ul>
			</div>
			<div class="articles">
				<h1 class="category-title red">ARTIGOS</h1>
				<?php 
				wp_nav_menu( array(
					'menu' => 'menu_categoria',
					'theme_location' => 'menu_categoria',
					'menu_class' => 'mobile-topics',
					'echo' => true,
					'depth' => 0,
					) );
				?>
			</div>
			<div class="product-types">
				<h1 class="category-title red">PRODUTOS</h1>
				<ul>
					<li><a href="<?php echo get_bloginfo('url')."/categoria/massa-muscular/"; ?>">MASSA MUSCULAR</a></li>
					<li><a href="<?php echo get_bloginfo('url')."/categoria/energia-e-resistencia/"; ?>">ENERGIA E RESISTÊNCIA</a></li>

					
					<li><a href="<?php echo get_bloginfo('url')."/categoria/definicao-muscular/"; ?>">DEFINIÇÃO MUSCULAR</a></li>
					<li><a href="<?php echo get_bloginfo('url')."/categoria/emagrecimento/"; ?>">EMAGRECIMENTO</a></li>
					<li><a href="<?php echo get_bloginfo('url')."/categoria/acessorios-esportivos/"; ?>">ACESSÓRIOS ESPORTIVOS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/marcas'; ?>">MARCAS</a></li>
					<li><a href="<?php echo get_bloginfo('url')."/categoria/kits-promocionais/"; ?>">KITS PROMOCIONAIS</a></li>
				</ul>
				<h2 class="red">SEGURANÇA</h2>
				<img src="<?php echo get_bloginfo('template_url');?>/images/common/comodo.png" alt="Logotipo Comodo certificado de segurança SSL">
			</div>
			<div class="social">
				<p>
					Novidades, Artigos, Dicas e Ofertas Exclusivas, cadastre-se.
				</p>
				<form method="post" class="validate newsletter" target="_blank" novalidate>
					<input type="email" value=""  name="EMAIL" placeholder="Digite seu E-mail" class="required email newsletter-mail news-EMAIL" >
					<input type="button" value="ENVIAR" name="subscribe" class="saveNewsletter-footer red-basic button">

					<div class="clear">
						<div class="response response-news" style="display:none"></div>
					</div>
				</form>
				<p>
					Nos acompanhe também nas Redes Sociais:
				</p>
				<a href="https://www.facebook.com/nutracorpore" target="_blank">	
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="https://www.youtube.com/user/nutracorpore" target="_blank">	
					<i class="fa fa-youtube" aria-hidden="true"></i>
				</a>
				<a href="https://www.instagram.com/nutracorpore" target="_blank">	
					<i class="fa fa-instagram" aria-hidden="true"></i>
				</a>
				<h2 class="red">FORMAS DE PAGAMENTO</h2>
				<img src="<?php echo get_bloginfo('template_url');?>/images/common/cards.png" alt="VISA, MasterCard, American Express, Dinners, ELO e Boleto Bancário">
			</div>
			<div class="logo">
				<img src="<?php echo get_bloginfo('template_url');?>/images/common/logo.png" alt="Logotipo NutraCorpore">
			</div>
		</div>
		<div class="copyright">
			<div class="center-content">	
				<p>Nutracorpore - CNPJ: 18.572.130/0001-64 - Rua Dona Primitiva Vianco, 939 - Centro - Osasco - São Paulo - CEP: 06010-000 | Copyright 2016 - Todos os direitos reservados</p>
				<a href="http://www.feeshop.com.br" target="_blank">
					<img src="<?php echo get_bloginfo('template_url');?>/images/common/feeshop.png" alt="Desenvolvido pela Agência Feeshop">
				</a>
			</div>
		</div>
	</div>
</footer>
<script src="https://use.fontawesome.com/dc89892f3f.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/src/jquery.fancybox-thumbs.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.20/jquery.zoom.min.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/dist/app.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.3/jquery.mask.min.js"></script>
<script data-require="sweet-alert@*" data-semver="0.4.2" src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/0.4.2/sweet-alert.min.js"></script>

<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>

<!--<script type="text/javascript">
	var _tn = _tn || [];
	_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
	_tn.push(['_setAction','track-view']);
	(function() {
	document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
	var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
	tss.src = '//www.tolvnow.com/tracker/tn.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
	})();
</script> -->

<script src="<?php echo get_bloginfo('url') ?>/wp-content/plugins/woocommerce/assets/js/frontend/cart.js"></script>
<script type="text/javascript">
	jQuery(window).click(function() {
        jQuery("#sugestao").hide('fast');
    });
</script>

<script>

$(document).ready(function(){

var url = "//www.nutracorpore.com.br/nutracorpore/?logout=sair";

if(document.URL == url) {
	window.location.href = "//www.nutracorpore.com.br/nutracorpore";
}







})

</script>


</body>
</html> 
