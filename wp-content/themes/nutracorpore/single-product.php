<?php 
get_header();

set_time_limit(0);

$current_user = wp_get_current_user();

@$user_id = $current_user->ID;
@$nome = $current_user->display_name;
@$user_email = $current_user->user_email;


$product_variation = new WC_Product_Variable( get_the_ID() );
$variations = $product_variation->get_available_variations();

$content_post = get_post(get_the_ID());
$content = $content_post->post_content;

$payment_options   = get_option('woocommerce_cielo_credit_settings');
$payment_discounts = get_option('woocommerce_payment_discounts');

$product_values = $woocommerce->product_factory->get_product(false, array('ID' => get_the_ID() ));
?>
<main class="product-wrapper">
	<
	<div class="breadcrumb">
		<div class="center-content">	
			<p class="path">
				<a href=""><span> DEFINIÇÃO MUSCULAR /</span></a>
				<a href=""><span> MASSA MUSCULAR /</span></a>
				<a href=""><span> MEU OBJETIVO  /</span></a>
				<a href=""><span> MASSA MAGRA /</span></a>
				<a href=""><span class="actual"> PROTEÍNAS</span></a>
			</p>
		</div>
	</div>
	
	
	<?php woocommerce_breadcrumb(); ?>

	<section class="product-page">
		<div class="center-content">
			<div class="product-details-segment">	
				<h1 class="product-title bordered">
					<?php echo get_the_title(); ?>
				</h1>
				<div class="bordered">
					<p class="topic">MARCA:</p>
					<h2 class="brand-final"><?php echo get_field('marca', get_the_ID())->post_title; ?></h2>
				</div>
				<fieldset class="rating bordered">
					<legend class="sr-only">De uma nota ao produto:</legend>
					<p class="topic">AVALIAÇÕES:</p>
					<?php
						$post_id = get_the_ID();

						$rating = $wpdb->get_results('SELECT count, total_rating FROM product_rating WHERE post_id = '.get_the_ID());
						
						if ( count($rating) == 0 ) {
							$average = 0;
						} else {
							foreach ($rating as $value) {
								# code...
							}
							$count = $value->count;
							$total_rating = $value->total_rating;
							$average = ceil($total_rating / $count);
						}
												
					?>
					<form class="product-rating">	
						<input <?php echo ($average == 5) ? 'checked' : ''; ?> type="radio" id="star1" name="rating" value="5" data-id="<?php echo get_the_ID(); ?>" style="background: #000"><label for="star1" title="Uma estrela"></label>
						<input <?php echo ($average == 4) ? 'checked' : ''; ?> type="radio" id="star2" name="rating" value="4" data-id="<?php echo get_the_ID(); ?>"><label for="star2" title="Duas estrelas"></label>
						<input <?php echo ($average == 3) ? 'checked' : ''; ?> type="radio" id="star3" name="rating" value="3" data-id="<?php echo get_the_ID(); ?>"><label for="star3" title="Três estrelas"></label>
						<input <?php echo ($average == 2) ? 'checked' : ''; ?> type="radio" id="star4" name="rating" value="2" data-id="<?php echo get_the_ID(); ?>"><label for="star4" title="Quatro estrelas"></label>
						<input <?php echo ($average == 1) ? 'checked' : ''; ?> type="radio" id="star5" name="rating" value="1" data-id="<?php echo get_the_ID(); ?>"><label for="star5" title="Cinco estrelas"></label>
					</form>
				</fieldset>
				
				<form id="shipping-product">
					<div class="freight-calculation bordered">
						<p class="topic">CALCULE O FRETE:</p>
						<input type="hidden" name="action" value="shippingProduct">
						<input type="hidden" name="product_id" value="<?php echo get_the_ID(); ?>">
						<input type="hidden" name="valor" value="<?php echo get_field('_regular_price') ?>" >
						<input type="text" name="zipcode">
						<button type="submit">
							<i class="fa fa-angle-right" aria-hidden="true"></i>	
						</button>
					</div>
				</form>
				<div id="loading" style="display: none;margin: 0 auto;text-align: center;padding: 10px 0;"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/nutracorpore/images/icons/loader.gif"></div>			

					<table class="freight-information" id="shipping_totals" style="display: none;">
						<tr>	
							<th>Valor do Frete</th>
							<th>Disponibilidade</th>
						</tr>				
					</table>		

				<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sabe seu cep? Clique aqui.</a>
			</div>
			<div class="product-details-segment product-picture">
				<div>	
					<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="">
				</div>
				<!-- <div>	
					<img src="https://placeholdit.imgix.net/~text?txtsize=27&txt=285%C3%97385&w=285&h=385" alt="">
				</div>
				<div>	
					<img src="https://placeholdit.imgix.net/~text?txtsize=27&txt=285%C3%97385&w=285&h=385" alt="">
				</div> -->
			</div>
			<div class="product-details-segment product-details-segment-boxtoFix">
				<?php

				//echo "<pre>";
				//print_r($product_values);
				//echo "</pre>";

				if ($product_values->product_type == 'variable') {
					$price = $variations[0]['display_price'];
					$full_price = $variations[0]['display_regular_price'];
				} else {
					//$price = $variations[0]['display_price'];
					//$full_price = $variations[0]['display_regular_price'];
					$full_price = get_field('_regular_price');
					$price 		= get_field('_sale_price');
				}

				/*if ($product_values->product_type == 'variable') {
					$full_price = get_field('_max_variation_price');
					if (get_field('_min_variation_price') < $full_price) {
						$price = $variations->display_price;
						$price = get_field('_sale_price');
					}
				} else {
					$full_price = get_field('_regular_price');
					$price 		= get_field('_sale_price');
				}*/
				
				?>
				<div class="price-and-payment">
					<?php
					if ($price > 0 && $price != $full_price) {
						$installments = number_format(($price / $payment_options['installments']), 2, ',', '.');

						?>
						<p class="discounted-price">De <span class="dashed">R$ <?php echo number_format($full_price, 2, ',', '.'); ?></span> por R$ 
							<?php echo number_format($price, 2, ',', '.'); ?></p>
							<?php
						} else {
							$installments = number_format(($full_price / $payment_options['installments']), 2, ',', '.');
							?>
							<p class="discounted-price">Por R$ <?php echo number_format($full_price, 2, ',', '.'); ?></p>
							<?php
						}
						?>

							<?php 

							$n = 1;

							while($n < 7){

								//echo "fp: $full_price<br>";
								//echo "price: $price<br>";
								if(is_numeric($price) && $price >0 && $price<$full_price){
									$full_price = $price;
								}

								$parcela = $full_price / $n;

								if( $full_price <= 40  ){

									$final = '1x de <span class="installment-price"> R$ '.$full_price;

								}else{

									if( $parcela <= 40 ){
										echo "";
									}else{

										$final = $n.'x de <span class="installment-price"> R$ '.str_replace('.',',',number_format($parcela,2));

									}

								}

								$n++;

							}

							?>

						<!-- <p class="final-installments">Em até <?php echo $payment_options['installments'];?>x de <span class="installment-price">R$ <?php echo $installments; ?></span> sem juros</p>-->

						<p class="final-installments"><?php echo  $calculo = " em até ".$final."</span> sem juros"; ?></p>

						<?php

						$final_price = ($price > 0) ? $price : $full_price;
						$price_aux = number_format( $final_price, 2 );
						$price_aux = explode( '.', $final_price );

						//echo "preco final: ".number_format($final_price,2,',','.')."<br>";
						//echo "price_aux: ".$price_aux;
						
						//die();

						?>
						<span class="xg discount-price">
							R$ <?php $tirar = ($price_aux[0] * 10)/100 ; ?>
							<span style="font-size: 7.2rem;">
								<?php

									echo str_replace(".",",",number_format($price_aux[0] - $tirar, 2));

								?>
							</span>
								<?php
									if(stripos($com,",") == 3){
										echo "00";
									}else{
										false;
									}
								?>
						</span>

						<small>No boleto, depósito ou transferência</small>

					</div>
						
						<div class="promotional-information">
							<?php
							if ($price > 0 && $price != $full_price) {
								 
								$percentChange = number_format((1 - $price / $full_price) * 100);
								?>
								<span class="discount-stripe"><?php echo number_format($percentChange, 0); ?> <small>OFF</small></span>
								<?php
							}
							?>
							<?php 
							if (get_field('frete_gratis')) { 
								?>
								<span class="generic-stripe yellow">FRETE GRÁTIS</span>
								<?php
							}
							?>
							<?php 
							if (get_field('lancamento')) { 
								?>
								<span class="generic-stripe orange">LANÇAMENTO</span>
								<?php
							}
							?>
							<!-- <span class="generic-stripe blue">COMPRE GANHE</span> -->
						</div>

						<?php if ($product_values->product_type == 'variable') : ?>
						<div class="product-options">
							<select class="bordered" id="variation" onclick="validar_comprar()">
								<option value="0" disabled selected>
									SELECIONE A VARIAÇÃO
								</option>
								<?php
								foreach ($variations as $v) {
									if ($v['max_qty'] > 0) {
										?>
										<option value="<?php echo $v['variation_id']; ?>">
											<?php echo strtoupper(reset($v['attributes'])); ?>
										</option>
										<?php
									}
								}
								?>	
							</select>
							<br>
						</div>
					<?php endif; ?>

					<br><br>
				<?php
					if (get_field('_stock_status') == 'instock') :
				?>
						<button class="add-to-cart add-cart" data-id="<?php echo get_the_ID();?>" data-variation="0" 
							data-variable="<?php echo $product_values->product_type; ?>">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							COMPRAR PRODUTO
						</button>
				<?php else: ?>
					<div class="notstock"><i class="fa fa-warning"></i>FORA DE ESTOQUE</div>

					<div id="box-avise-me">
						
						<span class="loadingQtd" style="display:none;width: 100%;text-align: center;padding-top: 20px;">
							<img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/nutracorpore/images/default.svg">
						</span>
						
						<div id="msg-avise" style="text-align: center;padding-top: 10px;font-size: 15px;color: #038e03;display:none;">SEU E-MAIL FOI REGISTRADO COM SUCESSO!</div>

						<div class="content-aviseme">
							<br>
							<span id="txt-avise-me">Avise-me quando chegar</span>
							<div id="campos-aviseme">
								<form name="aviseme" id="aviseme" method="post">
									<input type="hidden" name="aviseme_user_id" id="aviseme_user_id" value="1">
									<input type="hidden" name="aviseme_produto_id" id="aviseme_produto_id" value="4957">
									<input type="hidden" name="aviseme_produto" id="aviseme_produto" value="L-carnitine 120caps – Probiótica">
									<input type="text" name="aviseme_nome" id="aviseme_nome" class="formulario-aviseme" width="20" placeholder="Digite seu nome"><!--
									--><input type="text" name="aviseme_email" id="aviseme_email" class="formulario-aviseme" width="20" placeholder="Digite seu e-mail">
									<input name="Submit" type="button" id="btn-ok-aviseme" value="OK">
								</form>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if (get_field('_stock_status') == 'instock') : ?>
					<div class="safety" id="safety">
						<i class="fa fa-shield" style=""></i> AMBIENTE 100% SEGURO
					</div>
				<?php endif; ?>

			</div>
		</div>
	</section>
	<section class="product-description">
		<article class="product-segment">
			<div class="center-content">	
				<h1 class="title-rulers single">
					<i class="icons-detail detalhes-i"></i> DETALHES DO PRODUTO
				</h1>
				<div class="product-single-content">
					<?php echo $content; ?>
				</div>
			</div>
		</article>
		<article class="product-segment">
			<div class="center-content">	
				<h1 class="title-rulers single">
					<i class="icons-detail beneficios-i"></i> BENEFÍCIOS
				</h1>
				<?php 
					if ( get_field('beneficios') == '' ) {
						echo '<p>Produto sem benefícios</p>';
					} else {
						echo get_field('beneficios'); 
					}
				?>
				<!-- <ul>
					<li>-Whey Protein Isolado</li>
					<li>-Whey Protein Hidrolisado </li>
					<li>-Whey Protein Concentrado </li>
					<li>-Máximo desempenho </li>
					<li>-25g de proteínas por dose;</li>
					<li>-Ação rápida; </li>
					<li>-Fonde de aminoácidos essenciais.</li>
				</ul> -->
			</div>
		</article>
		<article class="product-segment">
			<div class="center-content">	
				<h1 class="title-rulers single">
					<i class="icons-detail sugestao-i"></i> SUGESTÃO DE USO
				</h1>
				<?php 
					if ( get_field('sugestao_de_uso') == '' ) {
						echo '<p>Produto sem Sugestão de Uso</p>';
					} else {
						echo get_field('sugestao_de_uso'); 
					}
				?>
			</div>
		</article>
		<article class="product-segment">
			<div class="center-content">
				<h1 class="title-rulers single">
					<i class="icons-detail informacao-i"></i>INFORMAÇÃO NUTRICIONAL
				</h1>
				<div class="table-description">
					<p>
					<?php echo get_field('informações_nutricionais'); ?>
					</p>
				</div>
			
		</article>
	</section>
	
	<section class="offers">
		<div class="center-content">	
			<h1 class="title-rulers">VEJA TAMBÉM</h1>	
			<div class="grid-products greyed five">
			<?php
				$terms = wp_get_post_terms( $post->ID, 'product_cat' );
				
				foreach ( $terms as $term ) $cats_array[] = $term->term_id;

				$args = array(
					'post_type' => 'product',
					'posts_per_page' => 5,
					'post__not_in' => array( $post->ID ),
	                'key' => '_stock_status',
	                'value' => 'instock',
	                'no_found_rows' => 1,
	                'post_status' => 'publish',
					'tax_query' => array( 
						array(
							'taxonomy' => 'product_cat',
							'field' => 'id',
							'terms' => $cats_array
						)
					)
				);

				$loop = new WP_Query( $args );
				if ( $loop->have_posts() ) :
					while ( $loop->have_posts() ) : $loop->the_post();
						$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
			?>
				<div class="product">
					<figure>
						<?php 
						if (get_field('frete_gratis')) { 
							?>
							<span class="generic-stripe orange">FRETE GRÁTIS</span>
							<?php
						}
						?>
						<?php 
						if (get_field('lancamento')) { 
							?>
							<span class="generic-stripe green">LANÇAMENTO</span>
							<?php
						}
						?>
						<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
						<div class="overlay">
							<a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
							<?php echo ( $product->is_in_stock() && $product->product_type != 'variable' ) ? '' : 'no-add';?>">
								<i class="fa fa-search" aria-hidden="true"></i>
							</a>
							<?php
							if ( $product->is_in_stock() && $product->product_type != 'variable' ) :
							?>
							<button class="add-cart" data-id="<?php echo get_the_ID();?>">
								<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							</button>
							<?php
							endif;
							?>
						</div>
						<figcaption class="name">
							<a href="<?php echo get_the_permalink(); ?>">
								<?php echo get_the_title(); ?>
							</a>
						</figcaption>
					</figure>
					<p class="full-price">
						R$ <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
						<span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
					</p>
					<p class="discount-price">
						<?php
						$percent 		   = str_replace('%', '', $payment_discounts['boleto']['amount']);
						$price = $product->price - ($product->price * ($percent / 100));
						$price = number_format($price, 2, '.', ',');
						$price = explode('.', $price);
						?>
						<?php echo $price[0]; ?>,<span class="cents"><?php echo (! isset($price[1])) ? '00' : $price[1]; ?></span>
					</p>
					<small>no boleto ou depósito</small>
				</div>
			<?php
				endwhile;
			endif;
			?>
			</div><!--ENDGRID -->
		</div><!--ENDCENTER -->
	</section>
</main>

<?php 

setPostViews($post_id);
get_footer();
?>
<script type="text/javascript">

	function valida_avisa_indisponivel(){
		alert('avise-me');
	}

</script>

<script>

function clicando(){

	swal("Exceto Rondônia e Roraima!");
	
}

</script>