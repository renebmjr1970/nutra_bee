<?php
// carregamos o core do wordpress
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );
global $wpdb;

// conexao com a base do site antigo em localhost, ALTERAR PARA RESGATAR OS DADOS DO SITE EM PRODUÇÃO
//$antigo = new PDO('mysql:host=localhost;charset=utf8;dbname=nutracorpore', 'nutracorpore', '');
  $antigo = new mysqli('localhost','root','','nutracorpore'); 
// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------

// vamos buscar na base antiga os produtos com variacao. Pegearemos a referencia e os ids das variacoes
$sql = "SELECT
			pcarac.ccodproduto,
			pcarac.ccodpropriedade,
			prod.preferencia,
			prod.pcodigo,
			pprop.ppropriedade,
			prod.pproduto,
			prod.pvalor_unitario
		FROM
			tblproduto_caracteristica AS pcarac
		INNER JOIN tblproduto AS prod ON pcarac.ccodproduto = prod.produtoid
		INNER JOIN tblproduto_propriedade AS pprop ON pcarac.ccodpropriedade = pprop.propriedadeid
		ORDER BY
			pcarac.ccodproduto ASC";
$results = $antigo->query($sql);

foreach ($results as $dados) {
	$ccodproduto[] 		= $dados['ccodproduto'];
	$ccodpropriedade[]	= $dados['ccodpropriedade'];
	$preferencia[] 		= $dados['preferencia'];
	$pcodigo[] 			= $dados['pcodigo'];
	$ppropriedade[]		= $dados['ppropriedade'];
	$pproduto[]			= $dados['pproduto'];
	$pvalor_unitario[]	= $dados['pvalor_unitario'];
	
	$sqlprod[]			= "SELECT
								posts.ID
							FROM
								wp_posts AS posts
							WHERE
								posts.post_title = '".$dados['pproduto']."'
							AND posts.post_date != '0000-00-00 00:00:00'";

}
$result_sqlprod 		= array_unique($sqlprod); 

// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------

// Aqui comecamos a fazer as inclusoes das variacoes
$variacoes = 'a:1:{s:9:"variacoes";a:6:{s:4:"name";s:11:"Variações";s:5:"value";s:1027:"Chocolate | Baunilha | Morango | Morango C/ Banana | Cookies | Cappuccino | Berry Blast | Fruit Punch | Blue Raspberry | Açai C/ Guarana | Limão | Laranja | Tangerina | Uva | Cereja Preta | Natural | Frutas Vermelhas | Manga Selvagem | Peanut Butter | Amendoin | Chocolate C/ Avela | Kiwi C/ Limão | Coco | Apple Melon | Mocha Cappuccino | Banana | Watermelon | Tamanho P | Tamanho M | Tamanho G | Rocky Road | Chocolate C/ Menta | Chocolate/Peanut Butter | Frango | Exotic Fruit | 2,268kg | 900g | Abacaxi | Apple Ambush | Raspberry Smoothi | Açai | Guaraná | Cereja | Amora | Napolitano | Mirtilo e Limão | Fudge Brownie | Pineapple mango | Melancia | 1364g | 907g | Kiwi com Morango | 726g | Sucker Punch | Chocolate ao leite | Pera | Rosa | Verde | Amarelo | Sorvete de Creme | Iogurte de Morango | Limonada Suiça | Cranberry | Laranja com Acerola | Maça Verde | Doce de Coco | Romeu e Julieta | Trufa de avelã | Torta de limão | Blueberry | Vitamina de Frutas | Pão de mel | Chá-verde | Chocolate Peanut Butter";s:8:"position";s:1:"0";s:10:"is_visible";i:1;s:12:"is_variation";i:1;s:11:"is_taxonomy";i:0;}}';


for ($i = 0; $i <= 700; $i++) {
	$sql =  $result_sqlprod[$i].';';

	echo $result_sqlprod

	if($sql != ";") {
		$post_id = $wpdb->get_var($sql);
		
		echo "post_id: ".$post_id."<br>";
		
		$sql_var = "INSERT INTO wp_postmeta(post_id, meta_value, meta_key) VALUES ('$post_id','$variacoes','_product_attributes')";
		$wpdb->query($sql_var);
		
		$sql_var1 = "INSERT INTO wp_postmeta(post_id, meta_key) VALUES ('$post_id','_min_variation_sale_price')";
		$sql_var2 = "INSERT INTO wp_postmeta(post_id, meta_key) VALUES ('$post_id','_max_variation_sale_price')";
		$sql_var3 = "INSERT INTO wp_postmeta(post_id, meta_key) VALUES ('$post_id','_min_sale_price_variation_id')";
		$sql_var4 = "INSERT INTO wp_postmeta(post_id, meta_key) VALUES ('$post_id','_max_sale_price_variation_id')";
		$sql_var5 = "INSERT INTO wp_postmeta(post_id, meta_key) VALUES ('$post_id','_default_attributes a:0:{}')";
		$wpdb->query($sql_var1);
		$wpdb->query($sql_var2);
		$wpdb->query($sql_var3);
		$wpdb->query($sql_var4);
		$wpdb->query($sql_var5);
	}
}


unset($ccodproduto); 	
unset($ccodpropriedade);
unset($preferencia); 	
unset($pcodigo); 		
unset($ppropriedade);	
unset($pproduto);
unset($pvalor_unitario);
unset($post_id);
unset($i);

// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------
// Aqui vamos buscar o valor e variacoes na base antiga
$sql = "SELECT
			pcarac.ccodproduto,
			pcarac.ccodpropriedade,
			prod.preferencia,
			prod.pcodigo,
			pprop.ppropriedade,
			prod.pproduto,
			prod.pvalor_unitario
		FROM
			tblproduto_caracteristica AS pcarac
		INNER JOIN tblproduto AS prod ON pcarac.ccodproduto = prod.produtoid
		INNER JOIN tblproduto_propriedade AS pprop ON pcarac.ccodpropriedade = pprop.propriedadeid
		ORDER BY
			pcarac.ccodproduto ASC";
$results = $antigo->query($sql);

foreach ($results as $dados) {
	$ccodproduto[] 		= $dados['ccodproduto'];
	$ccodpropriedade[]	= $dados['ccodpropriedade'];
	$preferencia[] 		= $dados['preferencia'];
	$pcodigo[] 			= $dados['pcodigo'];
	$ppropriedade[]		= $dados['ppropriedade'];
	$pproduto[]			= $dados['pproduto'];
	$pvalor_unitario[]	= $dados['pvalor_unitario'];
	
	$sqlprod			= "SELECT
								posts.ID
							FROM
								wp_posts AS posts
							WHERE
								posts.post_title = '".$dados['pproduto']."'
							AND posts.post_date != '0000-00-00 00:00:00'";
	$post_id[] = $wpdb->get_var($sqlprod);
}
echo "<pre>";
print_r($pproduto);
echo "</pre>";
echo "<hr>";


echo "<pre>";
print_r($ccodpropriedade);
echo "</pre>";
echo "<hr>";

echo "<pre>";
print_r($ppropriedade);
echo "</pre>";
echo "<hr>";


echo "<pre>";
print_r($pvalor_unitario);
echo "</pre>";
echo "<hr>";

echo "<pre>";
print_r($post_id);
echo "</pre>";
echo "<hr>";

$conta = 1;
for ($i = 0; $i <= count($pproduto)-1; $i++) {
	if($pproduto[$i] != $produto_velho) {
		$conta = 1;
	}
	$pproduto_spc 	= htmlspecialchars($pproduto[$i],ENT_QUOTES);
	echo $i." --> ".$pproduto[$i]." --> ".$pproduto_spc." --> ".$conta."<br>";
	$produto_velho 	= $pproduto[$i];
	$conta++;
}

// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------
echo "fim";
?>