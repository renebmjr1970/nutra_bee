<?php 
	if (! is_user_logged_in()) {
		header('location: '.get_bloginfo('url').'/login');
	}
	get_header();

	global $woocommerce;

?>
<div class="center-content">
	<?php 
		include 'promotional.php'; 
		global $woocommerce, $current_user;
	?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">	
			<section class="account-information">
				<h2 class="full-lined red">MEU ENDEREÇO</h2>
				<form id="form-edit-account-1" class="edit-account" action="<?php echo get_bloginfo('template_url'); ?>/editaendereco.php" method="post" id="formformulario">

					<fieldset>
						<legend class="sr-only">'Edite aqui as informações da sua conta</legend>

						<label class="encapsule">
							<span>CEP:</span>
							<input type="text" class="field" name="cep" disabled value="<?php echo $woocommerce->customer->shipping_postcode; ?>">
						</label>

						<label class="encapsule">
							<span>Endereço:</span>
							<input type="text" class="field" name="endereco" disabled value="<?php echo $woocommerce->customer->shipping_address_1; ?>">
						</label>

						<label class="encapsule">
							<span>Número:</span>
							<input type="text" class="field" name="numero" disabled value="<?php echo get_user_meta($current_user->ID, 'billing_number', true) ?>">
						</label>

						<label class="encapsule">
							<span>Complemento:</span>
							<input type="text" class="field" name="complemento" disabled value="<?php if(get_user_meta($current_user->ID, 'complement', true) == ""){echo "S/N";}else{echo get_user_meta($current_user->ID, 'complement', true);} ?>">
						</label>

						<label class="encapsule">
							<span>Referência:</span>
							<input type="text" class="field" name="referencia" disabled value="<?php if(get_user_meta($current_user->ID, 'reference', true) == ""){echo "S/N";}else{echo get_user_meta($current_user->ID, 'reference', true);} ?>">
						</label>

						<label class="encapsule">
							<span>Bairro:</span>
							<input type="text" class="field" name="bairro" disabled value="<?php echo get_user_meta($current_user->ID, 'neighborhood', true); ?>">
						</label>

						<label class="encapsule">
							<span>Cidade:</span>
							<input type="text" class="field" name="cidade" disabled value="<?php echo $woocommerce->customer->city; ?>">
						</label>

						<label class="encapsule">
							<span>Estado:</span>
							<input type="text" class="field" name="estado" disabled value="<?php echo $woocommerce->customer->state; ?>">
						</label>

					</fieldset>

					<button class="generic-blue" id="edit" type="button" data-action="edit">EDITAR</button>
				</form>

			</section>
		</main>
	</div>
</div>
<?php 
	get_footer(); 
?>
<script type="text/javascript">

	
	


	var clique = 1;
	$("#edit").click(function(){
		var botao = $("#edit").html();
		
		if(clique >1 && botao == "ENVIAR"){
			var form = $("#form-edit-account-1");
			var serializedForm = form.serialize();

			//console.log(serializedForm);

			if ( form.validate() ) {
				var url_site = 'https://'+window.location.hostname+'/';

				$.ajax({
					url: url_site+'wp-content/themes/nutracorpore/editaendereco.php',
					data: serializedForm,
					type: 'POST',
					success: function(data) {
						var dataJSON = JSON.parse(data);
						var status = dataJSON.status;
						var statusMessage = dataJSON.message;

						//console.log(dataJSON);

						swal({
							type: status,
							title: statusMessage
						}, function(){
							location.reload();
						});
					}
				});
			}
		}
    	clique +=clique;
	});
</script>