<?php get_header(); ?>

<?php
function glossario($letra) {
	global $wpdb;
	$sql = "SELECT
				post.post_title,
				post.post_name
			FROM
				wp_posts AS post
			WHERE
				post.post_type = 'marca'
			AND post.post_title LIKE '$letra%'
			ORDER BY
				post.post_title ASC";
	$results = $wpdb->get_results($sql);

	foreach ($results as $r) :
		echo '<li><a href="'.get_bloginfo('url').'/categoria/'.$r->post_name.'">'.$r->post_title.'.</a></li>';
	endforeach;
}
?>

<main>
	<section class="branding">
		<div class="center-content cleared">
			<h1 class="lined bigger">MARCAS</h1>
			<ul class="option-submenu brands five">
				<?php
				$args = array(
					'post_type' => 'marca',
					'posts_per_page' => 15
					);
				$loop = new WP_Query( $args );
				$cnt = 0;
				if ( $loop->have_posts() ) :
					while ( $loop->have_posts() ) : $loop->the_post();
				if ($cnt == 0) :
					?>
				<li>
					<ul>
						<?php
						endif;
						$post_data = get_post(get_the_ID());
						$post_name = $post_data->post_name;
						?>
						<li>
							<a href="<?php echo get_bloginfo('url') ;?>/categoria/<?php echo $post_name; ?>">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="">
							</a>
						</li>
						<?php
						$cnt++;
						if ($cnt == 3) :
							$cnt = 0;
						?>
					</ul>
				</li>
				<?php
				endif;
				endwhile;
				endif;
				?>
			</ul>
			<p class="full-brand-title">TODAS AS MARCAS</p>
			<div class="full-brand-listing cleared">
				<div class="brand-instances">
					<ul id="marca_A">
						<li class="alphabetical has-icon angle">A</li>
						<?php
						glossario('A');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_B">B</li>
						<?php
						glossario('B');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_C">C</li>
						<?php
						glossario('C');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_D">D</li>
						<?php
						glossario('D');
						?>
					</ul>
				</div>
				<div class="brand-instances">
					<ul>
						<li class="alphabetical has-icon angle marca_E">E</li>
						<?php
						glossario('E');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_F">F</li>
						<?php
						glossario('F');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_G">G</li>
						<?php
						glossario('G');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_H">H</li>
						<?php
						glossario('H');
						?>
					</ul>
				</div>
				<div class="brand-instances">
					<ul>
						<li class="alphabetical has-icon angle marca_I">I</li>
						<?php
						glossario('I');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_J">J</li>
						<?php
						glossario('J');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_K">K</li>
						<?php
						glossario('K');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_L">L</li>
						<?php
						glossario('L');
						?>
					</ul>
				</div>
				<div class="brand-instances">
					<ul>
						<li class="alphabetical has-icon angle marca_M">M</li>
						<?php
						glossario('M');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_N">N</li>
						<?php
						glossario('N');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_O">O</li>
						<?php
						glossario('O');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_P">P</li>
						<?php
						glossario('P');
						?>
					</ul>
					<ul>
						<li class="alphabetical has-icon angle marca_Q">Q</li>
						<?php
						glossario('Q');
						?>
					</ul>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>