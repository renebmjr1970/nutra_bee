<section class="promotional">
	<div class="center-content">
		<?php
			$args = array(
				'post_type' => 'Banner',
				'order' => 'desc'
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) :
				while ( $loop->have_posts() ) : $loop->the_post();
					if (get_field('tipo_de_banner') == 'skyscrape') : 
		?>
					<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="" class="banner">
		<?php
					endif;
				endwhile;
				wp_reset_query();
			endif;
		?>
		<div class="forepart">
			<div>
				<span>10% de DESCONTO</span>
				<i class="fa fa-tag" aria-hidden="true"></i>
				<span>Para compras à vista.</span>
			</div>
			<div>
				<span>FRETE GRÁTIS</span>
				<i class="fa fa-truck" aria-hidden="true"></i>
				<span>Para compras acima de R$ 149,00.</span>
			</div>
			<div>
				<span>TEM UMA DÚVIDA NUTRICIONAL?</span>
				<i class="fa fa-comments" aria-hidden="true"></i>
				<span>Fale com nossa nutricionista <a class="red" href="<?php echo get_bloginfo('url'); ?>/fale-conosco">aqui.</a></span>
			</div>
		</div>
		<p class="touch-warning"></p>
	</div>
</section>