<?php
	if (! is_user_logged_in()) {
		header('Location: '.get_bloginfo('url').'/login');
	}
	get_header();
?>
<div class="center-content">
	<?php include 'promotional.php'; ?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">
			<article class="description-segment">	
				<h2 class="full-lined red">DETALHES DO PEDIDO</h2>
				<div class="details">	
					<p><span class="info">Identificação:</span><span class="response">Identificação:</span></p>
					<p><span class="info">Destinatário:</span><span class="response">Destinatário:</span></p>
					<p><span class="info">Endereço:</span><span class="response">Endereço:</span></p>
					<p><span class="info">Cidade:</span><span class="response">Cidade:</span></p>
					<p><span class="info">Estado:</span><span class="response">Estado:</span></p>
					<p><span class="info">CEP:</span><span class="response">CEP:</span></p>
					<p><span class="info">Ponto de Referência:</span><span class="response">Ponto de Referência:</span></p>
					<p><span class="info">Status:</span><span class="response">Status:</span></p>
					<p><span class="info">Frete:</span><span class="response">Frete:</span></p>
					<p><span class="info">Tipo de Frete:</span><span class="response">Tipo de Frete:</span></p>
					<p><span class="info">Data do Pedido:</span><span class="response">Data do Pedido:</span></p>
				</div>
				<div class="payment-description">
					<div class="order-information">
						<p>Descrição</p>
					</div>
					<div class="order-information">
						<p>Quantidade</p>
					</div>
					<div class="order-information">
						<p>Preço Unitário</p>
					</div>
					<div class="order-information">
						<p>Valor Total</p>
					</div>
				</div>
				<div class="order-description">
					<div class="order-information">
						<p>Lorem Ipsom Sit Dolor</p>
					</div>
					<div class="order-information">
						<p>03</p>
					</div>
					<div class="order-information">
						<p>R$ 999,99</p>
					</div>
					<div class="order-information">
						<p>R$ 999,99</p>
					</div>
				</div>
				<div class="pricing">
					<p>
						<span class="value-info">Sub-Total</span>
						<span class="value-result"><b>R$ 310,00</b></span>
					</p>
					<p>
						<span class="value-info">Frete</span>
						<span class="value-result"><b>R$ 310,00</b></span>
					</p>
					<p>
						<span class="value-info">Total</span>
						<span class="value-result"><b>R$ 310,00</b></span>
					</p>
				</div>
			</article>
			<article class="description-segment">
				<h2 class="full-lined red">DETALHES DO PAGAMENTO</h2>
				<div class="order-summary">	
					<p>
						<span class="value-info">Valor total dos Produtos:</span>
						<span class="value-result"><b>R$ 310,00</b></span>
					</p>
					<p>
						<span class="value-info">Despesas com Frete:</span>
						<span class="value-result"><b>R$ 310,00</b></span>
					</p>
					<p>
						<span class="value-info">Valor Total à Pagar:</span>
						<span class="value-result"><b>R$ 310,00</b></span>
					</p>
					<p class="distanced">
						<span class="value-info">Forma de Pagamento:</span>
						<span class="value-result"><b>Transferência</b></span>
					</p>
					<button class="generic-blue">ENVIAR COMPROVANTE</button>
				</div>
			</article>
		</main>
	</div>
</div>
<?php 
	get_footer();
?>