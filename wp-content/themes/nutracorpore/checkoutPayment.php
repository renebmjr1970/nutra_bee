<?php include_once('header.php'); ?>
<main>
	<section class="checkout">
		<div class="center-content">
			<h1 class="lined">FINALIZAR COMPRA</h1>
			<div class="padded">
				<div class="step">
					<article class="fieldbox shaded-box second-step inactive">
						<h1 class="red">DADOS PESSOAIS</h1>
						<p class="average">Dados já preenchidos.</p>
					</article>
				</div>
				<div class="step">
					<article class="fieldbox shaded-box second-step inactive address">
						<h1 class="red">ENTREGA</h1>
						<p class="average">Rua xxxxxxxxxx - xxxxxxxx</p>
						<p class="average">XXXXXXXXxxxx xxxxxxxxxxxxx</p>
					</article>
					<article class="fieldbox shaded-box second-step payment">
						<h1 class="red">PAGAMENTO</h1>
						<legend class="average">Selecione a forma de Pagamento:</legend>
						<div class="field radiobox">
							<div class="boxed">
								<input type="radio">
							</div>
							<img src="images/common/cards.jpg" alt="Cartões aceitos: Visa, MasterCard, Hipercard, American Express e ELO " class="cards">
						</div>
						<div class="negative">
							<img src="images/common/card-placeholder.jpg" aria-hidden="true" class="card-placeholder">
							<label class="three-quarters">
								<span class="field-descriptor">
									Número do cartão
								</span>
								<input type="text" name="card-number" class="field">
							</label>
							<label class="one-quarter">
								<span class="field-descriptor">
									Validade
								</span>
								<input type="text" name="card-validity" class="field"> 
							</label>
							<label>
								<span class="field-descriptor">Nome do Titular</span>
								<input type="text" class="field" name="holder-name">
							</label>
							<label class="half">
								<span class="field-descriptor overflown">Cód. de segurança</span>
								<input type="text" class="field">
							</label>
							<label class="half">
								<span class="field-descriptor">Parcelas</span>
								<input type="text" class="field">
							</label>
							<p class="price-checkout average">Valor <b>R$ 62,90</b></p>
						</div>
						<div class="field radiobox">
							<div class="boxed">
								<input type="radio">
							</div>
							<span class="payment-method">
								<img src="images/common/billet.jpg" alt="Símbolo boleto bancário - código de barras">	
								<small>Boleto Bancário</small>
							</span>
							<span class="discounted">
								<small>Desconto de 10%</small>
							</span>
						</div>
						<div class="field radiobox">
							<div class="boxed">
								<input type="radio">
							</div>
							<span class="payment-method">
								<img src="images/common/itau.jpg" alt="Símbolo boleto bancário - código de barras">	
								<small>Depósito Banco Itaú</small>
							</span>
							<span class="discounted">
								<small>Desconto de 10%</small>
							</span>
						</div>
					</article>
				</div> 
				<div class="step">
					<article class="order-resume">
						<h2>RESUMO DO PEDIDO</h2>
						<div class="row">
							<span class="checkout-name">Whey Gold Whey Gold Whey Gold Whey</span>
							<span class="checkout-price"><b>R$ 450,50</b></span>
						</div>
						<div class="row">
							<span class="checkout-name">Whey Gold Whey Gold Whey Gold Whey</span>
							<span class="checkout-price"><b>R$ 450,50</b></span>
						</div>
						<div class="order-summary">
							<a href="">Voltar para o carrinho</a>
							<p>
								<span class="value-info">Subtotal:</span>
								<span class="value-result blue"><b>R$ 310,00</b></span>
							</p>
							<p>
								<span class="value-info">Total:</span>
								<span class="value-result"><b>R$ 310,00</b></span>
							</p>
						</div>
						<button class="finish-purchase shaded-box">FINALIZAR COMPRA</button>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
</main>
<?php include_once('footer.php'); ?>