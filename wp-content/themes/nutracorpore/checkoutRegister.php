<?php include_once('header.php'); ?>
<main>
	<section class="checkout">
		<div class="center-content">
			<h1 class="lined">FINALIZAR COMPRA</h1>
			<div class="padded">
				<div class="step">
					<article class="fieldbox shaded-box second-step hacked">
						<h1 class="red">NOVO CADASTRO</h1>
						<form>	
							<fieldset>
								<legend class="field-descriptor">Solicitamos apenas as informações essenciais para a realização da compra.</legend>
								<label>
									<span class="field-descriptor">
										E-mail
									</span>
									<input type="email" name="email" required class="field">
								</label>
								<label>
									<span class="field-descriptor">
										Nome Completo
									</span>
									<input type="text" name="complete-name" required class="field">
								</label>
								<label class="half">
									<span class="field-descriptor">
										CPF
									</span>
									<input type="text" name="CPF" required class="field">
								</label>
								<label class="half">
									<span class="field-descriptor">
										Telefone
									</span>
									<input type="tel" name="phone" required class="field">
								</label>
								<label>
									<span class="field-descriptor">
										Senha
									</span>
									<input type="password" name="password" required class="field">
								</label>
								<label>
									<span class="field-descriptor">
										Confirme sua Senha
									</span>
									<input type="passwordConfirm" name="password-confirm" required class="field">
								</label>
								<label class="half">
									<input type="checkbox">
									<span class="small-question">Quero receber e-mails com promoções</span>
								</label>
								<button class="generic-blue">ENTRAR</button>
							</fieldset>
						</form>
					</article>
				</div>
				<div class="step">
					<article class="fieldbox shaded-box second-step inactive address">
						<h1 class="red">Entrega</h1>
						<p class="average">Aguardando preenchimento dos dados.</p>
					</article>
					<article class="fieldbox shaded-box second-step inactive payment">
						<h1 class="red">PAGAMENTO</h1>
						<p class="average">Aguardando preenchimento dos dados.</p>
					</article>
				</div>
				<div class="step">
					<article class="order-resume">
						<h2>RESUMO DO PEDIDO</h2>
						<div class="row">
							<span class="checkout-name">Whey Gold Whey Gold Whey Gold Whey</span>
							<span class="checkout-price"><b>R$ 450,50</b></span>
						</div>
						<div class="row">
							<span class="checkout-name">Whey Gold Whey Gold Whey Gold Whey</span>
							<span class="checkout-price"><b>R$ 450,50</b></span>
						</div>
						<div class="order-summary">
							<a href="">Voltar para o carrinho</a>
							<p>
								<span class="value-info">Subtotal:</span>
								<span class="value-result blue"><b>R$ 310,00</b></span>
							</p>
							<p>
								<span class="value-info">Total:</span>
								<span class="value-result"><b>R$ 310,00</b></span>
							</p>
						</div>
					</article>
				</div>
			</div>
		</div>
	</section>
</main>
<?php include_once('footer.php'); ?>