<?php 
get_header();
$payment_options   = get_option('woocommerce_cielo_credit_settings');
$payment_discounts = get_option('woocommerce_payment_discounts');

?>
<style>
	.woocommerce-Price-currencySymbol{
		display: none !important;
	}
</style>
<main>
	<section class="purchases">	
		<div class="center-content">	
			<div class="safe">
				<h1 class="lined bigger">CARRINHO</h1>
				<img src="<?php echo get_bloginfo('template_url'); ?>/images/common/safe-enviroment.png" alt="Ambiente 100% seguro - Comodo SSL" class="safe-certificate">
			</div>
			<div class="cart-details padded">
				<div class="entries">
					<?php
						//get_cart_discount_total
					global $woocommerce;

					$woocommerce->session->total = (float) ($woocommerce->cart->cart_contents_total+$woocommerce->cart->fee_total);

					$woocommerce->cart->total = (float) ($woocommerce->cart->cart_contents_total+$woocommerce->cart->fee_total);
										
					// echo "xxx<pre>";
					// print_r($woocommerce->session);
					// echo "</pre><hr>";

					// echo "xxx<pre>";
					// print_r($woocommerce);
					// echo "</pre>";
					
					$items = $woocommerce->cart->get_cart();

					$woocommerce->cart->calculate_totals();

					//echo "<pre>";
					//print_r($payment_discounts);
					//echo "</pre>";
					//die;
					    
					$installments_value = $woocommerce->cart->cart_contents_total / $payment_options['installments'];
					$percent 			= str_replace('%', '', $payment_discounts['boleto']['amount']);

					//echo "installments_value: $installments_value<br>";
					//echo "percent: $percent<br>";
					$discount_value 	= $woocommerce->cart->cart_contents_total - ($woocommerce->cart->cart_contents_total * ($percent / 100));
					//echo "discount_value: $discount_value<br>";
					if($items){
						foreach($items as $item => $values) {
							$_product = $values['data']->post;
							$price = get_post_meta($values['product_id'] , '_price', true);
							?>
							<div class="entry shaded-box">
								<div class="picture">
									<a href="<?php echo $values['data']->post->guid; ?>">
										<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $values['product_id'], 'thumbnail' ) ); ?>" alt="<?php echo $values['data']->post->post_title; ?>" style="width: 100%;">
									</a>
								</div>
								<div class="product-info">
									<h1>
										<a href="<?php echo $values['data']->post->guid; ?>">
											<?php echo $values['data']->post->post_title; ?>
										</a>
									</h1>
									<?php
										$sql  = 'SELECT * FROM wp_posts ';
										$sql .= 'WHERE ID = '.get_post_meta($values['product_id'], 'marca', true);
										$brand = $wpdb->get_results($sql);
									?>
									<h2 class="brand"><?php echo $brand[0]->post_title; ?></h2>

									<button class="remove activate-load" data-id="<?php echo $item; ?>" onclick="removeProduto()" > ✕ </button>

								</div>
								<div class="price-quantity">
									<p>R$ <span><?php echo number_format($values['data']->price, 2, ',', '.'); ?></span>/un</p>	
									<button type="button" class="less numeral"><span>-</span></button>
									<?php
										$quantity = ($values['quantity'] < 10 && !strstr($values['quantity'], '0')) ? '0'.$values['quantity'] : $values['quantity'];
									?>
									<input type="number" name="quantity" data-id="<?php echo $item; ?>" data-variation="<?php echo $values['data']->product_type; ?>" data-variationID="<?php echo $values['variation_id']; ?>" value="<?php echo $quantity; ?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
									<button type="button" class="plus numeral"><span>+</span></button>
									<span class="loadingQtd" data-load="<?php echo $item; ?>" style="display:none;"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/nutracorpore/images/default.svg" style="
									    width: 20px;
									    margin-left: 5px;
									    margin-top: 5px;
									    vertical-align: middle;
									    display: inline-block;
									"></span>
								</div>
							</div>
							<?php
						}
					} else {
						?>
						<h1 class="empty-cart">Sem itens no carrinho</h1>
						<br><br>
						<a href=<?php echo get_bloginfo('url');?> ><div class="safety" id="variacao" style="color: #fff; background: #002c4b; border-color: #ebccd1; width: auto; border-radius: 10px">
							<i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Clique aqui para voltar a loja de produtos
						</div></a>
						<?php
					}
					?>
				</div>
				<div class="purchase-details">
					<div class="shaded-box order">
						<p class="order-details">DETALHES DO PEDIDO</p>
						<?php $amount = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) ); ?>
						<p class="order-price">Total<span><?php echo 'R$&nbsp;'.$sem_desconto = $woocommerce->cart->get_cart_total();?></span></p>
						
						<?php

						if ($items) {

							$n = 1;

							$com_desconto = $discount_value * 1.111111;

							$hey = $com_desconto;

							while($n < 7){

								$parcela = $hey / $n;

								if( $hey <= 40  ){

									$final = '1x </b> de <b>R$ '.number_format($hey,2,',','.');

								}else{

									if( $parcela <= 40 ){
										echo "";
									}else{

										$final = $n.'x </b> de <b> R$ '.str_replace('.',',',number_format($parcela,2));

									}

								}

								$n++;

							}

							?>

							<p class="order-installments">em até <b><?php echo $final; ?></b> S/ juros OU à vista com <b class="order-discount red">10% OFF</b></p>
								<p class="order-discount-price">por <strong>R$ <?php echo number_format($discount_value, 2, ',', '.'); ?></strong></p>

							<!--<p class="order-installments">em até <b><?php echo $payment_options['installments'];?>x</b> de <b>R$ 
								<?php echo number_format($installments_value, 2, ',', '.'); ?></b> S/ juros OU à vista com <b class="order-discount red"><?php echo $payment_discounts['boleto']['amount']; ?> OFF</b></p>
								<p class="order-discount-price">por <strong>R$ <?php echo number_format($discount_value, 2, ',', '.'); ?></strong></p>-->

								<?php
							}
							?>
						</div>
						<div class="input-box freight shaded-box lesser">
							<span>CALCULE O FRETE</span>
							<small class="zip-code-help">
								<a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">
									<span class="questionmark">?</span>
									<span class="text">Não sei<br> meu CEP</span>
								</a>
							</small>
							<form action="" id="cart-shipping-calculate">
								<input type="hidden" name="action" value="shippingProduct">
								<input type="text" class="gray-placeholder" name="zipcode" placeholder="Digite seu CEP">
								<button>OK</button>
							</form>
						</div>
						
						<div id="loading" style="display: none;margin: 0 auto;text-align: center;padding: 10px 0;"><img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/nutracorpore/images/icons/loader.gif"></div>
						<table class="freight-information" id="shipping_totals" style="display: none;">
						</table>

						<div class="input-box coupon shaded-box lesser">
							<span>POSSUI CUPOM DE DESCONTO?</span>
							<form action="" id="coupon-calculate">
								<input type="text" name="coupon-number" class="gray-placeholder" placeholder="Digite seu código">
								<button type="submit">OK</button>
							</form>
						</div>
						<a href="<?php echo get_bloginfo('url'); ?>/login?checkout=checkout" class="finish-purchase shaded-box">FINALIZAR COMPRA</a>
						<a href="<?php echo get_bloginfo('url'); ?>" class="keep-purchasing shaded-box">
							<i class="fa fa-angle-double-left" aria-hidden="true"></i> CONTINUAR COMPRANDO
						</a>
					</div>
				</div>
			</div>
		</section>
</main>
<?php 
get_footer();
?>

<script>

	function removeProduto(){
		var id = $(this).data('id');

		swal({
			title: "",
			text: "Você deseja mesmo excluir esse produto do carrinho?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Sim",
			cancelButtonText: "Não",
			closeOnConfirm: false
		},
		
		function(isConfirm) {
			if (isConfirm) {
				$.ajax({
					url: url,
					data: {id: id, action: 'removeProduct'},
					type: 'POST',
					success: function(data) {
					swal({
						type: 'success',
						title: 'Produto removido com sucesso'
					}, function() {
						location.reload();
							});
						}
					});
				}
			});
		}

</script>