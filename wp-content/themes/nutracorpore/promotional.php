<section class="promotional">
	<div class="center-content">
		<?php
			$args = array(
				'post_type' => 'Banner',
				'order' => 'desc'
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) :
				while ( $loop->have_posts() ) : $loop->the_post();
					if (get_field('tipo_de_banner') == 'skyscrape') : 
		?>
					<img src="<?php //echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="" class="banner">
		<?php
					endif;
				endwhile;

				wp_reset_query();
			endif;
		?>
	</div>
</section>