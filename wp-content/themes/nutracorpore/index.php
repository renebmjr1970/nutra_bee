<?php 
	get_header();
?>

<section class="highlights">
    <div class="full-bg">

        <!--</?php
		$args = array(
			'post_type' => 'Banner',
			'order' => 'asc'
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) :
			while ( $loop->have_posts() ) : $loop->the_post();
		if (get_field('tipo_de_banner') == 'hero') : 
			?>
		<a href="</?php echo ( get_field('link') == '' ) ? '#' : get_field('link'); ?>">
			<div class="slide"  style="background-image: url(</?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>);"></div>
		</a>
		</?php
		endif;
		endwhile;
		endif;
		?>-->

        <div class="slide">
            <iframe src="<?php echo get_bloginfo('template_url').'/src/slider/demos/01-default.php'; ?> " style="borde: 0; margin:0; padding: 0; overflow-y: hidden; width: 100%; height: 80rem;"></iframe>
        </div>

    </div>
    <div class="center-content">
        <div class="forepart">
            <div>
                <span>10% de DESCONTO</span>
                <i class="fa fa-tag" aria-hidden="true"></i>
                <span>Para compras à vista.</span>
            </div>
            <div>
                <span style="text-align: center;">FRETE GRÁTIS</span>
                <i class="fa fa-truck" aria-hidden="true"></i>
                <?php
					$sql_getFreeShipping = '
					SELECT
						option_value
					FROM
						wp_options
					WHERE
						option_name = "woocommerce_free_shipping_5_settings"
					';

					$results = $wpdb->get_results($sql_getFreeShipping);
					//print_r($results[0]->option_value);
					$results_unserialized = maybe_unserialize($results[0]->option_value);
				?>

                    <span class="frete-info">Para compras acima de R$ <?php echo number_format( $results_unserialized['min_amount'], 2, ',', '.' ); ?>*.</span>
            </div>
            <div>
                <span>TEM UMA DÚVIDA NUTRICIONAL?</span>
                <i class="fa fa-comments" aria-hidden="true"></i>
                <span>Fale com nossa nutricionista <a class="red" href="<?php echo get_bloginfo('url'); ?>/fale-conosco">aqui.</a></span>
            </div>
        </div>

        <!-- menu responsivo -->
        <dl style="margin-top: 10%;" class="menu-mobile">
            <dt><h1>OBJETIVOS</h1></dt>
                <dd>
                    <a href="<?php echo get_bloginfo('url').'/categoria/massa-muscular'; ?> "><div class="sub-item"><h1>MASSA MUSCULAR</h1></div></a>
                    <a href="<?php echo get_bloginfo('url').'/categoria/definicao-muscular'; ?>"><div class="sub-item"><h1>DEFINIÇÃO MUSCULAR</h1></div></a>
                    <a href="<?php echo get_bloginfo('url').'/categoria/energia-e-resistencia'; ?>"><div class="sub-item"><h1>ENERGIA E RESISTÊNCIA</h1></div></a>
                    <a href="<?php echo get_bloginfo('url').'/categoria/perder-gordura'; ?>"><div class="sub-item"><h1>PERDER GORDURA</h1></div></a>
                </dd> 
            <dt><h1>PRODUTOS</h1></dt>
                <dd>

                <!-- COLOCAR LOOPING AQUI -->
                <ul>
                    <?php
                        $sql  = 'SELECT wt.* ';
                        $sql .= 'FROM wp_terms AS wt ';
                        $sql .= 'INNER JOIN wp_term_taxonomy AS wtt ON wtt.term_id = wt.term_id ';
                        $sql .= 'WHERE ';
                        $sql .= 'wtt.parent = 0 AND wtt.taxonomy = "product_cat" AND wt.slug <> "meu-objetivo" LIMIT 4';

                        //echo $sql;

                        $results = $wpdb->get_results($sql);
                        $arrParent = '';
                        $firstPass = true;
                        foreach ($results as $r) {
                            $arrParent .= $r->term_id.',';
                        }

                        $arrParent = rtrim($arrParent,",");

                        for ( $i = 0; $i < 49; $i++ ) {
                        
                            if ( $firstPass ) {
                        ?>
                        <li style="display:none;">
                            <h2><a href="<?php echo get_bloginfo('url').'/categoria/'.$results[$i]->slug; ?>"><?php //echo $results[$i]->name; ?></a></h2>
                        </li>
                        <?php
                            $subsql  = 'SELECT wt.* ';
                            $subsql .= 'FROM wp_terms AS wt ';
                            $subsql .= 'INNER JOIN wp_term_taxonomy AS wtt ON wtt.term_id = wt.term_id ';
                            $subsql .= 'WHERE ';
                            $subsql .= 'wtt.parent IN ('.$arrParent.') AND wtt.taxonomy = "product_cat" ORDER BY name ASC';

                            $sub_results = $wpdb->get_results($subsql);
                        
                            foreach ( $sub_results as $sub ) :
                            ?>
                            <li style="text-transform: uppercase; width: 100%; color: #6d6e71;  font-family: 'Asap', sans-serif; font-size: 3em; padding: 20px 20px; -moz-transition: all 0.25s ease-in-out; -o-transition: all 0.25s ease-in-out; -ms-transition: all 0.25s ease-in-out; -webkit-transition: all 0.25s ease-in-out; transition: all 0.25s ease-in-out; overflow: hidden; font-size: 12px">
                                <a href="<?php echo get_bloginfo('url').'/categoria/'.$sub->slug; ?>">
                                    <?php 
                                        if ( strlen($sub->name) <= 27 ) {
                                            echo $sub->name;
                                        }
                                        else {
                                            echo substr($sub->name, 0, 27) . '...';
                                        }
                                    ?>

                                </a>
                            </li>
                            <?php
                                $i++;
                                endforeach;
                                $firstPass = false;
                            } else {
                                echo '<li></li>';
                            }
                        }
                    ?>
                </ul>
                <!-- FIM DO LOOPING -->

                </dd>
            <a href="<?php echo site_url('/marcas/'); ?>"><dt><h1>MARCAS</h1></dt></a>

            <a href="'.get_bloginfo('url').'/categoria/kits-promocionais/"><dt><h1>KITS PROMOCIONAIS</h1></dt></a>
        </dl>
        <script>
            $('dt').addClass('fechado');
            var $active = null;

            $('dt').click(function() {
                if ($active !== null) {
                    $active.next().slideToggle("fast");
                    $active.removeClass('aberto');
                    $active.addClass('fechado');
                }
                $active = $(this);
                $active.addClass('aberto');
                $next = $active.next();

                if ($next.is(":hidden")) {
                    $next.slideToggle("fast");
                } else {
                    $active.removeClass('aberto');
                    $active.addClass('fechado');
                    $active = null;
                }
            })

        </script>
        <!-- menu responsivo -->

        <!-- Menu start -->
        <div class="section-picker menu-desk">
            <div class="option active" data-picker="objectives">
                <div class="heading">
                    <button class="submenu-picker" data-picker="objectives" data-url="/teste">	
						<h1>OBJETIVOS</h1>
					</button>
                </div>
                <ul class="option-submenu objectives">
                    <li>
                        <ul>
                            <li>
                                <h2>
                                    <a href="<?php echo get_bloginfo('url').'/categoria/massa-muscular'; ?>">MASSA MUSCULAR</a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href="<?php echo get_bloginfo('url').'/categoria/definicao-muscular'; ?>">DEFINIÇÃO MUSCULAR</a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href="<?php echo get_bloginfo('url').'/categoria/energia-e-resistencia'; ?> ">ENERGIA E RESISTÊNCIA</a>
                                </h2>
                            </li>
                            <li>
                                <h2>
                                    <a href="<?php echo get_bloginfo('url').'/categoria/perder-gordura'; ?>">PERDER GORDURA</a>
                                </h2>
                            </li>
                        </ul>
                    </li>
                    <li class="overview blue">
                        <a href="<?php echo get_bloginfo('url').'/categoria/massa-muscular/' ?>">
                            <div class="background-form" aria-hidden="true"></div>
                            <div class="highlight muscle"></div>
                        </a>
                    </li>
                    <li class="overview red">
                        <a href="<?php echo get_bloginfo('url').'/categoria/definicao-muscular/' ?>">
                            <div class="background-form" aria-hidden="true"></div>
                            <div class="highlight thinning"></div>
                        </a>
                    </li>
                    <li class="overview yellow">
                        <a href="<?php echo get_bloginfo('url').'/categoria/energia-e-resistencia/' ?>">
                            <div class="background-form" aria-hidden="true"></div>
                            <div class="highlight energy"></div>
                        </a>
                    </li>
                    <li class="overview green">
                        <a href="<?php echo get_bloginfo('url').'/categoria/perder-gordura/' ?>">
                            <div class="background-form" aria-hidden="true"></div>
                            <div class="highlight health"></div>
                            <!--<span class="subtitle">
								<h2>SAÚDE E BEM-ESTAR</h2>
							</span>-->
                        </a>
                    </li>
                </ul>
            </div>
            <div class="option" data-picker="categories">
                <div class="heading">
                    <button class="submenu-picker" data-picker="categories">	
						<h1>PRODUTOS</h1>
					</button>
                </div>
                <ul class="option-submenu categories five">
                    <?php
						$sql  = 'SELECT wt.* ';
						$sql .= 'FROM wp_terms AS wt ';
						$sql .= 'INNER JOIN wp_term_taxonomy AS wtt ON wtt.term_id = wt.term_id ';
						$sql .= 'WHERE ';
						$sql .= 'wtt.parent = 0 AND wtt.taxonomy = "product_cat" AND wt.slug <> "meu-objetivo" LIMIT 4';

						//echo $sql;

						$results = $wpdb->get_results($sql);
						$arrParent = '';
						$firstPass = true;
						foreach ($results as $r) {
							$arrParent .= $r->term_id.',';
						}

						$arrParent = rtrim($arrParent,",");

						for ( $i = 0; $i < 49; $i++ ) {
						
							if ( $firstPass ) {
						?>
                        <li style="display:none;">
                            <h2><a href="<?php echo get_bloginfo('url').'/categoria/'.$results[$i]->slug; ?>"><?php //echo $results[$i]->name; ?></a></h2>
                        </li>
                        <?php
							$subsql  = 'SELECT wt.* ';
							$subsql .= 'FROM wp_terms AS wt ';
							$subsql .= 'INNER JOIN wp_term_taxonomy AS wtt ON wtt.term_id = wt.term_id ';
							$subsql .= 'WHERE ';
							$subsql .= 'wtt.parent IN ('.$arrParent.') AND wtt.taxonomy = "product_cat" ORDER BY name ASC';

							$sub_results = $wpdb->get_results($subsql);
						
							foreach ( $sub_results as $sub ) :
							?>
                            <li style="background: #f4f7f9 ;height: 2.8rem; width: 298px; border-bottom: 0.1rem solid #fff; border-right: 1px solid #eaeaea ;color: #6d6e71; padding: 0.6rem 0; -moz-transition: all 0.25s ease-in-out; -o-transition: all 0.25s ease-in-out; -ms-transition: all 0.25s ease-in-out; -webkit-transition: all 0.25s ease-in-out; transition: all 0.25s ease-in-out; overflow: hidden; font-size: 12px">
                                <a href="<?php echo get_bloginfo('url').'/categoria/'.$sub->slug; ?>">
                                    <?php 
										if ( strlen($sub->name) <= 27 ) {
											echo $sub->name;
										}
										else {
											echo substr($sub->name, 0, 27) . '...';
										}
									?>

                                </a>
                            </li>
                            <?php
								$i++;
								endforeach;
								$firstPass = false;
							} else {
								echo '<li></li>';
							}
						}
					?>
                </ul>
            </div>
            <!-- menu inicio -->
            <div class="option" data-picker="brands">
                <div class="heading">
                    <button class="submenu-picker" data-picker="brands">	
						<h1>MARCAS</h1>
					</button>
                </div>
                <!--<ul class="option-submenu brands five">
                    <?php
						$args = array(
                            'post_type'      => 'marca',
							'posts_per_page' => 12,
							'meta_key'		 => 'vitrine_home',
							'meta_value'	 => 'mostrar',
                            'order'          => 'DESC'
						);
						$loop = new WP_Query( $args );
						$cnt = 0;
						if ( $loop->have_posts() ) :
							while ( $loop->have_posts() ) : $loop->the_post();
								if ($cnt == 0) :
					?>
                        <li>
                            <ul>
                                <?php
								endif;	
								$post_data = get_post(get_the_ID());
								$post_name = $post_data->post_name;
								?>
                                    <li>
                                        <a href="<?php echo get_bloginfo('url') ;?>/categoria/<?php echo $post_name; ?>">
												<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="">
											</a>
                                    </li>
                                    <?php
								$cnt++;
								if ($cnt == 3) :
									$cnt = 0;
					?>
                            </ul>
                        </li>
                        <?php
								endif;
							endwhile;
						endif;
					?>
                        <li class="full"><a href="<?php echo site_url('/marcas/'); ?>">VER MAIS</a></li>
                </ul>-->


                <ul class="option-submenu brands five">
                    <li>
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/nutraforce">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/nutra-corpore_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/new-millen">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/new-millen_clr-150x80.jpg" alt="">
                                </a>
                            </li>                           

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/probiotica">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/probiotica_clr-150x80.jpg" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

                    <li>    
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/integralmedica">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/integralmedica_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/atlhetica-nutrition">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/atlhetica-nutrition_clr.png" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/max-titanium">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/max-titanium_clr.png" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

                    <li>    
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/optimum-nutrition">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/optimum-nutrition_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/arnold-nutrition">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/arnold-nutrition_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/dymatize">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/dymatize_clr-150x80.jpg" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>

                    <li>    
                        <ul>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/gat">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/gat_clr-150x80.jpg" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/muscletech">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/muscletech_clr.png" alt="">
                                </a>
                            </li>

                            <li>
                                <a href="<?php echo get_bloginfo('url') ;?>/categoria/mp-muscle-pharma">
                                    <img src="https://nutracorpore.com.br/wp-content/uploads/2017/02/mp-muscle-pharma_clr-150x80.jpg" alt="">
                                </a>
                            </li>                                

                        </ul>
                    </li>


                    <li class="full"><a href="<?php echo site_url('/marcas/'); ?>">VER MAIS</a></li>
                </ul>



            </div>
            <div class="option" data-picker="sales">
                <div class="heading">
                    <button class="submenu-picker" data-picker="sales">	
						<h1>KITS PROMOCIONAIS</h1>
					</button>
                </div>
                <div class="option-submenu sales">
                    <section class="grid-products greyed five">
                        <?php
							$args = array(
								'post_type'      => 'product',
								'posts_per_page' => 4,
								'product_cat'    => 'kits-promocionais',
								'meta_key'	     => 'kits_home',
								'meta_value'     => 'mostrar'
							);

							$payment_options   = get_option('woocommerce_cielo_credit_settings');
							$payment_discounts = get_option('woocommerce_payment_discounts');
							$percent 			= str_replace('%', '', $payment_discounts['boleto']);

							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) :
								while ( $loop->have_posts() ) : $loop->the_post();
									$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
							?>
                            <div class="product">
                                <figure>
                                    <?php 
									if (get_field('frete_gratis')) { 
										?>
                                    <span class="generic-stripe orange">FRETE GRÁTIS</span>
                                    <?php
									}
									?>
                                    <?php 
									if (get_field('lancamento')) { 
									?>
                                    <span class="generic-stripe green">LANÇAMENTO</span>
                                    <?php
									}
									?>
                                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
                                    <div class="overlay">
                                        <a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
										<?php echo ( $product->product_type != 'variable' && $product->is_in_stock() ) ? '' : 'no-add';?>">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </a>
                                        <?php
										if ( $product->product_type != 'variable' && $product->is_in_stock() ) :
										?>
                                            <button class="add-cart" data-id="<?php echo get_the_ID();?>">
											<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										</button>
                                            <?php
										endif;
										?>
                                    </div>
                                    <figcaption class="name">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <?php echo get_the_title(); ?>
                                        </a>
                                    </figcaption>
                                </figure>
                                <?php
									$full_price = get_field('_regular_price');
									$price 		= get_field('_sale_price');
									
									if ($full_price != $price && !empty($price)) :
								?>
                                    <p class="discounted-difference">De: <span class="striked">R$<?php echo number_format($full_price, 2, '.', ','); ?></span> Por: <b>R$<?php echo number_format($price, 2, '.', ','); ?></b><span>xou em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span></p>
                                    <?php
									else :
								?>

                                        <?php 

                                        $n = 1;

                                        while($n < 7){

                                            $parcela = $product->get_price() / $n;

                                            if( $product->get_price() <= 40  ){

                                                $final = '1x de <span class="installment-price"> R$ '.$product->get_price();

                                            }else{

                                                if( $parcela <= 40 ){
                                                    echo "";
                                                }else{

                                                    $final = $n.'x de <span class="installment-price"> R$ '.str_replace('.',',',number_format($parcela,2));

                                                }

                                            }

                                            $n++;

                                        }

                                        ?>

                                        <p class="full-price">
                                            R$
                                            <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
                                            <span class="installments">em até <?php echo $final; ?></span>
                                        </p>

                                        <!--<p class="full-price">
                                            R$
                                            <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
                                            <span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
                                        </p>-->


                                        <?php
									endif;
									if(is_array($percent)) {
										$percent = $percent['amount'];
									}

									$price = $product->price;
								?>
                                            <p class="discount-price">
                                            <?php
        										$price = $price - ($price * ($percent / 100));
        										$price = number_format($price, 2, '.', ',');
        										$price = explode('.', $price);
									            $hihi = number_format($price[0] - ($price[0] * 0.1),2,',','.');
                                           ?>

                                            <?php echo $hihi; ?> <!--<span class="cents"><?php echo (! isset($price[1])) ? '00' : $price[1]; ?></span>-->
                                            </p>
                                            <small>no boleto ou depósito</small>
                            </div>
                            <?php
							endwhile;
							echo '
								<div class="product" style="position:relative">
									<a href="'.get_bloginfo('url').'/categoria/kits-promocionais/" style="position: absolute;margin: auto;top: 0;bottom: 0;left: 0;right: 0;height: 60px;font-size: 20px;padding: 0 20px;font-weight: bold;line-height: 25px;outline:none !important;">VER TODOS KITS PROMOCIONAIS</a>
								</div>';
							else:
								echo 'nao tem';
							endif;
						?>
                    </section>
                </div>
            </div>
        </div>
        <!-- Menu end -->
    </div>
</section>
<div class="division">
    <div class="center-content">
        <div class="texty">
            <h1>NEWSLETTER</h1>
            <h2>Novidades, Artigos, Dicas e Ofertas Exclusivas, cadastre-se.</h2>
        </div>
        <form method="post" class="validate newsletter" target="_blank" novalidate>
            <input type="email" value="" name="EMAIL" placeholder="Digite seu E-mail" class="required email newsletter-mail news-EMAIL">
            <input type="button" value="ENVIAR" name="subscribe" class="saveNewsletter red-basic button">

            <div class="clear">
                <div class="response response-news" style="display:none"></div>
            </div>
        </form>

    </div>
</div>
<section class="top-sellers">
    <div class="center-content">
        <div class="starred">
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star huge" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
        </div>
        <h1 class="title-rulers">MAIS VENDIDOS</h1>
        <div class="grid-products five">
            <?php
			$args = array(
				'post_type' => 'product',
			    'meta_value' => 'a:1:{i:0;s:3:"sim";}',
			    'posts_per_page' => 10
			);

			$payment_options   = get_option('woocommerce_cielo_credit_settings');
			$payment_discounts = get_option('woocommerce_payment_discounts');
			$percent 			= str_replace('%', '', $payment_discounts['boleto']);

			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) :
				echo '<ul class="maisVendidosList">';
				while ( $loop->have_posts() ) : $loop->the_post();
					$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
			?>
                <li class="product">
                    <figure>
                        <?php

					if (get_field('frete_gratis')) { 
						?>
                            <span class="generic-stripe orange">FRETE GRÁTIS</span>
                            <?php
					}
					?>
                            <?php 
					if (get_field('lancamento')) { 
					?>
                            <span class="generic-stripe green">LANÇAMENTO</span>
                            <?php
					}
					?>
                            <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
                            <div class="overlay">
                                <a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
						<?php echo ( $product->product_type != 'variable' && $product->is_in_stock() ) ? '' : 'no-add';?>">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </a>
                                <?php
						if ( $product->product_type != 'variable' && $product->is_in_stock() ) :
						?>
                                    <button class="add-cart" data-id="<?php echo get_the_ID();?>">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</button>
                                    <?php
						endif;
						?>
                            </div>
                            <figcaption class="name">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <?php echo get_the_title(); ?>
                                </a>
                            </figcaption>
                    </figure>
                    <?php
					$full_price = get_field('_regular_price');
					$price 		= get_field('_sale_price');
					
					if ($full_price != $price && !empty($price)) :
						$n = 1;

                            while($n < 7){

                                $parcela = $price / $n;

                                if( $product->get_price() <= 40  ){

                                    $final = '1x de R$ '.$price;

                                }else{

                                    if( $parcela <= 40 ){
                                        echo "";
                                    }else{

                                        $final = $n.'x de R$ '.str_replace('.',',',number_format($parcela,2));

                                    }

                                }

                                $n++;

                            }
                            //echo "final: $final<br>";
				?>
                        <p class="discounted-difference">De: <span class="striked">R$<?php echo number_format($full_price, 2, '.', ','); ?></span> Por: <b>R$<?php echo number_format($price, 2, '.', ','); ?></b><span>ou em até <?php echo $final;?></span></p>
                        <?php
					else :
				?>


                            <?php 

                            $n = 1;

                            while($n < 7){

                                $parcela = $product->get_price() / $n;

                                if( $product->get_price() <= 40  ){

                                    $final = '1x de <span class="installment-price"> R$ '.$product->get_price();

                                }else{

                                    if( $parcela <= 40 ){
                                        echo "";
                                    }else{

                                        $final = $n.'x de <span class="installment-price"> R$ '.str_replace('.',',',number_format($parcela,2));

                                    }

                                }

                                $n++;

                            }

                            ?>


                            <p class="full-price">
                                R$
                                <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
                                <span class="installments">em até <?php echo $final; ?></span>
                            </p>

                            <!--<p class="full-price">
                                R$
                                <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
                                <span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
                            </p>-->                            

                            <?php
					endif;

				if(is_array($percent)) {
					$percent = $percent['amount'];
				}
				$price = $product->price;

				?>
                                <p class="discount-price">
                                    <?php
					$price = $price - ($price * ($percent / 100));
					$price = number_format($price, 2, '.', ',');
					//$price = explode('.', $price);
                    $hihi  = number_format($price , 2, ',','.');
                    $hihi_final = str_replace(".",",",$hihi)
					?>
                                        <?php echo $hihi; ?><!--<span class="cents"><?php echo (! isset($price[1])) ? '00' : $price[1]; ?></span> -->
                                </p>
                                <small>no boleto ou depósito</small>
                </li>
                <?php
			endwhile;
			echo '</ul>';
			endif;
			?>
        </div>
    </div>
</section>
<div class="division">
    <div class="center-content">
        <h1>NUTRACORPORE TV</h1>
        <a href="https://www.youtube.com/user/nutracorpore" target="_blank">
            <div class="youtube"></div>
        </a>
        <h2>Inscreva-se e acesse nosso conteúdo exclusivo em HD.</h2>
    </div>
</div>
<section class="offers">
    <div class="center-content">
        <h1 class="title-rulers">OFERTAS</h1>
        <div class="ofertas-products grid-products greyed five">
            <?php 
			$args = array(
		        'posts_per_page' => 10,
		        'post_type'      => 'product',
				'meta_key'		 => 'oferta',
				'meta_value'	 => 'Sim'
			);

			$loop = new WP_Query( $args );

			if ( $loop->have_posts() ) :
				while ( $loop->have_posts() ) : $loop->the_post();
					$productType = $product->product_type;

					$product_variation = new WC_Product_Variable( get_the_ID() );
					$variations = $product_variation->get_available_variations();


					$product_values = $woocommerce->product_factory->get_product( false, array( 'ID' => get_the_ID() ) );
					
					if ($product_values->product_type == 'variable') {
						$price = $variations[0]['display_price'];
						$full_price = $variations[0]['display_regular_price'];
					} else {
						$full_price = get_field('_regular_price');
						$price 		= get_field('_sale_price');
					}


					
					$i++;
					?>

            <div class="product">
                <figure>
                    <?php 
							if (get_field('frete_gratis')) { 
								?>
                    <span class="generic-stripe orange">FRETE GRÁTIS</span>
                    <?php
							}
							?>
                    <?php 
							if (get_field('lancamento')) { 
							?>
                    <span class="generic-stripe green">LANÇAMENTO</span>
                    <?php
							}
							?>
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
                    <div class="overlay">
                        <a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
								<?php echo ( $product->product_type != 'variable' && $product->is_in_stock() ) ? '' : 'no-add';?>">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </a>
                        <?php
								if ( $product->product_type != 'variable' && $product->is_in_stock() ) :
								?>
                            <button class="add-cart" data-id="<?php echo get_the_ID();?>">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
									</button>
                            <?php
								endif;
								?>
                    </div>
                    <div class="promotional-information">
                        <?php
								if ($price > 0 && $price != $full_price) {
									 
									$percentChange = number_format((1 - $price / $full_price) * 100);
									?>
                            <span class="discount-stripe"><?php echo number_format($percentChange, 0); ?> <small>OFF</small></span>
                            <?php
								}
								?>
                    </div>
                    <figcaption class="name">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <?php echo get_the_title(); ?>
                        </a>
                    </figcaption>
                </figure>
                <?php
							$full_price = get_field('_regular_price');
							$price 		= get_field('_sale_price');
							
							if ($full_price != $price && !empty($price)) :
								$n = 1;

	                            while($n < 7){

	                                $parcela = $price / $n;

	                                if( $product->get_price() <= 40  ){

	                                    $final = '1x de R$ '.$price;

	                                }else{

	                                    if( $parcela <= 40 ){
	                                        echo "";
	                                    }else{

	                                        $final = $n.'x de R$ '.str_replace('.',',',number_format($parcela,2));

	                                    }

	                                }

	                                $n++;

	                            }
	                            //echo "final: $final<br>";								
						?>
                    <p class="discounted-difference">De: <span class="striked">R$<?php echo number_format($full_price, 2, '.', ','); ?></span> Por: <b>R$<?php echo number_format($price, 2, '.', ','); ?></b><span>ou em até <?php echo $final; ?></span></p>
                    <?php
							else :
						?>

                            <?php 

                            $n = 1;

                            while($n < 7){

                                $parcela = $product->get_price() / $n;

                                if( $product->get_price() <= 40  ){

                                    $final = '1x de <span class="installment-price"> R$ '.$product->get_price();

                                }else{

                                    if( $parcela <= 40 ){
                                        echo "";
                                    }else{

                                        $final = $n.'x de <span class="installment-price"> R$ '.str_replace('.',',',number_format($parcela,2));

                                    }

                                }

                                $n++;

                            }

                            ?>

                        <p class="full-price">
                            R$
                            <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
                            <span class="installments">em até <?php echo $final; ?></span>
                        </p>

                        <!--<p class="full-price">
                            R$
                            <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
                            <span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
                        </p>-->

                        <?php
							endif;

						if(is_array($percent)) {
							$percent = $percent['amount'];
						}
						$price = $product->price;

						?>
                            <p class="discount-price">
                                <?php
							$price = $price - ($price * ($percent / 100));
							$price = number_format($price, 2, '.', ',');
							//$price = explode('.', $price);
                            $hihi  = number_format($price , 2, ',','.');
							?>
                                        <?php echo $hihi; ?><!--<span class="cents"><?php echo (! isset($price[1])) ? '00' : $price[1]; ?></span> -->
                                </p>
                                <small>no boleto ou depósito</small>
            </div>
            <?php
				endwhile;
			endif;
			?>
        </div>
        <!--ENDGRID -->
    </div>
    <!--ENDCENTER -->
</section>
<section class="blog">
    <div class="center-content">
        <h1 class="title-rulers hidden">
            <span class="mobile-only">CONTEÚDO EXCLUSIVO</span>
            <img src="<?php echo get_bloginfo('template_url');?>/images/common/exclusive.jpg" alt="Conteúdo Exclusivo: Vídeos, Ensaios, Receitas, Artigos sobre suplementação, Musculação, saúde e muito mais" class="exclusive">
        </h1>

        <div class="articles-topic articles-topic-home">
            <?php 
			wp_nav_menu( array(
				'menu' => 'menu_categoria',
				'theme_location' => 'menu_categoria',
				'menu_class' => 'topics',
				'echo' => true,
				'depth' => 0,
				) );
			?>

        </div>

        <?php
			$sqlMaterias  = "
			(
			 SELECT
			  `terms`.`term_id` AS `term_id`,
			  `termrel`.`object_id` AS `object_id`,
			  'materias' AS `tipos`
			 FROM
			  (
			   `wp_terms` `terms`
			   JOIN `wp_term_relationships` `termrel` ON (
			    (
			     `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			    )
			   )
			  )
			 WHERE
			  (`terms`.`slug` = 'materias')
			 LIMIT 0,
			 3
			)
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'videos' AS `videos`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'videos')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'ensaios' AS `ensaios`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'ensaios')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'receitas' AS `receitas`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'receitas')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'eventos' AS `eventos`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'eventos')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'atletas' AS `atletas`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'atletas')
			  LIMIT 0,
			  3
			 )
			";
			
			$results = $wpdb->get_results($sqlMaterias);
			$materiaAtual = '';
			foreach ($results as $r) :
				$id = $r->object_id;
				if ( $materiaAtual != $r->tipos ) $i = 0;
				$title  = get_the_title($id);

				switch ($r->tipos) {
					case 'materias':

						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'materias';

						break;

					case 'videos':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'videos';
						break;

					case 'ensaios':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';
						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'ensaios';
						break;

					case 'receitas':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'receitas';
						break;

					case 'eventos':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'eventos';
						break;

					case 'atletas':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}
						
						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'atletas';
						break;

					default:
						# code...
						break;
				}
			endforeach; ?>
</section>
<?php 
get_footer();
?>
