

<?php do_action( 'woocommerce_before_cart_totals' ); ?>

<style>
	span.woocommerce-Price-currencySymbol{
		display: none !important;
	}
</style>
<article class="order-resume">
	<h2>RESUMO DO PEDIDO</h2>
	<?php
	global $woocommerce;
	$items = $woocommerce->cart->get_cart();
	$woocommerce->cart->calculate_totals();
	$woocommerce->cart->shipping_total = 0;



	//echo "<pre>";
	//print_r($woocommerce->cart->total);
	//echo "</pre>";

	if($items){
		foreach($items as $item => $values) { 
			$_product = $values['data']->post;
			$price = get_post_meta($values['product_id'] , '_price', true);
			?>
			<div class="row">
				<span class="checkout-name"><?php echo $values['data']->post->post_title; ?></span>
				<span class="checkout-price"><b>R$ <?php echo number_format($values['line_subtotal'], 2, ',', '.'); ?></b></span>
			</div>
			<?php
		}

		$feesAvaible = $woocommerce->cart->fees;
		
		//echo '<pre>';
		//	print_r($feesAvaible);
		//echo '</pre>';
		$feeDiscount = 0;
		foreach ($feesAvaible as $value) {
			if ( strpos( $value->id, 'boleto' ) !== false) {
			    $feeDiscount = $value->amount * (-1);
			}
		}
		if ( isset( $_COOKIE['shipping_value'] ) ) {
			$totalgeral = ($woocommerce->cart->cart_contents_total)+($_COOKIE['shipping_value'])-($feeDiscount);
		} else {
			$totalgeral = ($woocommerce->cart->cart_contents_total)-$feeDiscount;
		}
		$woocommerce->cart->total = $totalgeral;

		$_SESSION['totalgeral'] = $totalgeral;


		//echo('sessao totalgeral: '.$_SESSION['totalgeral']);

		//echo $woocommerce->cart->cart_contents_total."<hr>";
		//echo $_COOKIE['shipping_value']."<hr>";
		//echo $feeDiscount."<hr>";
		?> 
		
		<div class="order-summary">
			<a href="<?php echo get_bloginfo('url'); ?>/carrinho">Voltar para o carrinho</a>
			
			<?php if ( count($woocommerce->cart->applied_coupons) > 0 ) { ?>	
			
			<p>
				<span class="value-info">Cupom:</span>
				<span class="value-result red">
					<b>
						<?php $cupomName = $woocommerce->cart->applied_coupons[0];  ?>
						<?php echo 'R$ (-'.number_format($woocommerce->cart->coupon_discount_amounts[$cupomName], 2, ',', '.').')'; ?>

					</b>
				</span>
			</p>

			<?php } ?>

			<p>
				<span class="value-info">Subtotal:</span>
				<span class="value-result blue">
					<b>
						<?php echo 'R$ '.number_format($woocommerce->cart->cart_contents_total, 2, ',', '.'); ?>
					</b>
				</span>
			</p>
			<?php if ( isset($showfrete) ) { ?>
				<?php if (in_array('true', $showfrete)) { ?>
				<p>
					<span class="value-info">Frete:</span>
					<span class="value-result blue">
						<b>
							<?php echo 'R$ '.number_format($_COOKIE['shipping_value'], 2, ',', '.'); ?>
						</b>
					</span>
				</p>
				<?php } ?>
			<?php } ?>

			<?php if ( $feeDiscount != 0 ) { ?>
				<p>
					<span class="value-info">Desconto:</span>
					<span class="value-result blue"><b>R$ <?php echo number_format($feeDiscount, 2, ',', '.'); ?></b></span>
				</p>
			<?php } ?>
			<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>
	
			<?php if ( isset($showfrete) ) { ?>
				<?php if (in_array('true', $showfrete)) { ?>
				<p>
					<span class="value-info">Total:</span>
					<span class="value-result"><b><?php echo 'R$ '.number_format($woocommerce->cart->total, 2, ',', '.');; ?></b></span>
				</p>
				<?php } ?>
			<?php } ?>
		
		</div>

		<?php 

	} else {
		?>
		<h1 class="empty-cart small" style="width: 100%">Sem Produtos no carrinho</h1>
		<?php
	}
	?>

</article>


<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>
<?php do_action( 'woocommerce_after_cart_totals' ); ?>
