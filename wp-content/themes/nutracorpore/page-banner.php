		<?php

		$args = array(
			'post_type' => 'Banner',
			'order' => 'desc'
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) :
			while ( $loop->have_posts() ) : $loop->the_post();
		if (get_field('tipo_de_banner') == 'hero') : 
			?>
		<a href="<?php echo ( get_field('link') == '' ) ? '#' : get_field('link'); ?>">
			<div class="slide" 
			style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>);"></div>
		</a>
		<?php
		endif;
		endwhile;
		endif;
		
		?>