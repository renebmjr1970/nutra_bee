<?php 
	get_header();
?>
<main>
	<section class="identification recover">
		<div class="center-content">	
			<h1 class="lined">ESQUECI MINHA SENHA</h1>
			<article class="fieldbox shaded-box">
				<h2 class="has-icon lock">RECUPERAR SENHA</h2>
				<form method="post" action="#">	
					<fieldset>
						<legend class="field-descriptor">Para recuperar sua senha de acesso informe seu e-mail.</legend>
								<?php include 'recuperar-senha.php'; ?>
								<?php $recuperar->enviar(); ?>
								<label>
									<span class="field-descriptor">
										Digite seu e-mail
									</span>

									<input type="email" name="email" required class="field light-gray-placeholder" placeholder="seu@email.com">

									<input type="password" name="senha" required class="field light-gray-placeholder" placeholder="Nova Senha">

									<input type="password" name="senha1" required class="field light-gray-placeholder" placeholder="Confirmar a senha">

								</label>
								<input type="submit" class="generic-blue" style="margin-top: 0.3rem;" name="continuar">

					</fieldset>
				</form>
			</article>
		</div>
	</section>
</main>
<?php 
	get_footer();
?>