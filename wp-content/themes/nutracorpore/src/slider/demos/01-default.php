<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Swiper demo</title>
    <!-- Link Swiper's CSS -->
    <link rel="stylesheet" href="../dist/css/swiper.min.css">

    <!-- Demo styles -->
    <style>
        body {
            background: #eee;
            font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
            font-size: 14px;
            color: #000;
            margin: 0;
            padding: 0;
        }
        
        .swiper-container {
            width: 100%;
            height: 784px;
        }
        
        .swiper-slide {
            text-align: center;
            font-size: 18px;
            background: #fff;
            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }
        
        .banner1 {
            background-image: url(https://www.nutracorpore.com.br/wp-content/uploads/2017/08/Banner-Site.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .banner2 {
            background-image: url(https://www.nutracorpore.com.br/wp-content/uploads/2017/06/Banner-L-Carnitine-120-capsulas-Probiótica.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .banner3 {
            background-image: url(https://www.nutracorpore.com.br/nutracorpore/wp-content/uploads/2017/05/Banner-Compre-e-Ganho-Fone-de-Ouvivido.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .banner4 {
            background-image: url(https://www.nutracorpore.com.br/wp-content/uploads/2017/06/Nova-Linha-NutraForce-nolink.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .banner5 {
            background-image: url(https://www.nutracorpore.com.br/nutracorpore/wp-content/uploads/2017/05/Banner-Combo-Max-Ganho-de-Peso.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .banner6 {
            background-image: url(https://www.nutracorpore.com.br/nutracorpore/wp-content/uploads/2017/05/Banner-Whey-Protein-Force.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }
        
        .banner7 {
            background-image: url(https://www.nutracorpore.com.br/wp-content/uploads/2016/12/5f95d797-daac-4b04-a84b-7c0027f7cc5b.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .banner8 {
            background-image: url(https://www.nutracorpore.com.br/wp-content/uploads/2017/06/Nova-Linha-NutraForce-nolink.jpg);
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }        

    </style>
</head>

<body>
    <!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide banner1" style="cursor: pointer;"></div>
            <div class="swiper-slide banner2" style="cursor: pointer;"></div>
            <div class="swiper-slide banner3" style="cursor: pointer;"></div>
            <div class="swiper-slide banner4" style="cursor: pointer;"></div>
            <div class="swiper-slide banner5" style="cursor: pointer;"></div>
            <div class="swiper-slide banner6" style="cursor: pointer;"></div>
            <div class="swiper-slide banner7" style="cursor: pointer;"></div>
            <div class="swiper-slide banner8" style="cursor: pointer;"></div>
        </div>
         <div class="swiper-pagination" style="position; absolute; top:55%;"></div>
    </div>

    <!-- Swiper JS -->
    <script src="../dist/js/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            paginationClickable: true
        });
    </script>
</body>

</html>
