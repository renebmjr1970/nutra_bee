<?php 
	if (! is_user_logged_in()) {
		header('location: '.get_bloginfo('url').'/login');
	}

	get_header();
?>
<div class="center-content">
	<?php 
		include 'promotional.php'; 
	?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>

		<main class="account shaded-box">
			<h2 class="full-lined red">ALTERAR SENHA</h2>
			<form id="form-edit-account" class="edit-password" action="<?php echo get_bloginfo('template_url'); ?>/editaconta.php" method="post" id="formformulario">
				<input type="hidden" class="field" name="editpassword">
				<fieldset>
					<label class="encapsule">
						<span>Digite sua senha Atual:</span>
						<input type="text" class="field" name="oldpw" value="">
					</label>
					<label class="encapsule">
						<span>Digite sua nova Senha:</span>
						<input type="password" class="field" name="newpw" value="">
					</label>
					<label class="encapsule">
						<span>Repita sua nova Senha:</span>
						<input type="password" class="field" name="confirmnewpw" value="">
					</label>
				</fieldset>
				<button class="generic-blue" type="button" id="confirmar">CONFIRMAR</button>
			</form>
		</main>
	</div>
</div>
<?php 
get_footer();
?>
<script type="text/javascript">
	var clique = 1;
	$("#confirmar").click(function(){

		var form = $("#form-edit-account");
		var serializedForm = form.serialize();

		if ( form.validate() ) {
			var url_site = 'http://'+window.location.hostname+'/nutracorpore/';

			$.ajax({
				url: url_site+'/wp-content/themes/nutracorpore/editaconta.php',
				data: serializedForm,
				type: 'POST',
				success: function(data) {
					var dataJSON = JSON.parse(data);
					var status = dataJSON.status;
					var statusMessage = dataJSON.message;

					swal({
						type: status,
						title: statusMessage
					});
				}
			});
		}
	});
</script>