<?php 
set_time_limit(0);

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $wpdb, $post, $woocommerce;

$current_user = wp_get_current_user();

/*echo "<pre>";
print_r($_GET);
echo "</pre><hr>";

echo "<pre>";
print_r($current_user);
echo "</pre><hr>";
*/
get_header();

/**
 * Get all approved WooCommerce order notes.
 *
 * @param  int|string $order_id The order ID.
 * @return array      $notes    The order notes, or an empty array if none.
 */
function km_get_order_notes( $order_id ) {
 remove_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );
 $comments = get_comments( array(
  'post_id' => $order_id,
  'orderby' => 'comment_ID',
  'order'   => 'DESC',
  'approve' => 'approve',
  'type'    => 'order_note',
 ) );
 $notes = wp_list_pluck( $comments, 'comment_content' );
 add_filter( 'comments_clauses', array( 'WC_Comments', 'exclude_order_comments' ) );
 return $notes;
}

$id = $_GET['id'];
$order 				= wc_get_order( $id );	
$order_meta 		= get_post_meta($id);
$items 				= $order->get_items();
$codRastreio = get_post_meta($_GET['id'], '_correios_tracking_code', true);
$textRastreio = km_get_order_notes( $id )[0];
?>
<div class="woocommerce-MyAccount-content">

<div class="center-content">
	<?php 
		include 'promotional.php'; 
		global $woocommerce, $current_user;
	?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">	
			<h2 class="full-lined red">DETALHES DO PEDIDO</h2>
			<table class="full-order-listing-blue">
				<tbody>
					<tr>
						<th style="text-align:left;">Descrição</th>
						<th>Quantidade</th>
						<th>Preço Unitário</th>
						<th style="text-align:right;">Valor Total</th>
					</tr>
					<?php
						$subtotal = 0;
						$totalPrice = 0;
						$discountCupom = get_post_meta($_GET['id'], '_cart_discount', true);
						if($items){
							foreach ($items as $linhas) {
								//$subtotal .= ($subtotal+$row['subtotalproduto']);
								?>
								<tr class="order_item" style="text-align:left;">
									<td style="text-align:left;">
										<?php echo $linhas['name'];?> 
									</td>
									<td><?php echo sprintf("%02d", $linhas['qty']);?></td>
									<td class="product-total">
										<?php echo 'R$ '.number_format($linhas['line_subtotal'],2,',','.');?> 
									</td>
									<td style="text-align:right;">
										<?php 
											echo 'R$ '.number_format($linhas['line_total'],2,',','.');
											$totalPrice += $linhas['line_subtotal'];
										?> 
									</td>
								</tr>
								<?php
							}
						}
					?>
				</tbody>
			</table>
			<style>
				.detailed-info{
					width: 100%;
					text-align: right;
					margin-top: 10px;
					font-size: 1.2rem;
				}
				.detailed-info tr td strong {
				    margin-left: 10px;
				}
				.detailed-info tr td {
				    margin-top: 5px;
				    display: block;
				}

				.rastreio-row {
					background-color: #eee;
					padding: 10px;
					text-align: center;
					margin-top: 20px !important;
					font-size: 14px;
				}

				.rastreio-row a {
					font-weight: bold;
					color: #136aaa;
				}
				.rastreio-row a:hover{
					text-decoration: underline;
				}
			</style>
			<table class="detailed-info">
				<tr>
					<td>Sub-Total <strong>R$ <?php echo number_format($linhas['line_subtotal'],2,',','.'); ?></strong></td>
				</tr>
				<tr>
					<td>Frete <strong>R$ <?php echo(number_format($order_meta['_order_shipping'][0],2,',','.')); ?></strong></td>
				</tr>
				<?php if ( isset( $order_meta['_order_desconto'][0] ) ) { ?>
					<tr>
						<td>Desconto <strong class="red">( R$ -<?php echo number_format($order_meta['_order_desconto'][0],2,',','.'); ?> )</strong></td>
					</tr>
				<?php }	?>
				<?php if ( $discountCupom != 0 ) { ?>
					<tr>
						<td>Desconto Cupom<strong class="red">( R$ -<?php echo number_format($discountCupom,2,',','.'); ?> )</strong></td>
					</tr>
				<?php }	?>
				<?php if ( isset( $order_meta['_order_desconto'][0] ) ) { ?>
					<tr>
						<td>Total <strong>R$ <?php echo number_format( ($totalPrice+$order_meta['_order_shipping'][0]) - $order_meta['_order_desconto'][0] - $discountCupom,2,',','.') ?></strong></td>
					</tr>
				<?php }	else { ?>
					<tr>
						<td>Total <strong>R$ <?php echo number_format( ($totalPrice+$order_meta['_order_shipping'][0]),2,',','.') ?></strong></td>
					</tr>
				<?php } ?>
				<?php if ( $codRastreio != '' ) { ?>
					<tr>
						<td class="rastreio-row"><strong><?php echo $textRastreio; ?></strong></td>
					</tr>
				<?php } ?>
			</table>
			<div style="text-align: right;margin-top: 20px;">
				<a class="generic-blue" href="<?php echo get_bloginfo('url'); ?>/meus-pedidos/" style="padding: 10px 20px;text-transform: uppercase;">Voltar</a>
			</div>
		</main>
	</div>
</div>
<?php 
	get_footer(); 
?>