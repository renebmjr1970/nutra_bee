<?php
// carregamos o core do wordpress
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

global $current_user,$woocommerce,$wpdb;


$userID = $current_user->ID;
$user_info = get_userdata($userID);
$return = array();
$newsletter = $_POST['newsletter'];

/* NEWSLETTER */
if($newsletter == "on"){
	$newsemail = $_POST['email'];

	$sql 		= "SELECT  COUNT(cemail) AS total FROM tblcadastro_news WHERE cemail = '$newsemail'";
	$retorno 	= $wpdb->get_results($sql);

	foreach ($retorno as $resultado) {
		$total = $resultado->total;
	}

	if($total == 0){
		$newsemailAUX = explode('@',$newsemail);
		$sql = "INSERT INTO tblcadastro_news (cnome,cemail,cdata_cadastro) VALUES ('$newsemailAUX[0]','$newsemail',NOW())";
	}

	if($total == 1){
		$sql = "UPDATE tblcadastro_news SET cemail = '$newsemail' WHERE cemail = '$newsemail'";
	}
	$wpdb->query($sql);	
}

/* ALTERAR SENHA */
if ( isset( $_POST['editpassword'] ) ) {

	$x = wp_check_password( $_POST['oldpw'], $current_user->user_pass, $userID );

	if($x) {
	    if( !empty($_POST['newpw']) && !empty($_POST['confirmnewpw'])) {
	        if($_POST['newpw'] == $_POST['confirmnewpw']) {
	            $udata['ID'] = $userID;
	            $udata['user_pass'] = $_POST['newpw'];
	            $uid = wp_update_user( $udata );
	            if($uid) {
	            	$return['status'] = 'success';
	            	$return['message'] = 'A senha foi atualizada com sucesso'; 
	            } else {
	            	$return['status'] = 'error';
	            	$return['message'] = 'Falha em atualizar sua senha, tente novamente mais tarde.';
	            }
	        }
	        else {
            	$return['status'] = 'error';
            	$return['message'] = 'Sua senha de confirmação está incorreta';
	        }
	    }
	    else {
        	$return['status'] = 'error';
        	$return['message'] = 'Digite uma nova Senha e a confirmação da mesma';
	    }
	} 
	else {
    	$return['status'] = 'error';
    	$return['message'] = 'Senha atual incorreta';
	}

	$return_JSON = json_encode($return);
	echo $return_JSON;

	unset($_POST);

/* SE NAO, ALTERAÇÃO NORMAL DE CONTA */
} else {

	$sql = "SELECT
		*
	FROM
		`wp_usermeta`

	WHERE
		`user_id` = '".$userID."'
	AND
		meta_key LIKE '%shipping_%'";
	$results = $wpdb->get_results($sql);


	try {
		/* EDIT FROM: Minha Conta */
		if ( isset($_POST['name']) ) {
			$nameSeparated = explode(' ', $_POST['name']);
			$i = 0;
			$firstname = '';
			$lastname = '';

			foreach ($nameSeparated as $value) {
				
				if ( $i != 0 ) {
					$lastname .= $value.' ';
				} else {
					$firstname = $value;
				}

				$i++;
			}

			update_user_meta( $userID, 'billing_first_name', $firstname );
			update_user_meta( $userID, 'billing_last_name', $lastname );
			update_user_meta( $userID, 'first_name', $firstname );
			update_user_meta( $userID, 'last_name', $lastname );
		}

		if ( isset( $_POST['type'] ) ) update_user_meta( $userID, 'tipo_pessoa', $_POST['type']);
		if ( isset( $_POST['documento'] ) ) update_user_meta( $userID, 'documento', $_POST['documento']);
		if ( isset( $_POST['birthday'] ) ) update_user_meta( $userID, 'nascimento', $_POST['birthday']);
		if ( isset( $_POST['sex'] ) ) update_user_meta( $userID, 'sexo', $_POST['sex']);
		if ( isset( $_POST['phone'] ) ) update_user_meta( $userID, 'billing_phone', $_POST['phone']);
		if ( isset( $_POST['cellphone'] ) ) update_user_meta( $userID, 'celular', $_POST['cellphone']);
		if ( isset( $_POST['email'] ) ) update_user_meta( $userID, 'billing_email', $_POST['email']);


		$return['status'] = 'success';
		$return['message'] = 'Dados alterados com sucesso';
	} catch (Exception $e) {
		$return['status'] = 'error';
		$return['message'] = 'Ocorreu um erro, tente novamente';
	}

	$return_JSON = json_encode($return);
	echo $return_JSON;

	unset($_POST);
}

