<?php 
	if (! is_user_logged_in()) {
		header('location: '.get_bloginfo('url').'/login');
	}
	get_header();

	global $woocommerce;

?>
<div class="center-content">
	<?php 
		include 'promotional.php'; 
		global $woocommerce, $current_user;
	?>
	<h1 class="lined">MINHA CONTA</h1>
	<div class="padded">
		<?php include_once('account-sidebar.php'); ?>
		<main class="account shaded-box">	
			<section class="account-information">
				<h2 class="full-lined red">MEUS DADOS</h2>
				<form id="form-edit-account" class="edit-account" action="<?php echo get_bloginfo('template_url'); ?>/editaconta.php" method="post" id="formformulario">
					<fieldset>
						<legend class="sr-only">'Edite aqui as informações da sua conta</legend>
						<label class="encapsule">
							<span>Tipo:</span>
							<select class="field" name="type" disabled>
								<?php 
									$tipoPessoa = get_user_meta($current_user->ID, 'tipo_pessoa', true);

									$fisicaSelected = '';
									$juridicaSelected = '';

									if ( $tipoPessoa != '' ) {
										if ( $tipoPessoa == 'fisica' ) {
											$fisicaSelected = 'selected';
										} else {
											$juridicaSelected = 'selected';
										}
									} else {
										$fisicaSelected = 'selected';
									}
								?>
								<option value="fisica" <?php echo $fisicaSelected ?> >Pessoa Física</option>
								<option value="juridica" <?php echo $juridicaSelected ?> >Pessoa Jurídica</option>
							</select>
						</label>
						<label class="encapsule">
							<span>Nome Completo:</span>
							<input type="text" class="field" name="name" disabled value="<?php echo $current_user->user_firstname.' '.$current_user->user_lastname; ?>">
						</label>
						<!-- <label>
							<span>Apelido:</span>
							<input type="text" class="field" name="nickname" disabled>
						</label> -->
						<label class="encapsule">
							<?php 
								if ( $fisicaSelected == 'selected' ) { 
									echo '<span class="tipoChangeDog">CPF:</span>'; 
								} else {
									echo '<span class="tipoChangeDog">CNPJ:</span>';
								}
							?>
							<input type="text" class="field" name="documento" disabled value="<?php echo get_user_meta($current_user->ID, 'documento', true); ?>">
						</label>
						<label class="encapsule">
							<span>E-mail:</span>
							<input type="email" class="field" name="email" disabled value="<?php echo $current_user->user_email; ?>">
						</label>
						<label class="encapsule">
							<span>Data de nascimento:</span>
							<input type="text" class="field" name="birthday" placeholder="99/99/9999" disabled value="<?php echo get_user_meta($current_user->ID, 'nascimento', true); ?>">
						</label>
						<label class="encapsule">
							<span>Sexo:</span>
							<select class="field" name="sex" disabled>
								<option value="default" selected>Selecione...</option>
								<option value="Feminino" <?php echo (get_user_meta($current_user->ID, 'sexo', true) == 'Feminino') ? 'selected' : '';?>>Feminino</option>
								<option value="Masculino" <?php echo (get_user_meta($current_user->ID, 'sexo', true) == 'Masculino') ? 'selected' : '';?>>Masculino</option>
							</select>
						</label>
						<label class="encapsule">
							<span>Telefone de Contato:</span>
							<input type="tel" class="field" name="phone" disabled value="<?php echo get_user_meta($current_user->ID, 'billing_phone', true); ?>">
						</label>
						<label class="encapsule">
							<span>Celular:</span>
							<input type="text" class="field" name="cellphone" disabled 
							value="<?php echo get_user_meta($current_user->ID, 'celular', true); ?>">
						</label>
						<label>
							<span>Newsletter:</span>
							<input type="checkbox" name="newsletter" disabled checked>
						</label>
					</fieldset>
					<a class="generic-blue" href="<?php echo site_url('/esqueci-minha-senha/') ?>">ALTERAR SENHA</a>
					<button class="generic-blue" id="edit" type="button" data-action="edit">EDITAR</button>
				</form>

			</section>
		</main>
	</div>
</div>
<?php 
	get_footer(); 
?>
<script type="text/javascript">
	var clique = 1;
	$("#edit").click(function(){
		var botao = $("#edit").html();
		
		if(clique >1 && botao == "ENVIAR"){
			var form = $("#form-edit-account");
			var serializedForm = form.serialize();

			//console.log(serializedForm);

			if ( form.validate() ) {
				var url_site = 'https://'+window.location.hostname+'/';

				$.ajax({
					url: url_site+'wp-content/themes/nutracorpore/editaconta.php',
					data: serializedForm,
					type: 'POST',
					success: function(data) {
						var dataJSON = JSON.parse(data);
						var status = dataJSON.status;
						var statusMessage = dataJSON.message;

						//console.log(dataJSON);

						swal({
							type: status,
							title: statusMessage
						}, function(){
							location.reload();
						});
					}
				});
			}
		}
    	clique +=clique;
	});
</script>