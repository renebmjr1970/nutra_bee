<?php
// carregamos o core do wordpress
$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$cepbuscado = $_GET['cep'];

/* 
 *  Função de busca de Endereço pelo CEP 
 *  -   Desenvolvido Felipe Olivaes para ajaxbox.com.br 
 *  -   Utilizando WebService de CEP da republicavirtual.com.br 
 */  
function busca_cep($cep){  
    $resultado = @file_get_contents('http://republicavirtual.com.br/web_cep.php?cep='.urlencode($cep).'&formato=query_string');  
    if(!$resultado){  
        $resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";  
    }  
    parse_str($resultado, $retorno); 
    return $retorno;  
}  
  
  
/* 
 * Exemplo de utilização  
 */   
  
//Vamos buscar o CEP 90020022  
$resultado_busca = busca_cep($cepbuscado);  
  
//print_r($resultado_busca);

$endereco 	= utf8_encode($resultado_busca['tipo_logradouro']." ".$resultado_busca['logradouro']);
$cidade 	= utf8_encode($resultado_busca['cidade']);
$estado 	= $resultado_busca['uf'];
$bairro 	= utf8_encode($resultado_busca['bairro']);

echo $endereco.",".$cidade.",".$estado.",".$bairro;
?>