<?php get_header(); ?>
<main>
	<?php 
		include('promotional.php');
	?>
	<section class="post-category-list">
		<div class="center-content cleared">
			<h1 class="title-rulers hidden">
			<span class="mobile-only">CONTEÚDO EXCLUSIVO</span>	
			<img src="<?php echo get_bloginfo('template_url');?>/images/common/exclusive2.jpg" alt="Conteúdo Exclusivo: Vídeos, Ensaios, Receitas, Artigos sobre suplementação, Musculação, saúde e muito mais" class="exclusive">
		</h1>

		<div class="articles-topic articles-topic-home">
			<?php 
			wp_nav_menu( array(
				'menu' => 'menu_categoria',
				'theme_location' => 'menu_categoria',
				'menu_class' => 'topics',
				'echo' => true,
				'depth' => 0,
				) );
			?>

		</div>

		<?php
			$sqlMaterias  = "
			(
			 SELECT
			  `terms`.`term_id` AS `term_id`,
			  `termrel`.`object_id` AS `object_id`,
			  'materias' AS `tipos`
			 FROM
			  (
			   `wp_terms` `terms`
			   JOIN `wp_term_relationships` `termrel` ON (
			    (
			     `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			    )
			   )
			  )
			 WHERE
			  (`terms`.`slug` = 'materias')
			 LIMIT 0,
			 3
			)
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'videos' AS `videos`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'videos')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'ensaios' AS `ensaios`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'ensaios')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'receitas' AS `receitas`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'receitas')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'eventos' AS `eventos`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'eventos')
			  LIMIT 0,
			  3
			 )
			UNION
			 (
			  SELECT
			   `terms`.`term_id` AS `term_id`,
			   `termrel`.`object_id` AS `object_id`,
			   'atletas' AS `atletas`
			  FROM
			   (
			    `wp_terms` `terms`
			    JOIN `wp_term_relationships` `termrel` ON (
			     (
			      `terms`.`term_id` = `termrel`.`term_taxonomy_id`
			     )
			    )
			   )
			  WHERE
			   (`terms`.`slug` = 'atletas')
			  LIMIT 0,
			  3
			 )
			";
			
			$results = $wpdb->get_results($sqlMaterias);
			$materiaAtual = '';
			foreach ($results as $r) :
				$id = $r->object_id;
				if ( $materiaAtual != $r->tipos ) $i = 0;
				$title  = get_the_title($id);

				switch ($r->tipos) {
					case 'materias':

						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'materias';

						break;

					case 'videos':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'videos';
						break;

					case 'ensaios':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';
						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'ensaios';
						break;

					case 'receitas':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'receitas';
						break;

					case 'eventos':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}

						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'eventos';
						break;

					case 'atletas':
						if ( $i == 0 ) {
							echo '<div data-show="'.$r->tipos.'" class="articles-box-display"><ul class="articles-displayed">';
						}
						
						echo '
						<li>
						<a href="'.get_the_permalink($id).'">
						<div><img src="'.wp_get_attachment_url( get_post_thumbnail_id( $id, 'thumbnail' ) ).'" alt="'.$title.'" class="post-thumbnail"></div>';

						if( strlen( $title ) > 100) {
							$str = explode( "\n", wordwrap( $title, 100));
							$title = $str[0] . '...';
							echo '<h3>'.$title.'</h3>';
						} else {
						    echo '<h3>'.$title.'</h3>';
						}

						echo '
						</a>
						</li>';

						if ( $i == 2 ) {
							echo '</ul></div>';
						}

						$i++;
						$materiaAtual = 'atletas';
						break;

					default:
						# code...
						break;
				}
			endforeach; ?>
			<div class="padded post-categories">
				<?php  				
				if ( have_posts() ) : ?>
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();
				?>
					<article class="post-preview">
						<a href="<?php echo get_permalink(); ?>">	
							<div class="thumbnail-container">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="" class="post-thumbnail">
							</div>
							<div class="text-preview">	
								<span class="average"><?php echo get_the_date( 'd/m/Y' ); ?></span>
								<h2>
									<?php
										$title  = get_the_title();

										if( strlen( $title ) > 50) {
											$str = explode( "\n", wordwrap( $title, 50));
											$title = $str[0] . '...';
											echo $title;
										} else {
										    echo $title;
										}
									?>								
								</h2>
							</div>
						</a>
					</article>
				<?php
					endwhile;

				else:
					echo '<p style="text-align: center;font-size: 26px;color: #fe0200;margin-bottom: 200px;">Não há posts dessa categoria.</p>';
				endif;
				?>
				
				<?php 

				$prev_link = get_previous_posts_link(__('&laquo; Older Entries'));
				$next_link = get_next_posts_link(__('Newer Entries &raquo;'));

				// as suggested in comments
				if ($prev_link || $next_link) {

				?>
				<div class="pagination-article"><!--
					--><nav class="woocommerce-pagination display-ib vertical-middle" style="width: 100%;">
						<?php					
							echo 'Página: '.paginate_links( apply_filters( 'woocommerce_pagination_args', array(
								'base'         => esc_url_raw( str_replace( 999999999, '%#%', remove_query_arg( 'add-to-cart', get_pagenum_link( 999999999, false ) ) ) ),
								'show_all'     => false,
								'format'       => '',
								'add_args'     => false,
								'current'      => max( 1, get_query_var( 'paged' ) ),
								'total'        => $wp_query->max_num_pages,
								'prev_text'    => '',
								'next_text'    => '',
								'end_size'     => 0,
								'mid_size'     => 1,
							) ) );
						?>
					</nav>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>