<?php
	include("include/inc_conexao.php");	
	include("include/inc_cadastro.php");
	
	//print_r($_POST);

	//echo "<hr>";
	
	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie	
	$redir = $_REQUEST["redir"];
	
	$msg = "";

	if($_POST){
	
		$email 	= addslashes($_REQUEST["txt-email"]);
		
		$ssql = "select cadastroid, capelido, cemail, csenha from tblcadastro where cemail='{$email}'";

		//echo "$ssql";
		
		$result = mysql_query($ssql);
		
		//print_r($result);

		if($result){
			$num_rows = mysql_num_rows($result);
			
			//echo $num_rows;

			while($row=mysql_fetch_assoc($result)){
				$id		= $row["cadastroid"];
				$nome 	= $row["capelido"];	
				$email 	= $row["cemail"];
				$senha  = $row["csenha"];
				
				$senha = base64_decode($senha);
				
				$log = date("Y-m-s H:i:s") . " - reenvio de senha id: $id e-mail: $email  através do IP ".$_SERVER['REMOTE_ADDR']." \r\n";
				
				$subject = "Reenvio de Senha - Site " . utf8_encode($site_nome);
				$body = '<table width="800" border="0" cellspacing="0" cellpadding="0" style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:#999;">
						  <tr>
							<td><a href="'.$site_site.'"><img src="'.$site_site.'/images/logo.png" alt="$site_nome" border="0" /></a></td>
							<td width="450" align="center">E-mail solicitado em '.date("d/m/Y H:i:s").'.</td>
						  </tr>
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="2"><p>Segue abaixo sua senha de acesso ao site '. utf8_encode($site_nome).':</p>
							<p>Senha de acesso: '.$senha.'</p>
							<p>&nbsp;</p>
							<p>Caso não tenha solicitado essa informação entre em contato com nossa equipe pelo e-mail '.$site_email.'.</p>
							<p>&nbsp;</p>
							<p>Atenciosamente,</p>
							<p>&nbsp;</p>
							<p>Equipe '.$site_nome.'<br>
							'.$site_site.'
							</p>
							<p>&nbsp;</p></td>
						  </tr>
						  <tr>
							<td colspan="2">&nbsp;</td>
						  </tr>
						</table>';
				
				$from_name	= $site_nome;
				$from_email	= $site_email;
				$to_name	= $nome;
				$to_email	= $email;
				
				//echo "$from_name<br>";
				//echo "$from_email<br>";
				//echo "$to_name<br>";
				//echo "$to_email<br>";

				if(envia_email($from_name, $from_email, $to_name, $to_email, $subject, $body)==true){
					//echo "123";
					$log .= date("Y-m-d H:i:s") . " - enviado com sucesso para $email \r\n";	
					$msg = "Sua senha foi reenviada para $email.<br /><br />Por questões de segurança essa solicitação gerou um log com seu IP de acesso.";
				}
				else
				{
					$log .= date("Y-m-d H:i:s") . " - erro ao enviar para $email \r\n";	
					$msg = "Erro ao tentar enviar sua senha para $email.<br /><br />Tente mais tarde ou então acesso nossa pagina <a href='central-relacionamento.php'>Central de Relacionamento</a>";
					$msg .= "<br /><br />Por questões de segurança essa solicitação gerou um log com seu IP de acesso.";
				}
				
				gera_log($log);
				
				
			}
			mysql_free_result($result);
		}
		
		
		if($num_rows == 0 ){
			$msg = "O email $email não foi localizado em nosso sistema. 
			<br/><br/>
			Caso tenha dificuldade de acessar sua conta entre em contato com nosso serviço de atendimento $site_email .";	
		}
		
						
	}

//echo "123";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Esqueci minha senha</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> - Senha" />
<meta name="description" content="<?php echo $site_nome;?>. Recupere sua senha." />
<meta name="keywords" content="<?php echo $site_nome;?> Login de Acesso" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Login de Acesso" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/esqueci-minha-senha.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		document.getElementById("txt-email").focus();
	});	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
	<div id="products-title">Esqueci minha senha</div>
    <div id="container-cadastro">
        <div id="box-esqueci-minha-senha">    
            <?php
            	if(!$_POST){
					echo '<form name="frm_login" id="frm_login" method="post" action="esqueci-minha-senha.php" onsubmit="return valida_esqueci_senha();">
								<span class="login-label">E-mail:<br /><br /></span>
								<input type="text" name="txt-email" id="txt-email" class="txt-login-field" maxlength="150" style="margin:0;" />
							  	<span class="login-label"><br />Para recuperar sua senha de acesso informe seu e-mail</span>
								<input type="image" src="images/btn-enviar-senha.png" alt="Botão Enviar Senha" title="Botão Enviar Senha" id="btn-enviar-senha"/>
							</form>';	
				}
				else
				{
					echo $msg;					
				}
			?>
        </div>
   	</div>  
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>