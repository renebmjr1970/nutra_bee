<?php
include("include/inc_conexao.php");
header("Content-type: text/xml");

$ssql = "select tblproduto.produtoid, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.psubtitulo,  tblproduto.pdescricao,  
			tblproduto.pdescricao_detalhada, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, tblproduto.plink_seo, 
			(tblproduto_midia.marquivo) as pimagem
			from tblproduto 
			left join tblproduto_midia on tblproduto_midia.mcodproduto=tblproduto.produtoid and tblproduto_midia.mprincipal=-1
			where tblproduto.pdisponivel = -1
			";
$result = mysql_query($ssql);
if($result){
	
	echo '<?xml version="1.0" encoding="UTF-8"?>';
	echo '<produtos>';

		while($row=mysql_fetch_assoc($result)){
			
			$link	=	"http://www.popdecor.com.br/" . $row["plink_seo"];
			$imagem	=	"http://www.popdecor.com.br/" . $row["pimagem"];
			$imagem =	str_replace("-tumb.","-big.",$imagem);
			
			$valor_comparativo = $row["pvalor_unitario"];
			
			if( $row["pvalor_comparativo"] > $row["pvalor_unitario"] ){
				$valor_comparativo = $row["pvalor_comparativo"];	
			}
			
			
			echo '<produto>';
			echo '<nome_titulo><![CDATA[' . $row["pproduto"] . ']]></nome_titulo>';
			echo '<codigo>' . $row["pcodigo"] . '</codigo>';
			echo '<link>' . $link . '</link>';
			echo '<imagem>' . $imagem . '</imagem>';
			echo '<descricao><![CDATA[' . $row["pdescricao"] . ']]></descricao>';
			echo '<categoria>25,41,44</categoria>';
			echo '<preco_real>' . number_format( $valor_comparativo, 2, ',', '.') . '</preco_real>';
			echo '<preco_desconto>' . number_format($row["pvalor_unitario"], 2, ',', '.') . '</preco_desconto>';
			echo '<specific>
					<marca/>
					<cor/>
					<tamanho/>
					<autor/>
					<artista/>
					<editora/>
					<ritmo/>
					<distribuidora/>
					<sinopse/>
					<loja>Pop Decor</loja>
				</specific>';
			echo '</produto>';
		}
		mysql_free_result($result);

	echo '</produtos>';

}
	
?>