<?php
	include("include/inc_conexao.php");	

	$conteudo_titulo;
	$conteudo_descricao;
	$conteudo_palavra_chave;
	$conteudo_texto;
	
	//Nutricionista
	get_conteudo(17);	
	$email = '';
	$nome = '';
	$msg = '';
	if(isset($_COOKIE["cadastro"])){
		$email = mysql_fetch_array(mysql_query("select cemail from tblcadastro where cadastroid = '".$_COOKIE["cadastro"]."';"), MYSQL_ASSOC);
		$email = $email["cemail"];
		$nome = $_COOKIE["apelido"];
	}
	if(isset($_POST["enviar"])){
		if($_POST["captcha"] == $_SESSION['captcha']){
			$body = '
				Nome: '.$_POST["nome"].'<br>
				E-mail: '.$_POST["email"].'<br>
				Mensagem: '.$_POST["mensagem"].'<br>
				Data: '.date("d/m/y H:i:s").'
			';
			envia_email($site_nome, $site_email, "Contato", $site_email, "Fale conosco", $body);
			$msg = "Mensagem enviada com sucesso!";
		}else{
			$msg = "Captcha digitado incorretamente.";
		}
	}
	$formulario = '
		<form name="formulario" action="fale-conosco.php" method="post" onsubmit="return valida_contato()">
			<table border="0" width="500px">
				<tr>
					<td><b>Nome:</b></td>
					<td><input type="text" name="nome" class="txt-contato-field" value="'.$nome.'"></td>
				</tr>
				<tr>
					<td><b>E-mail:</b></td>
					<td><input type="text" name="email" class="txt-contato-field" value="'.$email.'"></td>
				</tr>
				<tr>
					<td valign="top"><b>Mensagem:</b></td>
					<td><textarea name="mensagem" class="txt-contato-field" style="height:100px;"></textarea></td>
				</tr>
				<tr>
					<td align="center" colspan="2"><img src="captcha.php" alt="captcha"></td>
				</tr>
				<tr>
					<td><b>Digite o texto da<br>imagem ao lado</b></td>
					<td><input type="text" name="captcha" class="txt-contato-field"></td>
				</tr>
				<tr>
					<td colspan="2" align="right"><input type="submit" name="enviar" value="Enviar" id="enviar-contato"></td>
				</tr>
			</table>
		</form>
	';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> <?php echo $conteudo_titulo;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> <?php echo $conteudo_titulo;?>" />
<meta name="description" content="<?php echo $conteudo_descricao;?>" />
<meta name="keywords" content="<?php echo $conteudo_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $conteudo_titulo;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/sobre.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>
<script type="text/javascript">
	function valida_contato(){
		if(document.formulario.nome.value.length < 4){ alert('Digite o nome corretamente'); document.formulario.nome.focus(); return false; }
		if(document.formulario.email.value.length < 4){ alert('Digite o e-mail corretamente'); document.formulario.email.focus(); return false; }
		if(document.formulario.mensagem.value.length < 4){ alert('Digite a mensagem'); document.formulario.mensagem.focus(); return false; }
		if(document.formulario.captcha.value.length < 5){ alert('Digite o captcha corretamente'); document.formulario.captcha.focus(); return false; }
	}
	<?php if($msg != ''){ echo "alert('$msg');"; } ?>
</script>
<?php
include("include/inc_analytics.php");	
?>
</head>

<body>
	<div id="global-container">
		<div id="header-content">
	        <?php
				include("inc_header.php");
			?>
		</div>
			<div id="main-box-container">
				<div id="institucional-img"></div>
				    <div id="menu-conta-left">
						<?php
				            include("inc_left_conteudo.php");
				        ?>
				    </div>
		    <div id="box-meio-minha-conta">
		    	<div id="box-meus-dados">
		            <h4 class="h4-minha-conta"><?php echo $conteudo_titulo;?></h4>
		            <span class="dados-conta">&nbsp;</span>
		            
		            <div class="box-interna"> 
		            	<span class="descricao-interna">
		                <?php echo str_replace("#formulario_contato#",$formulario,$conteudo_texto);?>
		                </span>
		            </div>
		    	</div>
		    </div>
			</div>
			    <div id="footer-container">
					<?php
			            include("inc_footer.php");
			        ?>
			    </div>
	</div>
	<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>