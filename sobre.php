<?php
	include("include/inc_conexao.php");	

	$conteudo_titulo;
	$conteudo_descricao;
	$conteudo_palavra_chave;
	$conteudo_texto;
	
	//2 - Sobre a Empresa
	get_conteudo(2);
	

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> <?php echo $conteudo_titulo;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> <?php echo $conteudo_titulo;?>" />
<meta name="description" content="<?php echo $conteudo_descricao;?>" />
<meta name="keywords" content="<?php echo $conteudo_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $conteudo_titulo;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/sobre.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
    <div id="global-container">
    	<div id="header-content">
            <?php
    			include("inc_header.php");
    		?>
    	</div>
        	<div id="main-box-container">
                <div id="institucional-img"></div>
                	<div id="menu-conta-left">
                		<?php
                            include("inc_left_conteudo.php");
                        ?>
                    </div>   
                    <div id="box-meio-minha-conta">
                    	<div id="box-meus-dados">
                            <h4 class="h4-minha-conta"><?php echo $conteudo_titulo;?></h4>
                            <span class="dados-conta">&nbsp;</span>
                            
                            <div class="box-interna"> 
                            	<span class="descricao-interna">
                                <?php echo $conteudo_texto;?>
                                </span>
                            </div>
                    	</div>               
                    </div>    	
        	</div>
                <div id="footer-container">
            		<?php
                        include("inc_footer.php");
                    ?>
                </div>
    </div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>