<?php

	include("include/inc_conexao.php");
	include("include/inc_cadastro.php");
	include("include/inc_frete.php");


	function verifica_endereco($cep){
		$frete = new Frete();
		$frete->cep_destino = $cep;
			
		$frete->doEndereco();
		
		$endereco 	= $frete->logradouro;
		$bairro 	= $frete->bairro;
		$cidade		= $frete->cidade;
		$estado		= $frete->estado;
		
			
		if(strpos($endereco," - ")){
			$endereco = left($endereco,strpos($endereco," - "));	
		}
		if(strpos($endereco,"/")){
			$endereco = left($endereco, strpos($endereco,"/"));	
		}
			
		$endereco = trim($endereco);	
		
		return "ok||".$endereco."||".$bairro."||".$cidade."||".$estado."||";
		//echo "ok||aaa||aaaaa||aaaaa||aaaaaa||";
		//die();
	}
	
	
	/*-------------------------------------------------------------
	variaveis
	-------------------------------------------------------------*/	
	$cadastro = 0;
	$enderecoid = 0;
	
	$titulo = '';
	$nome = '';
	$endereco = '';
	$numero = '';
	$complemento = '';
	$bairro = '';
	$cidade = '';
	$estado = '';
	$referencia = '';


	/*-------------------------------------------------------------------	
	//navegação com ssl
	---------------------------------------------------------------------*/
	$config_certificado_instalado = get_configuracao("config_certificado_instalado");
	if($config_certificado_instalado==-1){
		if(strpos($_SERVER['SERVER_NAME'],".com")>0){
			if($_SERVER['SERVER_PORT']==80){
				header("location: https://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
				exit();
			}
		}	
	}

	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=endereco.php");
		exit();
	}
	


	/*------------------------------------------------------------------------
	verifica se tem algum orcamento em aberto com base em cookies
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["orcamento"])){
		$orcamento = $_COOKIE["orcamento"];
		if(!is_numeric($orcamento)){
			$orcamento = 0;	
		}
	}
	if($orcamento==0){
		header("location: index.php");
		exit();
	}		


	/*-----------------------------------------------------------------
	
	-----------------------------------------------------------------*/
	if($_POST){
	
		// gravar ou atualiza o endereço
		if($_POST["action"]=="gravar"){
			
			
			//$ret = file_get_contents($site_site."/ajax_frete.php?action=endereco&cep=".get_only_numbers($_REQUEST["endereco_cep"]),true);
			//echo $ret . "";
			//$ret = "ok||aaaa||aaaa||aaaaa||aaaaa||";
			/*
			$ret = verifica_endereco(get_only_numbers($_REQUEST["endereco_cep"]));
			if(trim($ret)=="ok||||||||||"){
				$msg = "Cep / Endereço inválido.";
			}
			else
			{
			*/
				$cadastro 		= $_SESSION["cadastro"];
				$enderecoid		=	$_POST["enderecoid"];
				$titulo			=	addslashes($_POST["endereco_titulo"]);
				$nome			=	addslashes($_POST["endereco_nome"]);
				$endereco		=	addslashes($_POST["endereco_endereco"]);
				$numero			=	addslashes($_POST["endereco_numero"]);
				$complemento	=	addslashes($_POST["endereco_complemento"]);
				$bairro			=	addslashes($_POST["endereco_bairro"]);
				$cidade			=	addslashes($_POST["endereco_cidade"]);
				$estado			=	addslashes($_POST["endereco_estado"]);
				$cep			=	get_only_numbers($_POST["endereco_cep"]);
				$referencia		=	addslashes($_POST["endereco_referencia"]);
			
				$ret = grava_endereco($enderecoid);
				if(!$ret){
					$msg =  "Erro ao gravar endereço, confira todos os campos preenchidos ou tente novamente mais tarde.";	
				}
				else
				{
					$msg =  "Endereço salvo com sucesso.";	
				}
					//echo $enderecoid;
				//reseta as variaveis
				endereco_reset();
			//}
			
		}
		
		
		
		
		/*-------------------------------------
		// exclui
		-------------------------------------*/
		if($_POST["action"]=="excluir"){
			$cadastro 		= $_SESSION["cadastro"];
			$enderecoid		=	$_REQUEST["enderecoid"];
			if(!del_endereco($enderecoid)){
				$msg =  "Erro ao excluir o endereço, tente novamente mais tarde.";	
			}
			else
			{
				$msg =  "Endereço excluido com sucesso.";	
			}
		}			
		
		// gravar ou atualiza o endereço
		if($_POST["action"]=="editar"){
			$cadastro 		= $_SESSION["cadastro"];
			$enderecoid		=	$_REQUEST["enderecoid"];
			get_endereco($enderecoid);
		}	
		
		
		
		
		
	}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Finalizar Pedido Endereço</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Finalizar Pedido Endereço" />
<meta name="description" content="<?php echo $site_nome;?> Finalizar Pedido Endereço. Selecione o endereço de entrega de seu pedido." />
<meta name="keywords" content="<?php echo $site_nome;?> Finalizar Pedido Endereço" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?>  Finalizar Pedido Endereço" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/endereco.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#endereco_cep").mask("99999-999");
		
		$('#endereco_cep').keyup(function(e){ 
			var t = document.getElementById("endereco_cep").value;
			t = get_only_numbers(t);
			if(t.length==8){
				endereco_load(t);
			}
		}); 

		$("#novo_endereco").click(function(){
			$("#novo_endereco").hide();
			$("#container-cadastro").show();
			$('body,html').animate({
				scrollTop: 600
			}, 1000);
			$("#endereco_titulo").focus();
		});
		
		
		<?php
			if( $_REQUEST["action"] == "novo" || intval( $_REQUEST["enderecoid"] > 0 && $_REQUEST['action'] != "gravar" ) ){
				echo '$("#novo_endereco").trigger("click");';	
			}
		?>
		
		
		
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">

	<div id="header-content">

        <?php
			include("inc_headerSTEP.php");
		?>

    </div>
  

	<div id="main-box-container">
		<div id="andamento">
			<span class="passox" style="margin-left:0;">1 - Identificação <img src="images/setaSTEP.jpg"/></span>
			<span class="passox ativo" style="margin-left: 120px;">2 - Entrega</span>
			<span class="passox ">3 - Pagamento</span>
			<span class="passox ">4 - Confirmação</span>
		</div>
	<span id="tituloStep">Entrega</span>
	<span id="subtituloStep">Escolha o endereço de entrega</span>
	<div id="enderecos-cadastrados">
    	<?php
			$cadastro = $_SESSION["cadastro"];
     		$enderecos = get_enderecos();
			
			if( trim($enderecos) == ""){
				echo 'Você não tem nenhum endereço cadastrado ainda.';	
			}
			else
			{
				echo $enderecos;	
			}
			
		?>
        

  </div>
  
  <div class="campo-cadastro">
        <span class="tit-cat-cadastro"><span id="novo_endereco">Clique aqui para cadastrar outras opções de endereço.</span></span>
  </div>  
  
  <div id="container-cadastro" style="display:none;">
      <form name="frm_endereco" id="frm_endereco" method="post" action="endereco.php" onsubmit="return valida_endereco();">
      <input type="hidden" name="action" id="action" value="gravar" />
      <input type="hidden" name="enderecoid" id="enderecoid" value="<?php echo $enderecoid;?>" />
        <div class="campo-cadastro">
	        <span class="tit-cat-cadastro"><?php echo ($enderecoid == 0) ? "Cadastrar outro" : "Atualizar"; ?> endereço de entrega:</span>
        </div>
        
        <div class="campo-cadastro">
    	    <span class="label-campos">Identificação do endereço*</span> 
            <input name="endereco_titulo" type="text" class="campos-cadastro" id="endereco_titulo" value="<?php echo $titulo;?>" maxlength="50" />
            <span class="label-campos-exemplo">exemplo: casa, trabalho, fatura do cartão, namorado(a)</span>
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Nome do destinatário*</span> <input name="endereco_nome" type="text" class="campos-cadastro" id="endereco_nome" value="<?php echo $nome;?>" maxlength="50" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">CEP</span><input name="endereco_cep" type="text" class="campos-cadastro-cep" id="endereco_cep" value="<?php echo $cep;?>" size="15" maxlength="9" />
            <span class="label-campos-exemplo" id="spinner" style="display:none;"><img src="images/spinner.gif" border="0" /> consultando...</span>
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Endereço</span> <input name="endereco_endereco" type="text" class="campos-cadastro" id="endereco_endereco" value="<?php echo $endereco;?>" maxlength="200" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Número</span> <input name="endereco_numero" type="text" class="campos-cadastro" id="endereco_numero" value="<?php echo $numero;?>" maxlength="20" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Complemento</span><input name="endereco_complemento" type="text" class="campos-cadastro" id="endereco_complemento" value="<?php echo $complemento;?>" maxlength="20" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Bairro</span><input name="endereco_bairro" type="text" class="campos-cadastro" id="endereco_bairro" value="<?php echo $bairro;?>" maxlength="100" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Cidade</span><input name="endereco_cidade" type="text" class="campos-cadastro" id="endereco_cidade" value="<?php echo $cidade;?>" maxlength="100" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Estado</span>
   		  <select name="endereco_estado" id="endereco_estado" class="campos-cadastro">
            <?php 
				echo monta_combo_estado($estado);
			?>
          </select>
        </div>
        
        <div class="campo-cadastro">	
        	<span class="label-campos">Ponto de referência</span>
        	<input name="endereco_referencia" type="text" class="campos-cadastro" id="endereco_referencia" value="<?php echo $referencia;?>" maxlength="100" />
        </div>
        
        <div id="btns-navegacao">
            <div id="btn-voltar">
	            <a href='endereco.php'>Voltar</a>
            </div>
            <div id="btn-continuar">
              <input name="cmd_continuar" id="cmd_continuar" type="image" src="images/btn-cadastrar.png" border="0" />
            </div>
        </div>
    </form>
  </div>   
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footerSTEP.php");
        ?>
    </div>
</div>
</body>
</html>