<?php
	include("include/inc_conexao.php");	

/*-------------------------------------------------------------------
base href
--------------------------------------------------------------------*/
$server = ($_SERVER['SERVER_PORT']==80 ? "http://" : "https://") . $_SERVER['SERVER_NAME'] . str_replace("marca.php","",$_SERVER['SCRIPT_NAME']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $site_nome;?> Busca de Produtos</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Busca de Produtos" />
<meta name="description" content="<?php echo $site_nome;?>. Localize os produtos que deseja através de nossos filtros de busca" />
<meta name="keywords" content="<?php echo $site_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> " />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/busca.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript">
  function replace_param(theURL, paramName, newValue){
    var uri_array = theURL.split('?');
    try{
	  var params_array = uri_array[1].split('&');
	}catch(e){
	  theURL = theURL+'/?pagina=1';
	  var uri_array = theURL.split('?');
	  var params_array = uri_array[1].split('&');
	}
	var achou = 0;
    var items_array = new Array;
    for (i=0; i<params_array.length; i++){
      items_array = params_array[i].split('='); 
      if (items_array[0] == paramName){
        params_array[i] = items_array[0] + '=' + newValue;
		achou = 1;
      }
    }
	var link = uri_array[0] + '?' + params_array.join('&');
	if(achou == 0){
	  link = link+'&pagina=2';
	}
    return link;
  }
  function pag_anterior(){
    var pagina = $("#txt-pagina").val();
    var url = replace_param(document.URL, 'pagina', parseInt(pagina-1));
    window.location = url;
  }
  function pag_posterior(){
    var pagina = $("#txt-pagina").val();
    var url = replace_param(document.URL, 'pagina', parseInt(pagina*1+1));
    window.location = url;
  }
  $(document).ready(function(e){
    $("#txt-pagina").keyup(function(e){
	  if (e.which == 13){
        var pagina = $("#txt-pagina").val();
        var url = replace_param(document.URL, 'pagina', pagina);
		window.location = url;
	  }
    });
  });
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
	  <div align="center">
        <img src="images/blog.jpg" border="0" width="800px"  style="position: relative;top: 30px;" />
	  </div>
    </div>
</div>
    
    <div id="footer-container">
    <?php
		include("inc_footer.php");
	?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>