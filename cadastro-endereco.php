<?php
	include("include/inc_conexao.php");
	include("include/inc_cadastro.php");	
	
	/*-------------------------------------------------------------
	variaveis
	-------------------------------------------------------------*/	
	$cadastro = 0;
	$enderecoid = 0;
	
	$titulo;
	$nome;
	$endereco;
	$numero;
	$complemento;
	$bairro;
	$cidade;
	$estado;
	$referencia;
	$redir = $_REQUEST["redir"];



	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=minha-conta.php");
		exit();
	}
	




	/*-----------------------------------------------------------------
	
	-----------------------------------------------------------------*/
	if($_POST){
	
		// gravar ou atualiza o endereço
		if($_POST["action"]=="gravar"){
						
			$cadastro 		= $_SESSION["cadastro"];
			$enderecoid		=	$_POST["enderecoid"];
			$titulo			=	addslashes($_POST["endereco_titulo"]);
			$nome			=	addslashes($_POST["endereco_nome"]);
			$endereco		=	addslashes($_POST["endereco_endereco"]);
			$numero			=	addslashes($_POST["endereco_numero"]);
			$complemento	=	addslashes($_POST["endereco_complemento"]);
			$bairro			=	addslashes($_POST["endereco_bairro"]);
			$cidade			=	addslashes($_POST["endereco_cidade"]);
			$estado			=	addslashes($_POST["endereco_estado"]);
			$cep			=	get_only_numbers($_POST["endereco_cep"]);
			$referencia		=	addslashes($_POST["endereco_referencia"]);
			
			if(!grava_endereco($enderecoid)){
				$msg =  "Erro ao gravar endereço, confira todos os campos preenchidos ou tente novamente mais tarde.";	
			}
			else
			{
				$msg =  "Endereço salvo com sucesso.";	
			}
			
			//reseta as variaveis
			endereco_reset();
			
			if($redir=="pagamento"){
				header("location: pagamento.php");
				exit();
			}
			
			
		}
		
		
		
		
		/*-------------------------------------
		// exclui
		-------------------------------------*/
		if($_POST["action"]=="excluir"){
			$cadastro 		= $_SESSION["cadastro"];
			$enderecoid		=	$_REQUEST["enderecoid"];
			if(!del_endereco($enderecoid)){
				$msg =  "Erro ao excluir o endereço, tente novamente mais tarde.";	
			}
			else
			{
				$msg =  "Endereço excluido com sucesso.";	
			}
		}			
		
		// gravar ou atualiza o endereço
		if($_POST["action"]=="editar"){
			$cadastro 		= 	$_SESSION["cadastro"];
			$enderecoid		=	$_REQUEST["enderecoid"];
			get_endereco($enderecoid);
		}	
				
		
	}
	

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> - Endereços</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> - Endereços" />
<meta name="description" content="<?php echo $site_nome;?>. Cadastre seus endereços de entrega" />
<meta name="keywords" content="<?php echo $site_nome;?> Endereço" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> " />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/cadastro-endereco.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/jquery-mask.js"></script>

<script language="javascript" type="text/javascript">
	$(document).ready(function() {	
		$("#endereco_cep").mask("99999-999");
		
		$('#endereco_cep').keyup(function(e){ 
			var t = document.getElementById("endereco_cep").value;
			t = get_only_numbers(t);
			if(t.length==8){
				endereco_load(t);
			}
		}); 	
		
		
		$("#novo_endereco").click(function(){
			$("#container-cadastro").show();
			$('body,html').animate({
				scrollTop: 1000
			}, 1000);
			$("#endereco_titulo").focus();
		});
		
		
		<?php
			if( $_REQUEST["action"] == "novo" || intval( $_REQUEST["enderecoid"] > 0 && $_REQUEST['action'] != "gravar" ) ){
				echo '$("#novo_endereco").trigger("click");';	
			}
		?>		
		
		
    });	
</script>

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">

         <div id="menu-topo-conta">
    		<a href="minha-conta.php" id="meu-perfil">Minha Conta</a>
            <span style="float:left; padding:0; font-size:15px;">&nbsp;|&nbsp;</span>
            <a href="meus-pedidos.php" id="meus-pedidos">Meus Pedidos</a>
    	</div>  
  

  <div id="enderecos-cadastrados">
      <div class="campo-cadastro">
            <span class="tit-cat-cadastro">Seus endereços: <span style="font-weight:normal; color:#F00"><?php echo $msg;?></span></span>
   	 </div>
    	<?php
			$ret ="";
			$cadastro = $_SESSION["cadastro"];
			$ssql = "select enderecoid, ecodcadastro, etitulo, enome, ecep, eendereco, enumero, ecomplemento, ebairro, ecidade, eestado, ereferencia
					from tblcadastro_endereco where ecodcadastro = ".$GLOBALS["cadastro"]."";				
					//echo $ssql;
			$result = mysql_query($ssql);
			if($result){
				while($row=mysql_fetch_assoc($result)){
					$ret .= '        
						<div class="box-endereco-entrega-container">
							
							<span class="id-end">'.$row["etitulo"].'</span>
							<a class="info-usar-endereco" href="javascript:void(0);" onclick="javascript:endereco_frete('.$row["enderecoid"].');"></a>
							<div class="box-endereco-entrega">
								<span class="destinatario"><b>Destinatário:</b> '.$row["enome"].'</span>
								<span class="info-endereco"></b>Endereço:</b> '.$row["eendereco"]. ', '. $row["enumero"]. '&nbsp;&nbsp;&nbsp;' . $row["ecomplemento"] . '</span>
								<span class="info-endereco">'.$row["ebairro"].'</span>
								<span class="info-endereco">'.$row["ecidade"].'-'.$row["eestado"].'</span>
								<span class="dados-cep">CEP: '.substr($row["ecep"],0,5).'-'.substr($row["ecep"],5,3).'</span>
								<span class="referencia">Referência: '.$row["ereferencia"].'</span>
							</div>
							
							<a class="info-excluir" onclick="javascript:endereco_excluir('.$row["enderecoid"].');">Excluir</a>
							<a class="info-editar" onclick="javascript:endereco_editar('.$row["enderecoid"].');">Editar</a>
						
						</div>								
						';
				}
				mysql_free_result($result);
			}
			echo $ret;
		?>

  </div>
  
  <div class="campo-cadastro">
        <span class="tit-cat-cadastro"><span id="novo_endereco" style="font-weight:bold; text-decoration:none; cursor:pointer;"><u>Clique</u> aqui para cadastrar outras opções de endereço.</span></span>
  </div>    
  
  <div id="container-cadastro" style="display:none;">
      <form name="frm_endereco" id="frm_endereco" method="post" action="cadastro-endereco.php" onsubmit="return valida_endereco();">
      <input type="hidden" name="action" id="action" value="gravar" />
      <input type="hidden" name="enderecoid" id="enderecoid" value="<?php echo $enderecoid;?>" />
      <input type="hidden" name="redir" id="redir" value="<?php echo $redir;?>" />
        <div class="campo-cadastro">
	        <span class="tit-cat-cadastro"><?php echo ($enderecoid == 0) ? "CADASTRAR NOVO" : "ATUALIZAR"; ?> ENDEREÇO</span>
        </div>
        
        <div class="campo-cadastro">
    	    <span class="label-campos">Identificação do endereço*</span> 
            <input name="endereco_titulo" type="text" class="campos-cadastro" id="endereco_titulo" value="<?php echo $titulo;?>" maxlength="50" />
            <span class="label-campos-exemplo">exemplo: casa, trabalho, fatura do cartão, namorado(a)</span>
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Nome do destinatário*</span> <input name="endereco_nome" type="text" class="campos-cadastro" id="endereco_nome" value="<?php echo $nome;?>" maxlength="50" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">CEP</span><input name="endereco_cep" type="text" class="campos-cadastro-cep" id="endereco_cep" value="<?php echo $cep;?>" size="15" maxlength="9" />
            <span class="label-campos-exemplo" id="spinner" style="display:none;"><img src="images/spinner.gif" border="0" /> consultando...</span>
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Endereço</span> <input name="endereco_endereco" type="text" class="campos-cadastro" id="endereco_endereco" value="<?php echo $endereco;?>" maxlength="200" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Número</span> <input name="endereco_numero" type="text" class="campos-cadastro" id="endereco_numero" value="<?php echo $numero;?>" maxlength="20" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Complemento</span><input name="endereco_complemento" type="text" class="campos-cadastro" id="endereco_complemento" value="<?php echo $complemento;?>" maxlength="20" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Bairro</span><input name="endereco_bairro" type="text" class="campos-cadastro" id="endereco_bairro" value="<?php echo $bairro;?>" maxlength="100" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Cidade</span><input name="endereco_cidade" type="text" class="campos-cadastro" id="endereco_cidade" value="<?php echo $cidade;?>" maxlength="100" />
        </div>
        
        <div class="campo-cadastro">
        	<span class="label-campos">Estado</span>
   		  <select name="endereco_estado" id="endereco_estado" class="campos-cadastro">
            <option value=""></option>
            <?php 
				echo monta_combo_estado($estado);
			?>
          </select>
        </div>
        
        <div class="campo-cadastro">	
        	<span class="label-campos">Ponto de referência</span>
        	<input name="endereco_referencia" type="text" class="campos-cadastro" id="endereco_referencia" value="<?php echo $referencia;?>" maxlength="100" />
        </div>
        
        <div id="btns-navegacao">
            <div id="btn-voltar">
	            <a href='minha-conta.php'>Voltar</a>
            </div>
            <div id="btn-continuar">
              <input name="cmd_continuar" id="cmd_continuar" type="image" src="images/btn-continuar.jpg" border="0" />
            </div>
        </div>
    </form>
  </div>
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
</body>
</html>