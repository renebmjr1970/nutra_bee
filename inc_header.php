<link href="css/owl.carousel.css" rel="stylesheet">
<script src="js/owl.carousel.js"></script>
<?php
	//header("Location: http://nutracorpore.com.br");
	$welcome_message = get_welcome_message();
	
	$totals = get_carrinho_totals();
	
	@list($cart_items, $cart_total, $cart_subtotal, $cart_frete, $cart_desconto, $cart_desconto_cupom, $cart_peso) = explode("||",$totals);
	
	if(!is_numeric($cart_items)){
		$cart_items = 0;	
	}
	
	
	
	$cart_items 			= number_format($cart_items,0);
	$cart_total 			= number_format($cart_total,2,",",".");
	$cart_subtotal 			= number_format($cart_subtotal,2,",",".");
	$cart_frete				= number_format($cart_frete,2,",",".");
	$cart_desconto			= number_format($cart_desconto,2,",",".");
	$cart_desconto_cupom	= number_format($cart_desconto_cupom,2,",",".");
	$cart_peso				= number_format($cart_peso,2,",",".");
	


	/*----------------------------------------------------------------------
	verifica se veio parametro utm_source para gerar descontos
	----------------------------------------------------------------------*/
	if( isset( $_REQUEST["utm_source"] ) ){
		$expires = time()+ 60 * 60 * 1 * 1; // 1 hora de cookie
		$utm_source = addslashes($_REQUEST["utm_source"]);
		
		$ssql = "select dutm_source from tbldesconto where dutm_source='{$utm_source}'";
		$result = mysql_query($ssql);
		if($result){
			$num_rows = mysql_num_rows($result);
			if($num_rows>0){
				$_SESSION["utm_source"] = $utm_source;
				setcookie("utm_source", $utm_source, $expires);
			}
			else
			{
				unset($_SESSION["utm_source"]);
				unset($_COOKIE["utm_source"]);
			}	
		}	
	}
	
	
	
	/*----------------------------------------------------------------------
	HTTP REFER - Referência de venda
	----------------------------------------------------------------------*/	
	if( isset($_SERVER['HTTP_REFERER']) ){
		$_SESSION['HTTP_REFERER'] = left( addslashes( $_SERVER['HTTP_REFERER'] ), 255);	
	}
	
	
	
	/*----------------------------------------------------------------------
	insere a palavra bucada na tblbusca quando vindo do buscador Google
	origem = 1 : busca do site
	origem = 2 : busca google
	----------------------------------------------------------------------*/
	if(isset($_SERVER['HTTP_REFERER'])){
	  $url = $_SERVER['HTTP_REFERER'];
	}else{
	  $url = "index.php";
	}
	$query = parse_url ($url, PHP_URL_QUERY); 
	$host = parse_url ($url, PHP_URL_HOST); 
	
	if (strstr ($query, 'q=') && strstr ($host, 'google.')) { 
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$refer = "google";
		$refer = strtolower($refer);
		
		$keys = explode("&",$query);
		$string = $keys[2];
		$string = str_replace("q=","",$string);
		$string = urldecode($string);
		$string = addslashes(urldecode($string));
		
		$ssql = "select btag from tblbusca where btag='{$string}' and bip='{$ip}'";
		$result = mysql_query($ssql);
		if($result){
			if(mysql_num_rows($result)==0){
				$ssql = "insert into tblbusca (btag, borigem, breferencia, bip, bdata_alteracao, bdata_cadastro)
							values('{$string}','2','{$refer}','{$ip}','{$data_hoje}','{$data_hoje}')";
				mysql_query($ssql);
			}
			//echo $ssql.' '. $refer;
		}
		
	}
	
	
	
	/*-----------------------------------------------------------------------
	Estilo do menu por pagina / categoria
	-----------------------------------------------------------------------*/
	$url_pagina = str_replace("/nutraNew/","", $_SERVER['REQUEST_URI']);
	

?>
<div id="header">
	
  <div id="top-header">
        <div id="logo" title="<?php echo $site_nome;?>" alt="<?php echo $site_nome;?>">
        	<a href="index.php"><img src="images/logo.png" alt="<?php echo $site_nome;?>" title="<?php echo $site_nome;?>" border="0" /></a>        
        </div>
		<div id="exclusivo">
			CONTEÚDO EXCLUSIVO >>
		</div>
		
		
		<div id="top-menu">
			<?php carregaCategorias(2); ?>
		</div>
		<div id="top-icons">
			<span class="item"><img src="images/descontoBoleto.png" alt="Desconto"></span>
			<span class="item"><img src="images/frete.png" alt="Frete"></span>
			<span class="item"><a href="nutricionista.php"><img src="images/nutricionista.png" alt="Nutricionista" border="0"></a></span>
		</div>
		<div id="buscaBloco">
			<form name="busca" method="post" action="busca/"  onsubmit="return valida_busca(1);">
				<input type="text" class="ui-autocomplete-input" onkeypress="javascript:busca_auto_completa();" autocomplete="off" role="textbox" aria-autocomplete="list" aria-haspopup="true" name="busca" id="busca" value="<?=$string ?>">
				<input type="submit" value="" id="buscaEnviar">
			</form>
		</div>
	</div>
	<div id="artigos-container">
		<div id="artigos-nav"></div>
		<div id="prev-artigos-content"></div>
	</div>
</div>
<div id="main-menu-container">
	<div class="carousel-container">
<!-- 		<span class="item" id="produtos-menu" onmouseover="produtos_menu(this)" onclick="produtos_menu(this)">Produtos</span>
		<span class="item" id="fabricantes-menu" onmouseover="fabricantes_menu(this)" onclick="fabricantes_menu(this)">Fabricantes</span>
		<span class="item" id="kits-menu" onmouseover="kits_menu(this)" onclick="kits_menu(this)">Kits Promocionais</span>
		<span class="item" id="objetivos-menu" onmouseover="objetivos_menu(this)" onclick="objetivos_menu(this)">Meu objetivo</span>
		<span class="item" id="top20-menu" onmouseover="top20_menu(this)" onclick="top20_menu(this)">Top 20</span> -->

		<span class="item" id="produtos-menu">Produtos</span>
		<span class="item" id="fabricantes-menu">Fabricantes</span>
		<span class="item" id="kits-menu">Kits Promocionais</span>
		<span class="item" id="objetivos-menu">Meu objetivo</span>
		<span class="item" id="top20-menu">Top 20</span>
		<div id="main-menu-fixed-content">
			<?php if(!isset($_COOKIE["cadastro"])){ ?>
			<span class="fixed">Olá, visitante, faça seu &nbsp; <a href="login.php">login</a><br>ou &nbsp; <a href="login.php">cadastre-se</a></span>
			<?php }else{ ?>
			<span class="fixed">Olá, <?=$_COOKIE["apelido"] ?>!<br><a href="index.php?action=logout">Clique aqui</a> para sair</span>
			<?php } ?>
			<div class="fixed" onmouseover="popula_carrinho(<?php echo $cart_items;?>);" onmouseout="layer_carrinho_off();" onclick="window.location='carrinho.php';">
				<div id="cart"><?=$cart_items ?>&nbsp;</div>
				<div id="cart-details"><span id="texto"><?php if($cart_items){ echo "Você possui $cart_items item(s)"; }else{ echo "Seu carrinho está vazio"; } ?></span></div>
			</div>
		</div>
		<div id="cart-box" onmouseover="popula_carrinho(<?php echo $cart_items;?>);" onmouseout="layer_carrinho_off();" style="display: none;"><div id="base-cart-box"></div>
		</div>
		<span id="outlet-top-button" onclick="window.location='outlet';" style="position: absolute;right: 0px;margin-right: 0px;background: #e5333c;padding: 15px 5px 0 5px;">Outlet</span>
	</div>
</div>
<div class="carousel-container">
	<div id="produto-container" class="main-menu-item-opened-container">
		<?php
			$categorias = mysql_query("select categoriaid,ccategoria,clink_seo,ccodcategoria from tblcategoria where categoriaid != 29 AND cativa = -1 and now() between cdata_inicio and cdata_termino group by ccategoria order by ccategoria;");
			$i=1;
			$num_rows = mysql_num_rows($categorias);
			$div_a = array(1,22,43,65,87,109);
			$div_b = array(21,42,64,86,108,130,$num_rows);
			while($categoria = mysql_fetch_array($categorias)){
				if(in_array($i,$div_a)){ echo '<div class="main-menu-item-opened-container-columns" style="width:230px;margin: 10px;">'; }
				if((int)$categoria["ccodcategoria"] > 0){
					$departamento = mysql_fetch_array(mysql_query("select ccategoria,clink_seo from tblcategoria where categoriaid = ".$categoria["ccodcategoria"].";"), MYSQL_ASSOC);
				}
		?>
			<a href="<?=$departamento["clink_seo"]."/".$categoria["clink_seo"] ?>"><span class="categoria"><?=$categoria["ccategoria"] ?></span></a>
		<?php
				$departamento = array();
				if(in_array($i,$div_b)){ echo '</div>'; }
				$i++;
			}
		?>
	</div>
	<div id="fabricantes-container" class="main-menu-item-opened-container">
		<span id="top20-title" style="margin: 14px;">TOP 10 MARCAS</span>
		<?php
			$marcas = mysql_query("select mlink_seo, mimagem from tblmarca where mguia > 0 order by mordem asc limit 10");
			while($row = mysql_fetch_array($marcas)){
				echo "<a href='fabricante/".$row["mlink_seo"]."'><span class='marca_topo'><img src='";
				if(file_exists($row["mimagem"]) && $row["mimagem"] != ''){ echo $row["mimagem"]; }else{ echo "imagem/marca/marca_not_found.jpg"; }
				echo "' style='height: 80px;width: 160px;'></span></a>";
			}
		?>
		<span id="top20-title" style="margin: 14px;">Todas >></span>
		<?php
			$marcas = mysql_query("select mmarca, mlink_seo from tblmarca order by mmarca");
			$i=0;
			$cnt = 0;
			$num_rows = mysql_num_rows($marcas);
			$total_menu = ceil($num_rows / 5);
			while($row = mysql_fetch_array($marcas)){
				$i++;
				$cnt++;
				if($i==1){echo '<div class="main-menu-item-opened-container-columns" style="margin-top: 0px;">';}
				echo "<a href='fabricante/".$row["mlink_seo"]."'><span class='categoria'>".$row["mmarca"]."</spam></a>";
				if($i == $total_menu || $cnt == $num_rows){
					$i = 0;
					echo "</div>";
				}
			}
		?>
	</div>
	<div id="objetivo-container" class="main-menu-item-opened-container">
		<?php
			$departamentos = mysql_query("select categoriaid,ccategoria,clink_seo from tblcategoria where ccodcategoria = 0 and cativa = -1 and now() between cdata_inicio and cdata_termino and (cmenu = 0 or cmenu = 2) and categoriaid not in (9,8,29);");
			while($departamento = mysql_fetch_array($departamentos)){
		?>
			<div class="main-menu-item-opened-container-columns">
				<a href="<?=$departamento["clink_seo"] ?>"><span class="nome"><?=$departamento["ccategoria"] ?></span></a>
			<?php
				$categorias = mysql_query("select categoriaid,ccategoria,clink_seo from tblcategoria where categoriaid != 29 AND ccodcategoria = '".$departamento["categoriaid"]."' and cativa = -1 and now() between cdata_inicio and cdata_termino and (cmenu = 0 or cmenu = 2) order by ccategoria;");
				while($categoria = mysql_fetch_array($categorias)){
			?>
				<a href="<?=$departamento["clink_seo"]."/".$categoria["clink_seo"] ?>"><span class="categoria"><?=$categoria["ccategoria"] ?></span></a>
			<?php
				}
			?>
			</div>
		<?php
			}
		?>
	</div>
	<div id="kits-container" class="main-menu-item-opened-container">
		<span id="top20-title" style="margin: 14px;">KITS PROMOCIONAIS</span>
			<div id="top20-carousel-prev" onclick="$('#kits-carousel').data('owlCarousel').prev()"></div>
			<div id="kits-carousel" class="owl-carousel">
				<?php
					$produtos = mysql_query("select * from ((select 1 as rank, SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
					tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblmarca on marcaid = tblproduto.pcodmarca
					inner join tblproduto_categoria as pc on pc.pcodproduto = tblproduto.produtoid and pc.pcodcategoria = 8
					where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and now() between tblproduto.pdata_inicio and tblproduto.pdata_termino and pcodmarca = marcaid
					group by tblproduto.preferencia 
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 )))union(
					select 2 as rank, SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
					tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblmarca on marcaid = tblproduto.pcodmarca
					inner join tblproduto_categoria as pc on pc.pcodproduto = tblproduto.produtoid and pc.pcodcategoria = 8
					where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and now() between tblproduto.pdata_inicio and tblproduto.pdata_termino and pcodmarca = marcaid
					group by tblproduto.preferencia
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))) order by rank) as TMP group by produtoid;") or die(mysql_error());
					while($produto = mysql_fetch_array($produtos)){
				?>
					<div class="item">
						<a href="<?=$produto["plink_seo"] ?>">
						<div class="produto">
							<span class="product-thumb"><img class="lazyOwl" data-src="<?php if(file_exists($produto["pimagem"])){ echo $produto["pimagem"]; }else{ echo "imagem/produto/med-indisponivel.png"; } ?>" alt="produto"></span>
							<span class="product-menu-top20-product-title"><?=$produto["pproduto"] ?></span>
							<span class="product-menu-top20-product-mark"><b>+</b> <?=$produto["mmarca"] ?></span>
							<span class="product-menu-top20-product-price">R$ <s><?=number_format($produto["pvalor_unitario"],2,",",".") ?></s></span>
							<span class="par-velue"><?=get_valor_parcelado_new($produto["produtoid"],$produto["pvalor_unitario"])?></span>
							<span class="a-vista-price">Ou R$ <?=number_format($produto["pvalor_unitario"]-($produto["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
							<span class="boleto-text">via boleto</span>
						</div>
						</a>
					</div>
				<?php
					}
				?>
			</div>
		<div id="top20-carousel-next" onclick="$('#kits-carousel').data('owlCarousel').next()"></div>
	</div>
	<div id="top20-container" class="main-menu-item-opened-container">
		<span id="top20-title" style="margin: 14px;">TOP 20 PRODUTOS</span>
			<div id="top20-carousel-prev" onclick="$('#top20-carousel').data('owlCarousel').prev()"></div>
			<div id="top20-carousel" class="owl-carousel">
				<?php
					$produtos = mysql_query("(select 1 as rank, SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.plink_seo,tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
					tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblmarca on marcaid = tblproduto.pcodmarca
					where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and now() between tblproduto.pdata_inicio and tblproduto.pdata_termino and pcodmarca = marcaid and ptop20 = -1
					group by tblproduto.preferencia 
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 )))union(
					select 2 as rank, SUM(tblestoque.eestoque),tblproduto.pdescricao, tblproduto.plink_seo,tblproduto.pcontrola_estoque,tblproduto.pdisponivel,tblproduto.produtoid as produtoid, tblmarca.mmarca, tblmarca.mlink_seo, tblproduto.preferencia,
					tblproduto.pproduto, tblproduto.pvalor_unitario, tblproduto.pvalor_comparativo, 
					tblproduto.plink_seo, tblproduto_midia.marquivo as pimagem
					from tblproduto
					left join tblestoque on tblestoque.ecodproduto = tblproduto.produtoid
					left join tblproduto_midia on tblproduto.produtoid = tblproduto_midia.mcodproduto and tblproduto_midia.mprincipal = -1
					left join tblmarca on marcaid = tblproduto.pcodmarca
					where tblproduto.pdisponivel=-1 and tblproduto.pvitrine=-1 and now() between tblproduto.pdata_inicio and tblproduto.pdata_termino and pcodmarca = marcaid
					group by tblproduto.preferencia 
					HAVING ((SUM(tblestoque.eestoque) > 0 && tblproduto.pcontrola_estoque = -1 && tblproduto.pdisponivel = -1) || (tblproduto.pdisponivel = -1 && tblproduto.pcontrola_estoque = 0 ))) order by rank");
					$cnt = 0;
					while($produto = mysql_fetch_array($produtos)){
						$compara = strtolower($produto["pproduto"]);
						if(!strstr($compara,"creatina") && !strstr($compara,"carnetina") && !strstr($compara,"carnitine") && !strstr($compara,"polifenol de alcachofra")){
							$cnt++;
				?>
					<div class="item">
						<a href="<?=$produto["plink_seo"] ?>">
						<div class="produto" style="margin-top: 0px;">
							<span class="product-thumb"><img class="lazyOwl" data-src="<?php if(file_exists($produto["pimagem"])){ echo $produto["pimagem"]; }else{ echo "imagem/produto/med-indisponivel.png"; } ?>" alt="produto"></span>
							<span class="product-menu-top20-product-title"><?=$produto["pproduto"] ?></span>
							<span class="product-menu-top20-product-mark"><b>+</b> <?=$produto["mmarca"] ?></span>
							<span class="product-menu-top20-product-price">R$ <s><?=number_format($produto["pvalor_unitario"],2,",",".") ?></s></span>
							<span class="par-velue"><?=get_valor_parcelado_new($produto["produtoid"],$produto["pvalor_unitario"])?></span>
							<span class="a-vista-price">Ou R$ <?=number_format($produto["pvalor_unitario"]-($produto["pvalor_unitario"]*0.10),2,",",".")?> à vista</span>
							<span class="boleto-text">via boleto</span>
						</div>
						</a>
					</div>
				<?php
						}
						if($cnt == 20){
							break;
						}
					}
				?>
			</div>
		<div id="top20-carousel-next" onclick="$('#top20-carousel').data('owlCarousel').next()"></div>
	</div>
</div>
<script type="text/javascript">
	<?php carregaJs(); ?>
</script>
