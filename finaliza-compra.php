<?php
	
	include("include/inc_conexao.php");
	include("include/inc_frete.php");
	include("include/inc_boleto.php");	

	$expires = time()+ 60 * 60 * 24 * 60; // 60 dias de cookie
	

	/*-------------------------------------------------------------
	verifica se ta logado
	-------------------------------------------------------------*/
	if(isset($_SESSION["cadastro"])){
		$cadastro = $_SESSION["cadastro"];
		if(!is_numeric($cadastro)){
			header("location: login.php?redir=minha-conta.php");
			exit();
		}
	}
	else
	{
		header("location: login.php?redir=carrinho.php");
		exit();
	}
	
	/*-------------------------------------------------------------------	
	//navegação com ssl
	---------------------------------------------------------------------*/
	
	$config_certificado_instalado = get_configuracao("config_certificado_instalado");
	if($config_certificado_instalado==-1){
		if(strpos($_SERVER['SERVER_NAME'],".com")>0){
			if($_SERVER['SERVER_PORT']==80){
				header("location: https://".$_SERVER['SERVER_NAME']."".$_SERVER['REQUEST_URI']);
				exit();
			}
		}	
	}
	
	/*------------------------------------------------------------------------
	verifica se tem algum pedido
	--------------------------------------------------------------------------*/
	if(isset($_COOKIE["pedido"])){
		$pedido = $_COOKIE["pedido"];
		if(!is_numeric($pedido)){
			$pedido = 0;	
		}
	}
	

	/*------------------------------------------------------------------------
	pedido de venda
	--------------------------------------------------------------------------*/
	$ssql = "select tblpedido.pcartao_numero,tblforma_pagamento.fbandeira,tblcadastro.ctelefone,tblcadastro.ccelular,tblpedido.pcodorcamento, tblpedido.pcodcadastro, tblpedido.ptitulo, tblpedido.pnome, tblpedido.pendereco, tblpedido.pnumero, tblpedido.pcomplemento, 
			tblpedido.pbairro, tblpedido.pcidade, tblpedido.pestado, tblpedido.pcep,   
			tblpedido.psubtotal, tblpedido.pvalor_desconto, tblpedido.pvalor_frete, 
			tblpedido.pvalor_presente, tblpedido.pvalor_presente_cartao, tblpedido.pvalor_desconto_cupom, tblpedido.pvalor_desconto_troca, 
			tblpedido.pvalor_total, tblpedido.pcodforma_pagamento, tblpedido.pcodcondicao_pagamento, tblpedido.pcodfrete, tblforma_pagamento.fforma_pagamento, 
			tblforma_pagamento.finstrucao_pagamento, tblpedido.ptexto_presente, 
			tblforma_pagamento.fcodtipo, tblcondicao_pagamento.ccondicao, tblcondicao_pagamento.cnumero_parcelas, tblfrete_tipo.fcodigo, tblfrete_tipo.fdescricao, 
			tblfrete_tipo.fprazo, tblcupom_desconto.ccupom,
			tblcadastro.cemail,
			tbllista_presente.listaid, tbllista_presente.lnotificacao, tbllista_presente.lcodcadastro
			from tblpedido
			inner join tblforma_pagamento on tblpedido.pcodforma_pagamento=tblforma_pagamento.formapagamentoid
			left join tblcondicao_pagamento on tblpedido.pcodcondicao_pagamento=tblcondicao_pagamento.condicaoid
			inner join tblfrete_tipo on tblpedido.pcodfrete=tblfrete_tipo.freteid
			inner join tblcadastro on tblpedido.pcodcadastro=tblcadastro.cadastroid
			left join tblcupom_desconto on tblpedido.pcodcupom_desconto=tblcupom_desconto.cupomid
			left join tbllista_presente on tblpedido.pcodlista_presente = tbllista_presente.listaid
			where tblpedido.pedidoid='{$pedido}'";
	$result = mysql_query($ssql);
	
	if($result){
		
		while($row=mysql_fetch_assoc($result)){
			$orcamento				= $row["pcodorcamento"];
			$titulo					= $row["ptitulo"];
			$nome					= $row["pnome"];
			$email					= $row["cemail"];
			$telefone				= $row["ctelefone"];
			$celular				= $row["ccelular"];
			$endereco				= $row["pendereco"];
			$numero					= $row["pnumero"];
			$complemento			= $row["pcomplemento"];
			$bairro					= $row["pbairro"];
			$cidade					= $row["pcidade"];
			$estado					= $row["pestado"];
			$bandeira_cartao		= $row["fbandeira"];
			$cartao_numero_bd		= $row["pcartao_numero"];
			$cep					= substr($row["pcep"],0,5)."-".substr($row["pcep"],5,3);
			
			
			$despesa				= number_format($row["pvalor_presente"]+$row["pvalor_presente_cartao"],2,",",".");
			$frete					= number_format($row["pvalor_frete"],2,",",".");
			$valor_frete			= number_format($row["pvalor_frete"],2,".","");
			$pagseguro_frete		= number_format($row["pvalor_frete"],2);
			$desconto				= number_format($row["pvalor_desconto"],2,",",".");
			$desconto_cupom			= number_format($row["pvalor_desconto_cupom"],2,",",".");
			
			$subtotal				= number_format($row["psubtotal"],2,",",".");
			$total					= number_format($row["pvalor_total"],2,",",".");
			
			
			$valor_documento		= $row["pvalor_total"];
			$valor_subtotal			= number_format($row["psubtotal"],2,".","");	
			$valor_total			= number_format($row["pvalor_total"],2,".","");		
			
			if($frete==0){
				$frete = "Frete grátis";	
			}else{
				$frete = "R$ " . $frete;
				}
			
			$forma_pagamento		= $row["fforma_pagamento"];
			$instrucao_pagamento	= $row["finstrucao_pagamento"];
			
			$pagto_tipo				= $row["fcodtipo"];
			
			$cartao_numero			= $row["pcartao_numero"];
			$condicao_pagamento		= $row["pcondicao"];
			$ccondicao_pagamento		= $row["ccondicao"];
			$parcelas				= $row["cnumero_parcelas"];
			$frete_tipo				= strtoupper($row["fdescricao"]) . "<br>Cód. serviço: " . $row["fcodigo"];
			$frete_desc				= $row["fdescricao"];
			$frete_prazo			= $row["fprazo"];
			
			$cartao_numero 			= formata_cartao_mascara($cartao_numero);
			
			$cupom_desconto			= $row["ccupom"];
			
			$lista_texto_presente	= $row["ptexto_presente"];			
			
			$lista_cadastro			= intval($row["lcodcadastro"]);
			$lista_notificacao		= intval($row["lnotificacao"]);
			$lista_presente			= intval($row["listaid"]);			
		
		}
		mysql_free_result($result);
	}




	/*-------------------------------------------------------------------------------
	frete_prazo
	-------------------------------------------------------------------------------*/
		$frete1 = new Frete();
		$frete1->cep_origem = get_loja(1,"lcep");
		$frete1->cep_destino = $cep;
		$frete1->valor_declarado = 0;
		$frete1->peso = 1;			
		//$frete_prazo = $frete1->CalculaPrazoEntrega( right($frete_tipo,6) ) ." dias úteis.";            


	/*-------------------------------------------------------------------------------
		Gera boleto se necessário
	---------------------------------------------------------------------------------*/
	
	if($pagto_tipo == 1){
		
		//verifica se já existe o boleto
		$boletoid = 0;
		$ssql = "select boletoid, bstatus from tblboleto where bcodpedido=$pedido";
		$result = mysql_query($ssql);
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$boletoid = $row["boletoid"];
				if($row["bstatus"]==9){		//cancelado
					$boletoid = -1;
				}
			}
			mysql_free_result($result);
		}
		
		
		//carrega variaveis e cedente
		$ssql = "select cedenteid, crazao_social, ccnpj, cbanco, cnumero_banco, cnumero_banco_dac, cagencia, cconta_corrente, cconta_corrente_dac, cnosso_numero, cnumero_documento, 
				ccarteira, cconvenio, ctaxa, cdias from tblcedente where cedenteid = 1";
		$result = mysql_query($ssql);		
		if($result){
			while($row=mysql_fetch_assoc($result)){
				$cedenteid			 		=	$row["cedenteid"];
				$cedente_razaosocial 		=	$row["crazao_social"];
				$cedente_cnpj		 		=	$row["ccnpj"];
				$cedente_banco		 		=	$row["cbanco"];
				
				$cedente_numero_banco 		=	$row["cnumero_banco"];
				$cedente_numero_banco_dac 	=	$row["cnumero_banco_dac"];
				
				$cedente_agencia		 	=	right("0000".$row["cagencia"],4);
				$cedente_conta_corrente 	=	right("00000".$row["cconta_corrente"],5);
				$cedente_conta_corrente_dac	=	$row["cconta_corrente_dac"];
				
				$cedente_nosso_numero	 	=	right("00000000".$pedido,8);		//$row["cnoso_numero"];
				$cedente_numero_documento 	=	right("0000000000".$pedido,10); 	//$row["cnumero_documento"];
				
				$cedente_carteira		 	=	$row["ccarteira"];
				$cedente_convenio		 	=	$row["cconvenio"];
				
				$cedente_taxa		 		=	$row["ctaxa"];
				$cedente_dias 				=	$row["cdias"];
			}
			mysql_free_result($result);
		}		



		//formata variavies 
		if(!is_numeric($cedente_dias)){
			$cedente_dias = 3;	
		}
		if(!is_numeric($cedente_taxa)){
			$cedente_taxa = 0;	
		}		

		$moeda						=	9;
		// $valor_documento			= 	$valor_documento + $cedente_taxa;
		//$valor_documento 			= 	formata_valor_db($valor_documento);
		
		$data_vencimento			=	addDayIntoDate(date("Ymd"),$cedente_dias);
		$data_vencimento			=	substr($data_vencimento,0,4)."-".substr($data_vencimento,4,2)."-".substr($data_vencimento,6,2);
		$data_documento				= 	date("Y-m-d");
		
		$fator_vencimento			=	fator_vencimento(formata_data_tela($data_vencimento));
		
		$cedente_nosso_numero_dac 	= 	modulo_10($cedente_agencia.$cedente_conta_corrente.$cedente_carteira.$cedente_nosso_numero);
		$instrucao					=	"Pedido #$cedente_nosso_numero efetuado na loja " . $site_nome;
		
		$valor_documento_formatado	=	right("0000000000" . get_only_numbers($valor_documento) ,10) ;
		
		$codigo_barras 	= 	$cedente_numero_banco.$moeda.$fator_vencimento.$valor_documento_formatado.$cedente_carteira.$cedente_nosso_numero.$cedente_nosso_numero_dac.$cedente_agencia.$cedente_conta_corrente.$cedente_conta_corrente_dac.'000';

		$dv 			= 	digitoVerificador_barra($codigo_barras);
		
		$linha			=	substr($codigo_barras,0,4).$dv.substr($codigo_barras,4,43);
		
		$referencia_numerica =  monta_linha_digitavel($linha);
		
		// if($boletoid==0){
			$ssql = "insert into tblboleto (bagencia, bconta_corrente, bconta_corrente_dac, bcodpedido, bcodcadastro, bemail, bdata_vencimento, bdata_documento, bnumero_documento, 
											bcarteira, bnosso_numero, bnosso_numero_dac, bvalor_documento, binstrucao, breferencia_numerica, bcodigo_barras, bstatus,
											btaxa,
											bcodusuario, bdata_alteracao, bdata_cadastro) 
											values('{$cedente_agencia}', '{$cedente_conta_corrente}','{$cedente_conta_corrente_dac}','{$pedido}','{$cadastro}','{$email}','{$data_vencimento}',
												   '{$data_documento}','{$cedente_numero_documento}','{$cedente_carteira}','{$cedente_nosso_numero}','{$cedente_nosso_numero_dac}',
												   '{$valor_documento}','{$instrucao}', '{$referencia_numerica}','{$codigo_barras}','0','{$cedente_taxa}',
												   '0','{$data_hoje}','{$data_hoje}') ";	
			$result = mysql_query($ssql);
		// }

		
		// if($boletoid>0){
		// 		$ssql = "update tblboleto set bagencia='{$cedente_agencia}', bconta_corrente='{$cedente_conta_corrente}', bconta_corrente_dac='{$cedente_conta_corrente_dac}', bcodpedido='{$pedido}', 
		// 				bcodcadastro='{$cadastro}', bemail='{$email}', bdata_vencimento='{$data_vencimento}', bdata_documento='{$data_documento}', 
		// 				bnumero_documento='{$cedente_numero_documento}', bcarteira='{$cedente_carteira}', bnosso_numero='{$cedente_nosso_numero}', 
		// 				bnosso_numero_dac='{$cedente_nosso_numero_dac}', bvalor_documento='{$valor_documento}', binstrucao='{$instrucao}', 
		// 				breferencia_numerica='{$referencia_numerica}', 
		// 				bcodigo_barras='{$codigo_barras}', btaxa='{$cedente_taxa}', bdata_alteracao='{$data_hoje}' where boletoid = $boletoid";
		// 	$result = mysql_query($ssql);
		// 	//echo $ssql;
		// }
		
	
		//link pagamento
		$link_boleto = "boleto_itau.php?pedido=".$pedido."&email=".$email;
	
	}

/*-------------------------------------------------------------------------------
		gera itens do e-mail	// pagseguro	// paypal
	---------------------------------------------------------------------------------*/
	$i = 0;
	
	$ssql = "select tblpedido_item.itemid, tblpedido_item.pcodproduto, tblpedido_item.pcodpropriedade, tblpedido_item.pcodtamanho, tblpedido_item.pquantidade, 
			tblpedido_item.pvalor_unitario, tblpedido_item.ppeso, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pdescricao, tblproduto.pfrete_gratis
			from tblpedido_item
			inner join tblproduto on tblpedido_item.pcodproduto = tblproduto.produtoid
			where tblpedido_item.pcodpedido='{$pedido}' and tblpedido_item.pquantidade > 0";
	$result = mysql_query($ssql);
	if($result){
		
		$num_rows = mysql_num_rows($result);
		
		//if($desconto_cupom > 0 ){
		//	$desconto_cupom_pagseguro = $desconto_cupom/$num_rows;	
		//}
		
		while($row=mysql_fetch_assoc($result)){	
			$qtde_itens += $row["pquantidade"];
		}
		
		if($desconto_cupom > 0 ){
			$desconto_cupom_pagseguro = $desconto_cupom/$qtde_itens;	
		}
		
		mysql_data_seek($result,0);
		
		while($row=mysql_fetch_assoc($result)){		
			$i++;

			$items .= '<tr>
						  <td valgn=top style="border:none;border-bottom:dotted #CCCCCC 1.0pt;padding:2.25pt 6.75pt 2.25pt 6.75pt">
							<p style="line-height:16.2pt"><strong><span style="font-size:8.5pt;font-family:Verdana,sans-serif;color:#2F2F2F">'.$row["pproduto"].'</span></strong></p>
						  </td>
						  <td valign=top style="border:none;border-bottom:dotted #CCCCCC 1.0pt; padding:2.25pt 6.75pt 2.25pt 6.75pt">
							<p style="line-height:16.2pt"><span style="font-size: 8.5pt;font-family:Verdana,sans-serif;color:#2F2F2F">'.$row["pcodigo"].'</span></p>
						  </td>
						  <td valign=top style="border:none;border-bottom:dotted #CCCCCC 1.0pt; padding:2.25pt 6.75pt 2.25pt 6.75pt">
							<p align=center style="text-align:center;line-height: 16.2pt"><span style="font-size:8.5pt;font-family:Verdana,sans-serif; color:#2F2F2F">'.$row["pquantidade"].'</span></p>
						  </td>
						  <td valign=top style="border:none;border-bottom:dotted #CCCCCC 1.0pt; padding:2.25pt 6.75pt 2.25pt 6.75pt">
							<p align=right style="text-align:right;line-height:16.2pt"><span><span style="font-size:8.5pt;font-family:Verdana,sans-serif; color:#2F2F2F">R$ '.number_format($row["pquantidade"]*$row["pvalor_unitario"],2,",",".").'</span></span></p>
						  </td>
					   </tr>';
			
			$produto = left($row["pproduto"],80);
			$produto = utf8_decode($produto); 
			$pagseguro_frete = "0";		
			
			$pagseguro_items .='<input type="hidden" name="item_id_'.$i.'" value="'.$row["pcodproduto"].'">';	
			$pagseguro_items .='<input type="hidden" name="item_descr_'.$i.'" value="'.$produto. '">';
			$pagseguro_items .='<input type="hidden" name="item_quant_'.$i.'" value="'.$row["pquantidade"].'">';
			$pagseguro_items .='<input type="hidden" name="item_valor_'.$i.'" value="'.number_format($row["pvalor_unitario"]-$desconto_cupom_pagseguro,2,".",",").'">';
			$pagseguro_items .='<input type="hidden" name="item_frete_'.$i.'" value="'.number_format($pagseguro_frete,2).'" />';
			$pagseguro_items .='<input type="hidden" name="item_peso_'.$i.'" value="0"  />';
			
			
		}
		
		
			
		if(floatval($valor_frete)>0){
			$i++;
			$pagseguro_items .='<input type="hidden" name="item_id_'.$i.'" value="'.$i.'">';	
			$pagseguro_items .='<input type="hidden" name="item_descr_'.$i.'" value="Despesa de frete">';
			$pagseguro_items .='<input type="hidden" name="item_quant_'.$i.'" value="1">';
			$pagseguro_items .='<input type="hidden" name="item_valor_'.$i.'" value="'.number_format($valor_frete,2,".",",").'">';
			$pagseguro_items .='<input type="hidden" name="item_frete_'.$i.'" value="0" />';
			$pagseguro_items .='<input type="hidden" name="item_peso_'.$i.'" value="0"  />';
				
			
		}	

			
		$paypal_items .= '<input type="hidden" name="item_name" value="Pedido # '.$pedido.' - '.$site_nome.'">';	
		$paypal_items .= '<input type="hidden" name="item_number_1" value="1">';
		$paypal_items .= '<input type="hidden" name="quantity" value="1">';
		$paypal_items .= '<input type="hidden" name="amount" value="'.$valor_total.'">';
		
		
			
		mysql_free_result($result);
	}	
	
	$pagseguro_email = get_configuracao("pagseguro_email");
	$pagseguro_email = strtolower($pagseguro_email);
	
	$paypal_email	= get_configuracao("paypal_email");
	$paypal_email	= strtolower($paypal_email);
	
	
	$link_pagseguro = '
				  <form method="post" action="https://pagseguro.uol.com.br/checkout/checkout.jhtml" id="frm_pagseguro" name="frm_pagseguro" target="pagseguro">
                  <input type="hidden" name="email_cobranca" value="'.$pagseguro_email.'"/>
                  <input type="hidden" name="tipo" value="CP"/>
                  <input type="hidden" name="moeda" value="BRL"/>
                  <input type="hidden" name="ref_transacao" value="'.$pedido.'" />';
	 $link_pagseguro .= $pagseguro_items;
	 
     $link_pagseguro .= '             
                  <!-- IN&Iacute;CIO DOS DADOS DO USU&Aacute;RIO -->
                  <input type="hidden" name="cliente_nome" value="'.$nome.'" />
                  <input type="hidden" name="cliente_cep" value="'.$cep.'" />
                  <input type="hidden" name="cliente_end" value="'.$endereco.'" />
                  <input type="hidden" name="cliente_num" value="'.$numero.'" />
                  <input type="hidden" name="cliente_compl" value="'.$complemento.'" />
                  <input type="hidden" name="cliente_bairro" value="'.$bairro.'" />
                  <input type="hidden" name="cliente_cidade" value="'.$cidade.'" />
                  <input type="hidden" name="cliente_uf" value="'.$estado.'" />
                  <input type="hidden" name="cliente_pais" value="BRA" />
                  <input type="hidden" name="cliente_ddd" value="'.$ddd.'" />
                  <input type="hidden" name="cliente_tel" value="'.$telefone.'" />
                  <input type="hidden" name="cliente_email" value="'.$email.'" />
                  <input type="image" name="cmd_pagseguro" id="cmd_pagseguro" src="images/ico-pagseguro.gif">
                  <!-- FIM DOS DADOS DO USU&Aacute;RIO -->
                  </form>';
				  
				  
	$link_paypal = '
					<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="frm_paypal" target="_paypal"> 
					<!-- Identify your business so that you can collect the payments. --> 
					<input type="hidden" name="business" value="'.$paypal_email.'"> 
					<!-- Specify a Buy Now button. --> 
					<input type="hidden" name="cmd" value="_xclick"> 
					<!-- Specify details about the item that buyers will purchase. --> 
					'.$paypal_items.'
					<input type="hidden" name="currency_code" value="BRL"> 
					<!-- Display the payment button. --> 
					<input name="returnUrl" type="hidden" value="'.$site_site.'/?refer=paypal" />
					<input type="image" name="submit" border="0" src="images/ico-paypal.png" alt="PayPal - The safer, easier way to pay online"> 
					<img alt="" border="0" width="1" height="1" src="https://www.paypal.com/en_US/i/scr/pixel.gif" >
					</form> 	
				';			  

		// Código de envio de email //


		// Encerramento //	

?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> Compra Finalizada</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> Compra Finalizada" />
<meta name="description" content="<?php echo $site_nome;?> Compra Finalizada" />
<meta name="keywords" content="<?php echo $site_nome;?> Compra Finalizada" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $site_nome;?> Compra Finalizada" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<link rel="canonical" href="<?php echo $site_site;?>/finaliza-compra.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/funcao.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>

<script language="javascript" type="text/javascript">
$(document).ready(function(){
	$(".btn-codigo").click(function(){
		$(".box-codigo-barra").show();								
	});
	
	$("#instrucoes").click(function(){
		$("#shadow").fadeIn();
		$("#janela_modal").fadeIn();
	});
	$("#shadow").click(function(){
		$("#shadow").hide();
		$("#janela_modal").hide();
	});
	$("#fechar_modal").click(function(){
		$("#shadow").hide();
		$("#janela_modal").hide();
	});
        $("iframe[name=google_conversion_frame]").css("width","0px");
        $("iframe[name=google_conversion_frame]").css("height","0px");
});
</script>
<?php include("include/inc_analytics.php");	?>
</head>

<body>
<div id="global-container">

	<div id="header-content">

        <?php
			include("inc_headerSTEP.php");
		?>

    </div> 
    <div id="shadow"></div>
	<div id="janela_modal">
            <div id="fechar_modal">X</div>
            <div id="conteudo_modal">
                <p><strong>Como enviar o comprovante do pagamento:</strong></p>
                <p>Os sites de bancos costumam ter a opção de salvar o comprovante em PDF diretamente no seu computador, se souber como fazê-lo, pule as etapas de 1 a 4 e siga diretamente para a 5. 
                Caso tenha dificuldades, siga os passos abaixo para salvar o comprovante de outra forma:</p>
                <p>1 – após efetuar a transferência pela internet, deixe a tela do comprovante aberta e pressione a tecla PRINT SCREEN ou PRTSCN do seu teclado. Isto irá tirar uma foto da sua tela.</p>
                <p>2 – Abra o programa Paint do Windows. </p>
                <p>3 – Clique no botão COLAR na barra de ferramentas superior esquerda do programa OU aperte as teclas CTRL+V do seu teclado.</p>
                <p>4 – Depois vá em Arquivo -> Salvar como -> e em “Nome do arquivo” digite um nome para seu comprovante e escolha um local para salvar.</p>
                <p>5 – Volte para a tela de pagamento do site, clique em ENVIAR COMPROVANTE, encontre seu arquivo na pasta onde salvou e dê um duplo clique nele.</p>
                <p>Pronto, seu arquivo foi anexado e será analisado pelo nosso departamento financeiro. 
                Obrigado.</p>
            </div>
	</div>
    
	<div id="main-box-container">
		<div id="andamento">
			<span class="passox " style="margin-left:0;">1 - Identificação <img src="images/setaSTEP.jpg"/></span>
			<span class="passox " style="margin-left: 162px;">2 - Entrega <img src="images/setaSTEP.jpg"/></span>
			<span class="passox " style="margin-left: 162px;">3 - Pagamento <img src="images/setaSTEP.jpg"/></span>
			<span class="passox ativo" style="margin-left: 162px;">4 - Confirmação</span>
		</div>
		<span id="tituloStep">Parabéns!</span>
		<span id="subtituloStep" style="border:0px;">Seu pedido foi efetuado com sucesso!</span>
		<br>
        <div id="informacoes-pagamento">
    
        <div id="box-numero-pedido">
            <div class="txt-numero-pedido">Número do Pedido: <span style="color:#000; font-size:35px;"><?php echo 15500+$pedido;?></span></div>
            <div style="float: left;color: #000;font-size: 15px;margin-left: 50px;margin-top: 10px;font-family: Gudea-BOLD;">A confirmaçao do pedido foi enviada para: <strong><?php echo $email;?></strong></div>
            <div class="txt-numero-pedido" style="width:247px; height:25px; font-size:21px; margin-top:35px">Forma de Pagamento:</div>
            <div style="width: 500px;float: left;color: #000;font-size: 16px;margin-left: 50px;font-family: Gudea-BOLD;">Você optou por pagar no <strong><?php echo $forma_pagamento;?></strong>.</div>
			<?php
				if($pagto_tipo == 7){
			?>
            <div style="width: 500px;float: left;color: #000;font-size:14px; margin-top: 10px;margin-left: 50px;font-family: Gudea-BOLD;">Dados da Conta: <br/>Agência: 7196<br/> Conta Corrente: 14179-5<br />Banco: Itaú</div>
            <?php
				}
			?>
            
            <div class="box-btn-pagamento">
                
                <?php
                if( $pagto_tipo == 1 ){
                    echo '	
                        <div class="box-codigo-barra">
                            <strong>BOLETO (CÓDIGO DE BARRAS)</strong><br>
                            '.$referencia_numerica.'
                        </div>
                        
                        <div class="btn-visualizar">
                            <a href="'.$link_boleto.'" target="_blank">
                            <img src="images/btn-visualizar-boleto.png" border="0" />
                            </a>
                        </div>
                        
                        <div class="btn-codigo">
                            <img src="images/btn-copiar-codigo.png" border="0" />
                        </div>
                    
                        <div class="btn-imprimir">
                            <a href="'.$link_boleto.'&action=print" target="_blank">
                            <img src="images/btn-imprimir.png" border="0" />
                            </a>
                        </div>';
                }
                
                
                if( $pagto_tipo == 5 ){
                    echo '	
                        <div class="box-instrucao">
                            <b>CLIQUE NO ÍCONE ABAIXO PARA REALIZAR SEU PAGAMENTO.</b><br /><br />
                            '.$link_pagseguro.'
                        </div>';
                }
				
				
                if( $pagto_tipo == 6 ){
                    echo '	
                        <div class="box-instrucao">
                            <b>CLIQUE NO ÍCONE ABAIXO PARA REALIZAR SEU PAGAMENTO.</b><br /><br />
                            '.$link_paypal.'
                        </div>';
                }
				
				if( $pagto_tipo == 7 ){
					$_SESSION["pedidoid"] = $pedido;
					
                    echo '<div class="btn-imprimir" style="cursor: pointer; margin-left: 50px;" id="instrucoes">
                            <img src="images/btn-instrucoes.png"/>
                        </div>
                        <iframe src="upload.php" frameborder="0" scrolling="no" style="margin-left: 65px;"></iframe>
						';
                }
                
                ?>
                                
            </div>
 
        </div>
    
        <div id="box-resumo-pedido">
            <div style="width:320px; height:25px; font-size:20px; margin-top:17px; margin-left:20px; font-size:12px;padding-left:10px;"><strong style="font-family: Gudea-BOLD;color: #f00;font-size: 19px;">Resumo do pedido:</strong></div>
            <div style="width:320px; height:20px; float:left; color:#616060; font-family:Arial, Helvetica, sans-serif; font-size:14px; margin-left:20px;padding-left:10px;"><strong style="color: #606060;position: relative;top: 2px;">Itens do Pedido</strong>:</div>
                
            <div class="itens-pedido">
                <?php
                    $ssql = "select tblpedido_item.itemid, tblpedido_item.pcodproduto, tblpedido_item.pcodpropriedade, tblpedido_item.pcodtamanho, tblpedido_item.pquantidade, 
                            tblpedido_item.pvalor_unitario, tblpedido_item.ppeso, tblproduto.pcodigo, tblproduto.pproduto, tblproduto.pdescricao, tblproduto.pfrete_gratis
                            from tblpedido_item
                            inner join tblproduto on tblpedido_item.pcodproduto = tblproduto.produtoid
                            where tblpedido_item.pcodpedido='{$pedido}' and tblpedido_item.pquantidade > 0";
                    $result = mysql_query($ssql);
                    if($result){  
                    	$products[] = array();
                    	$valor_total = 0;
                        while($row = mysql_fetch_assoc($result) ){
                            $products[] =   $row["pcodproduto"];
                            $valor_total += $row["pvalor_unitario"];
                            $produto	=	$row["pproduto"];
                            $qtde		=	$row["pquantidade"];
                            $valor		=	number_format($row["pquantidade"]*$row["pvalor_unitario"],2,",",".");
                            
                            echo '
                                <span class="relacao-item-pedido">'.$produto.'</span><span class="valor-item-pedido"><strong><span style="font-family:Oswald-REGULAR; font-size:10px;font-weight:normal;">R$ </span>'.$valor.'</strong></span>
                            ';
                        }
                        mysql_free_result($result);
                    }
                ?>
            </div>
            
            <div style="width:200px; height:16px; float:left; color:#616060; font-family: DoppioOne-REGULAR; font-size: 15px; margin-left:30px; margin-top:5px">Frete</div>
            <div style="width:100px; height:16px; float:left; color:#616060; font-family: DoppioOne-REGULAR; font-size: 15px; margin-left:0px; margin-top:5px; text-align:right"><?php if($valor_frete>0){ echo "R$".number_format($valor_frete,2,",",".");}else{ echo "Grátis";}?></div>
            
            <div style="width:200px; height:16px; float:left; color:#616060; font-family: DoppioOne-REGULAR; font-size: 15px; margin-left:30px; margin-top:5px">Desconto</div>
            <div style="width:100px; height:16px; float:left; color:#616060; font-family: DoppioOne-REGULAR; font-size: 15px; margin-left:0px; margin-top:5px; text-align:right">R$<?php echo $desconto;?></div>
            
            <div style="width:200px; height:18px; float:left; color:#616060; font-family:DoppioOne-REGULAR; font-size:18px; margin-left:30px; margin-top:10px">Total</div>
            <div style="width:100px; height:18px; float:left; color:#616060; font-family:DoppioOne-REGULAR; font-size:18px; margin-left:0px; margin-top:10px; text-align:right">R$<?php echo $total;?></div>
            
            <div class="txt-numero-pedido" style="width:247px; height:16px; font-size:14px; margin-top:10px; margin-left:30px"><strong style="color: #f00;font-family: DoppioOne-REGULAR;">Endereço de entrega</strong></div>
            
            
            <div class="box-endereco">
                <span class="info-endereco">
                    <?php 
                        echo $endereco . "," . $numero . "&nbsp;&nbsp;&nbsp;". $complemento . " " .$bairro;
                        echo "<br />";
                        echo $cidade . "-" . $estado;
                        echo "<br />";
                        echo $cep;
                    ?>
                </span>
            </div>
            
        </div>
        
      </div>
  
 	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footerSTEP.php");
        ?>
    </div>
</div>


<script type="text/javascript">
var google_tag_params = {
	ecomm_prodid: <?php echo json_encode($products); ?>,
	ecomm_pagetype: 'purchase',
	ecomm_totalvalue: <?php echo number_format($valor_total, 2, ".", ".");?>,
};
</script>

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<div style="display:inline;">
	<img height="1" width="1" style="border­style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/970176820/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '570933616448411');
fbq('track', "PageView");
fbq('track', 'Purchase', {
	content_type: 'product',
	content_ids: <?php echo json_encode($products); ?>,
	value: <?php echo number_format($valor_total, 2, ".", ".");?>,
	currency: 'BRL'
});
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=570933616448411&ev=PageView&noscript=1"/></noscript>

<script type="text/javascript">
var _tn = _tn || [];
_tn.push(['_setAccount','a2ce2798026ce456d1422a01de2b73b3']);
_tn.push(['_setAction','track-view']);
(function() {
document.write(unescape("%3Cspan id='tolvnow'%3E%3C/span%3E"));
var tss = document.createElement('script'); tss.type = 'text/javascript'; tss.async = true;
tss.src = '//www.tolvnow.com/tracker/tn.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(tss, s);
})();
</script>
<!-- Google Code for Compra Produtos Conversion Page --> 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 970176820;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "-mqbCMbs8lcQtPLOzgM"; var google_conversion_value = 10.00; var google_conversion_currency = "BRL"; var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript"  
src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="//www.googleadservices.com/pagead/conversion/970176820/?value=10.00&amp
;currency_code=BRL&amp;label=-mqbCMbs8lcQtPLOzgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
	
	