<?php
	include("include/inc_conexao.php");	

	$conteudo_titulo;
	$conteudo_descricao;
	$conteudo_palavra_chave;
	$conteudo_texto;
	
	//13 - Clipping
	get_conteudo(13);	
	

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $site_nome;?> <?php echo $conteudo_titulo;?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX, FOLLOW" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="Fri, 13 Jul 2001 00:00:01 GMT" />

<meta name="title" content="<?php echo $site_nome;?> <?php echo $conteudo_titulo;?>" />
<meta name="description" content="<?php echo $conteudo_descricao;?>" />
<meta name="keywords" content="<?php echo $conteudo_palavra_chave;?>" />
<meta name="language" content="pt-br" />
<meta name="abstract" content="<?php echo $conteudo_titulo;?>" />

<meta name="copyright" content="<?php echo $site_nome;?>" />

<link rel="shortcut icon" href="images/favicon.png" type="image/png" />

<base href="<?php echo $server;?>" />
<link rel="canonical" href="<?php echo $site_site;?>/seguranca.php" />

<link type="text/css" rel="stylesheet" href="css/style.css" />
<link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />

<?php
include("include/inc_analytics.php");	
?>

</head>

<body>
<div id="global-container">
	<div id="header-content">

        <?php
			include("inc_header.php");
		?>

    </div>
    
	<div id="main-box-container">
    	<div id="menu-conta-left">
		<?php
            include("inc_left_conteudo.php");
        ?>
    </div>
    
    <div id="box-meio-minha-conta">
    	<div id="box-meus-dados">
            <h4 class="h4-minha-conta"><?php echo $conteudo_titulo;?></h4>
            <span class="dados-conta">&nbsp;</span>
            
            <div class="box-interna"> 
            	<span class="descricao-interna">
                <?php echo $conteudo_texto;?>
                </span>
            </div>
    	</div>
        
        
    </div>
       		
        
        
	</div>
    
    <div id="footer-container">
		<?php
            include("inc_footer.php");
        ?>
    </div>
</div>
</body>
</html>